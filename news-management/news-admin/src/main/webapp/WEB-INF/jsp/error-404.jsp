<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="content">
    <div class="container jumbotron">
        <h3>Error 404 - Page Not Found</h3>
        <h6>Message:</h6>
        <c:if test="${exception} != null">
            <p>${exception.message}</p>
        </c:if>
    </div>
</div>