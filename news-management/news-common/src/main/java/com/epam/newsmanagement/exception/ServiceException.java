package com.epam.newsmanagement.exception;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Exception class that can be thrown in case of error or exception in any service class method
 */
public class ServiceException extends Exception {

    public ServiceException(){}

    public ServiceException(String message){
        super(message);
    }

    public ServiceException(DAOException exception){
        super(exception.getMessage(), exception);
    }

    public ServiceException(Throwable exception){
        super(exception);
    }

}
