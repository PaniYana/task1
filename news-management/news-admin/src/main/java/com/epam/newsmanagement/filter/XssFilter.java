package com.epam.newsmanagement.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by Yana_Valchok on 6/16/2016.
 */
public class XssFilter implements Filter {

    private static final String HEADER_NAME = "Content-Security-Policy";
    private static final String HEADER_VALUE = "default-src 'self';" +
            "                            style-src 'self'  " +
            "                            script-src 'self'";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletResponse response = (HttpServletResponse)servletResponse;
        response.addHeader(HEADER_NAME, HEADER_VALUE);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
