package com.epam.newsmanagement.exception;

/**
 * Created by Yana_Valchok on 6/10/2016.
 */
public class CommandException extends Exception {

    public CommandException() {
        super();
    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }
}
