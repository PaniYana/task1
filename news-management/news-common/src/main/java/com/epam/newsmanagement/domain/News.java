package com.epam.newsmanagement.domain;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

/**
 * Created by Yana_Valchok on 4/5/2016.
 */

/**
 * Domain class, representation of news entity
 */
public class News implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long version;
    private String title;
    private String shortText;
    private String fullText;
    private Timestamp creationDate;
    private Date modificationDate;
    private Set<Comment> comments;
    private Set<Tag> tags;
    private Set<Author> authors;

    public News(Long id, String title, String shortText, String fullText, Timestamp creationDate, Date modificationDate) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }

    public News() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if (this == obj){
            return true;
        }
        if (!(obj instanceof News)){
            return false;
        }

        News news = (News) obj;

        if (!id.equals(news.id)){
            return false;
        }
        if (!title.equals(news.title)){
            return false;
        }
        if (!shortText.equals(news.shortText)){
            return false;
        }
        if (!fullText.equals(news.fullText)){
            return false;
        }
        if (!creationDate.equals(news.creationDate)){
            return false;
        }
        if (!modificationDate.equals(news.modificationDate)){
            return false;
        }


        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + ((this.getId() == null) ? 0 :this.getId().hashCode());
        result = 31 * result + ((title ==  null) ? 0 : title.hashCode());
        result = 31 * result + ((shortText == null) ? 0 :shortText.hashCode());
        result = 31 * result + ((fullText == null) ? 0 : fullText.hashCode());
        result = 31 * result + ((creationDate == null) ? 0 :creationDate.hashCode());
        result = 31 * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
        return result;
    }
}
