package com.epam.newsmanagement.validation;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Yana_Valchok on 5/25/2016.
 */
@Component
public class CommentFormValidator implements Validator {

    private static final String TEXT = "text";
    private static final String NOT_EMPTY_TEXT = "NotEmpty.commentForm.text";
    private static final String REPLACE_REGEXP = "<[^>]*>";

    @Autowired
    CommentService commentService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Comment.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {

        Comment comment = (Comment) o;

        if(comment.getText() == null  || comment.getText().equals("")){
            errors.rejectValue(TEXT, NOT_EMPTY_TEXT);
        }else{
             comment.setText(comment.getText().replaceAll(REPLACE_REGEXP, ""));
        }
    }
}