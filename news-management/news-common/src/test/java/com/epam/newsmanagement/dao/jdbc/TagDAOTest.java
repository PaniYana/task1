package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Yana Volchok on 10.04.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:springconfigtest.xml")
@Transactional
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "/tag_test_data.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "/tag_test_data.xml", type = DatabaseOperation.DELETE_ALL)
@ActiveProfiles("jdbc")
public class TagDAOTest {

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Autowired
    TagDAO tagDAO;

    @Autowired
    NewsDAO newsDAO;


    @Test
    public void createTest() throws DAOException {
        Tag tag = new Tag();
        tag.setName("Tag Name");
        Long id = tagDAO.create(tag).getId();
        Assert.assertNotNull(id);
        List<Tag> tagList = tagDAO.getAll();
        Assert.assertEquals(3, tagList.size());
        Assert.assertEquals(tag.getName(), tagList.get(2).getName());
    }

    @Test
    public void getAllTest() throws DAOException{

        List<Tag> tagList = tagDAO.getAll();
        Assert.assertEquals(2, tagList.size());
    }

    @Test
    public void getByIdTest() throws DAOException{

        Tag tag = tagDAO.find(1L);
        Assert.assertEquals(1L, (long)tag.getId());
    }

    @Test
    public void updateTest() throws DAOException{

        Tag tag = new Tag(1L, "New Name");
        Long idTag = tagDAO.update(tag).getId();
        Assert.assertEquals(tag.getId(), idTag);
    }

    @Test
    public void deleteTest() throws DAOException{

        newsDAO.detachTagFromNews(1L, 1L);
        tagDAO.delete(1L);
    }

    @Test
    public void getTagsByNewsTest() throws DAOException{
        List<Tag> tagList = tagDAO.getTagsByNews(1L);
        Assert.assertEquals(2, tagList.size());
    }

    @Test
    public void createSeveralTest() throws DAOException{
        List<Tag> tagList = new ArrayList<Tag>();
        tagList.add(new Tag(1L, "New Name"));
        tagList.add(new Tag(2L, "New Name"));
        tagList.add(new Tag(3L, "New Name"));
        tagDAO.create(tagList);

        List<Tag> tags = tagDAO.getAll();
        Assert.assertEquals(5, tags.size());
        Assert.assertEquals(tagList.get(2).getName(), tags.get(2).getName());
    }
}
