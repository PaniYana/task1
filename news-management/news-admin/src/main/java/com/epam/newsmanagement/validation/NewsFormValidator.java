package com.epam.newsmanagement.validation;

import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.to.NewsAddTO;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Yana_Valchok on 5/18/2016.
 */
@Component
public class NewsFormValidator implements Validator {

    private static final String TITLE = "news.title";
    private static final String SHORT_TEXT = "news.shortText";
    private static final String FULL_TEXT = "news.fullText";
    private static final String AUTHOR_LIST = "authorList";
    private static final String NOT_EMPTY_TITLE = "NotEmpty.newsForm.news.title";
    private static final String NOT_EMPTY_SHORT_TEXT = "NotEmpty.newsForm.news.shortText";
    private static final String NOT_EMPTY_FULL_TEXT = "NotEmpty.newsForm.news.fullText";
    private static final String NOT_EMPTY_AUTHOR_LIST = "NotEmpty.newsForm.authorList";
    private static final String REPLACE_REGEXP = "<[^>]*>";

    @Autowired
    NewsService newsService;

    @Override
    public boolean supports(Class<?> clazz) {
        return NewsAddTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {

        NewsAddTO news = (NewsAddTO) o;

        if(news.getNews().getTitle() == null || news.getNews().getFullText().equals("")){
            errors.rejectValue(TITLE, NOT_EMPTY_TITLE);
        }else{
            news.getNews().setTitle((news.getNews().getTitle().replaceAll(REPLACE_REGEXP, "")));
        }

        if ( news.getNews().getShortText() == null || news.getNews().getShortText().equals("")) {
            errors.rejectValue(SHORT_TEXT, NOT_EMPTY_SHORT_TEXT);
        }else{
            news.getNews().setShortText((news.getNews().getShortText().replaceAll(REPLACE_REGEXP, "")));
        }

        if (news.getNews().getFullText() == null || news.getNews().getFullText().equals("")) {
            errors.rejectValue(FULL_TEXT, NOT_EMPTY_FULL_TEXT);
        }else{
            news.getNews().setFullText((news.getNews().getFullText().replaceAll(REPLACE_REGEXP, "")));
        }

        if (news.getAuthorList() == null) {
            errors.rejectValue(AUTHOR_LIST, NOT_EMPTY_AUTHOR_LIST);
        }
    }
}
