package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DAOException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 7/5/2016.
 */
public class TagDAOImpl extends GenericDAOImpl<Tag, Long> implements TagDAO {

    final String HQL_GET_TAG_BY_NEWS = "SELECT t FROM News n JOIN n.tags t WHERE n.id = :NT_NEWS_ID";

    public TagDAOImpl(Class<Tag> entityClass) {
        super(entityClass);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Tag> getTagsByNews(Long idNews) throws DAOException {
        try {
            Query query = getCurrentSession().createQuery(HQL_GET_TAG_BY_NEWS);
            query.setParameter("NT_NEWS_ID", idNews);
            List list = query.list();
            return (List<Tag>)list;
        }catch(Exception e){
            throw new DAOException("Method getTagsByNews in TagDAO", e);
        }
    }

    @Transactional
    @Override
    public List<Long> create(List<Tag> tags) throws DAOException {
        try {
            Session session = getCurrentSession();
            List<Long> idList = new ArrayList<Long>();
            session.beginTransaction();
            for(Tag tag: tags) {
                idList.add((Long)session.save(tag));
            }
            session.getTransaction().commit();
            return idList;
        }catch(Exception e){
            throw new DAOException("Method create in TagDAO", e);
        }
    }
}
