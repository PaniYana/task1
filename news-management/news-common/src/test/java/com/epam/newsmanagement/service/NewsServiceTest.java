package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/14/2016.
 */
public class NewsServiceTest {

    @InjectMocks
    private NewsService newsService = new NewsServiceImpl();

    @Mock
    private NewsDAO newsDAO;

    @Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testAdd() throws ServiceException {
        try {
            News news = new News(1L, "Title", "Short text", "Full text", new Timestamp(1460623612723L), new Date(1460623612723L));

            Mockito.when(newsDAO.create(news)).thenReturn(news);

            Long id = newsService.add(news);
            Assert.assertEquals(1L, (long)id);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testDelete() throws ServiceException {
        try {
            newsService.delete(1L);
            Mockito.verify(newsDAO).delete(1L);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGetAll() throws ServiceException{
        try{
            List<News> newsList = new ArrayList<News>(2);
            newsList.add(new News(1L, "Title", "Short text", "Full text", new Timestamp(1460623612723L), new Date(1460623612723L)));
            newsList.add(new News(2L, "Title", "Short text", "Full text", new Timestamp(1460623612723L), new Date(1460623612723L)));
            Mockito.when(newsService.getAll()).thenReturn(newsList);
            List<News> actualNewsList = newsService.getAll();
            Mockito.verify(newsDAO).getAll();
            Assert.assertEquals(newsList.size(), actualNewsList.size());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGet() throws ServiceException{
        try{
            News news = new News(1L, "Title", "Short text", "Full text", new Timestamp(1460623612723L), new Date(1460623612723L));
            Mockito.when(newsDAO.find(1L)).thenReturn(news);
            News actualNews = newsService.get(1L);
            Mockito.verify(newsDAO).find(1L);
            Assert.assertEquals(news.getId(), actualNews.getId());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testUpdate() throws ServiceException{
        try {
            News news = new News(1L, "Title", "Short text", "Full text", new Timestamp(1460623612723L), new Date(1460623612723L));
            Mockito.when(newsDAO.update(news)).thenReturn(news);
            Long id = newsService.update(news);
            Mockito.verify(newsDAO).update(news);
            Assert.assertEquals(news.getId(), id);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGetNewsCount() throws ServiceException{
        final int NEWS_COUNT = 3;
        try{
            Mockito.when(newsDAO.getNewsCount()).thenReturn(NEWS_COUNT);
            Integer idCount = newsService.getNewsCount();
            Assert.assertEquals(NEWS_COUNT, (int)idCount);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testAddAuthorToNews() throws ServiceException{
        try {
            newsService.attachAuthorToNews(1L, 1L);
            Mockito.verify(newsDAO).attachAuthorToNews(1L, 1L);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testAddTagToNews() throws ServiceException{
        try {
            newsService.attachTagToNews(1L, 1L);
            Mockito.verify(newsDAO).attachTagToNews(1L, 1L);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void findBySearchCriteriaTest() throws ServiceException{
        List<News> newsList = new ArrayList<News>(2);
        newsList.add(new News(1L, "Title", "Short text", "Full text", new Timestamp(1460623612723L), new Date(1460623612723L)));
        newsList.add(new News(2L, "Title", "Short text", "Full text", new Timestamp(1460623612723L), new Date(1460623612723L)));

        List<Long> tagList = new ArrayList<Long>();
        List<Long> authorList = new ArrayList<Long>();
        tagList.add(1L);
        tagList.add(2L);
        authorList.add(3L);
        authorList.add(4L);
        try {
            Mockito.when(newsDAO.find(Mockito.any(SearchCriteria.class))).thenReturn(newsList);
            List<News> actualNewsList = newsService.find(tagList, authorList);
            Mockito.verify(newsDAO).find(Mockito.any(SearchCriteria.class));
            Assert.assertEquals(2, actualNewsList.size());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testDeleteAuthorFromNews() throws ServiceException{
        try {
            newsService.detachAuthorFromNews(1L, 1L);
            Mockito.verify(newsDAO).detachAuthorFromNews(1L, 1L);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testDeleteTagFromNews() throws ServiceException{
        try {
            newsService.detachTagFromNews(1L, 1L);
            Mockito.verify(newsDAO).detachTagFromNews(1L, 1L);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testAddSeveralTagsToNews() throws ServiceException{
        try {
            List<Long> tagList = new ArrayList<Long>();
            tagList.add(1L);
            tagList.add(2L);
            tagList.add(3L);
            newsService.attachTagToNews(1L, tagList);
            Mockito.verify(newsDAO).attachTagToNews(1L, tagList);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }
}
