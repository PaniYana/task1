package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.IllegalParameterException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/12/2016.
 */

/**
 * Implementation of interface <tt>NewsService</tt>

 * @see NewsService
 */
@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsDAO newsDAO;

    private static final Logger LOGGER = LogManager.getLogger(AuthorServiceImpl.class);

    /**
     * Implementation of method {@link NewsService#add(Object)}
     * @see NewsService
     */
    @Transactional
    @Override
    public Long add(News news) throws ServiceException{
        Long createdId = null;
        if(news != null){
            if(news.getCreationDate() == null){
                news.setCreationDate(new Timestamp(new Date().getTime()));
                news.setModificationDate(new java.sql.Date(news.getCreationDate().getTime()));
            }
            try {
                createdId = newsDAO.create(news).getId();
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return createdId;
    }

    /**
     * Implementation of method {@link NewsService#getAll()}
     * @see NewsService
     */
    @Transactional
    @Override
    public List<News> getAll() throws ServiceException {
        List<News> newsList = null;
        try {
            newsList = newsDAO.getAll();
        }catch(DAOException e){
            throw new ServiceException(e.getMessage());
        }
        return newsList;
    }

    /**
     * Implementation of method {@link NewsService#get(Object)}
     * @see NewsService
     */
    @Transactional
    @Override
    public News get(Long id) throws ServiceException {
        News news = null;
        if(id != null){
            try {
                news = newsDAO.find(id);
            }catch(DAOException e) {
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return news;
    }

    /**
     * Implementation of method {@link NewsService#delete(Long)}
     * @see NewsService
     */
    @Transactional
    @Override
    public void delete(Long id) throws ServiceException{
        if(id != null){
            try{
                newsDAO.delete(id);
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
    }

    /**
     * Implementation of method {@link NewsService#update(News)}
     * @see NewsService
     */
    @Transactional
    @Override
    public Long update(News news) throws ServiceException, StaleObjectStateException {
        Long idNews = null;
        if(news != null){
            if(news.getModificationDate() == null){
                news.setModificationDate(new java.sql.Date(new Date().getTime()));
            }
            try {
                News newsEntity  = newsDAO.update(news);
                idNews = newsEntity.getId();
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return idNews;
    }

    /**
     * Implementation of method {@link NewsService#getNewsCount()}
     * @see NewsService
     */
    @Transactional
    @Override
    public int getNewsCount() throws ServiceException {
        try{
            return newsDAO.getNewsCount();
        }catch(DAOException e){
            LOGGER.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link NewsService#attachAuthorToNews(Long, Long)}
     * @see NewsService
     */
    @Transactional
    @Override
    public void attachAuthorToNews(Long idNews, Long idAuthor) throws ServiceException {
        try {
            if (idNews != null && idAuthor != null) {
                newsDAO.attachAuthorToNews(idNews, idAuthor);
            } else {
                throw new IllegalParameterException();
            }
        }catch(DAOException e){
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link NewsService#attachTagToNews(Long, Long)}
     * @see NewsService
     */
    @Transactional
    @Override
    public void attachTagToNews(Long idNews, Long idTag) throws ServiceException {
        try{
            if (idNews != null && idTag != null) {
                newsDAO.attachTagToNews(idNews, idTag);
            } else {
                throw new IllegalParameterException();
            }
        }catch(DAOException e){
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link NewsService#find(List, List)}
     * @see NewsService
     */
    @Transactional
    @Override
    public List<News> find(List<Long> tagList, List<Long> authorList) throws ServiceException{

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthorList(authorList);
        searchCriteria.setTagList(tagList);
        try{
            return newsDAO.find(searchCriteria);
        }catch(DAOException e){
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }


    /**
     * Implementation of method {@link NewsService#detachAuthorFromNews(Long, Long)}
     * @see NewsService
     */
    @Transactional
    @Override
    public void detachAuthorFromNews(Long idNews, Long idAuthor) throws ServiceException {
        try {
            if (idNews != null && idAuthor != null) {
                newsDAO.detachAuthorFromNews(idNews, idAuthor);
            } else {
                throw new IllegalParameterException();
            }
        }catch(DAOException e){
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link NewsService#detachTagFromNews(Long, Long)}
     * @see NewsService
     */
    @Transactional
    @Override
    public void detachTagFromNews(Long idNews, Long idTag) throws ServiceException {
        try {
            if (idNews != null && idTag != null) {
                newsDAO.detachTagFromNews(idNews, idTag);
            } else {
                throw new IllegalParameterException();
            }
        }catch(DAOException e){
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link NewsService#attachTagToNews(Long, List)}
     * @see NewsService
     */
    @Transactional
    @Override
    public void attachTagToNews(Long idNews, List<Long> tagList) throws ServiceException {
        if(idNews != null && tagList != null){
            try {
                newsDAO.attachTagToNews(idNews, tagList);
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }else{
            throw new IllegalParameterException();
        }
    }

    /**
     * Implementation of method {@link NewsService#getAllPaged(Integer, Integer)}
     * @see NewsService
     */
    @Transactional
    @Override
    public List<News> getAllPaged(Integer offset, Integer count) throws ServiceException {
        List<News> newsList = null;
        if(offset != null && count != null) {
            try {
                newsList = newsDAO.getAllPaged(offset, count);
            } catch (DAOException e) {
                throw new ServiceException(e.getMessage());
            }
            return newsList;
        }else{
            throw new IllegalParameterException();
        }
    }

    /**
     * Implementation of method {@link NewsService#attachAuthorToNews(Long, List)}
     * @see NewsService
     */
    @Transactional
    @Override
    public void attachAuthorToNews(Long idNews, List<Long> authorList) throws ServiceException {

        if(idNews != null && authorList != null){
            try {
                newsDAO.attachAuthorToNews(idNews, authorList);
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }else{
            throw new IllegalParameterException();
        }
    }

    /**
     * Implementation of method {@link NewsService#detachAuthorFromNews(Long, List)}
     * @see NewsService
     */
    @Transactional
    @Override
    public void detachAuthorFromNews(Long idNews, List<Long> idAuthorList) throws ServiceException {

        try {
            if (idNews != null && idAuthorList != null) {
                newsDAO.detachAuthorFromNews(idNews, idAuthorList);
            } else {
                throw new IllegalParameterException();
            }
        }catch(DAOException e){
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link NewsService#detachTagFromNews(Long, List)}
     * @see NewsService
     */
    @Transactional
    @Override
    public void detachTagFromNews(Long idNews, List<Long> idTagList) throws ServiceException {

        try {
            if (idNews != null && idTagList != null) {
                newsDAO.detachTagFromNews(idNews, idTagList);
            } else {
                throw new IllegalParameterException();
            }
        }catch(DAOException e){
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link NewsService#getPrevious(Long)}
     * @see NewsService
     */
    @Transactional
    @Override
    public News getPrevious(Long id) throws ServiceException {
        News news = null;
        Long prevId = null;
        if(id != null){
            try {
                prevId = newsDAO.getPrevious(id);
                if(prevId == 0){
                    news = get(id);
                }else {
                    news = get(prevId);
                }
            }catch(DAOException e) {
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return news;
    }

    /**
     * Implementation of method {@link NewsService#getNext(Long)}
     * @see NewsService
     */
    @Transactional
    @Override
    public News getNext(Long id) throws ServiceException {
        News news = null;
        Long nextId = null;
        if(id != null){
            try {
                nextId = newsDAO.getNext(id);
                if(nextId == 0) {
                    news = get(id);
                }else{
                    news = get(nextId);
                }
            }catch(DAOException e) {
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return news;
    }

    /**
     * Implementation of method {@link NewsService#find(List, List, int, int)}
     * @see NewsService
     */
    @Transactional
    @Override
    public List<News> find(List<Long> tagList, List<Long> authorList, int offset, int count) throws ServiceException{

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthorList(authorList);
        searchCriteria.setTagList(tagList);
        try{
            return newsDAO.find(searchCriteria, offset, count);
        }catch(DAOException e){
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }
}
