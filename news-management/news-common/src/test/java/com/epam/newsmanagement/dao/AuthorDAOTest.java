package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

/**
 * Created by Yana Volchok on 10.04.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:springconfigtest.xml")
@Transactional
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:author_test_data.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:author_test_data.xml", type = DatabaseOperation.DELETE_ALL)
@ActiveProfiles("hibernate")
public class AuthorDAOTest {

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Autowired
    AuthorDAO authorDAO;

    @Autowired
    NewsDAO newsDAO;

    @Test
    public void createTest() throws DAOException{
        Author author = new Author();
        author.setName("Name");
        author.setExpired(new Timestamp(18273696));

        Author createdAuthor = authorDAO.create(author);
        Assert.assertNotNull(createdAuthor.getId());

        List<Author> authorList = authorDAO.getAll();
        Assert.assertEquals(3, authorList.size());
        Author authorCreated = authorDAO.find(createdAuthor.getId());

        Assert.assertTrue(authorList.contains(authorCreated));
    }

    @Test
    public void getAllTest() throws DAOException{

        List<Author> authorList = authorDAO.getAll();
        Assert.assertEquals(2, authorList.size());
    }

    @Test
    public void getByIdTest() throws DAOException{

        Author author = authorDAO.find(1L);
        Assert.assertEquals(1L, (long)author.getId());
    }

    @Test
    public void updateTest() throws DAOException{

        Author author = new Author(1L, "Name", new Timestamp(18273696) );

        Author updatedAuthor = authorDAO.update(author);
        Assert.assertEquals(author.getId(), updatedAuthor.getId());

    }

    @Test
    public void deleteTest() throws DAOException{

        newsDAO.detachAuthorFromNews(1L, 1L);
        authorDAO.delete(1L);
    }

    @Test
    public void getAuthorByNewsTest() throws DAOException{
        List<Author> authorList = authorDAO.getAuthorsByNews(1L);
        Assert.assertEquals(1,authorList.size() );
    }

    @Test
    public void getNotExpiredListTest() throws DAOException{
        List<Author> authors = authorDAO.getNotExpiredList();
        Assert.assertNotNull(authors);
    }
}
