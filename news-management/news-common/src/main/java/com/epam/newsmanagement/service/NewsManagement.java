package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.domain.to.NewsAddTO;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.IllegalParameterException;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/15/2016.
 */

/**
 * Service that provides interaction between different services for entity management.
 */
public interface NewsManagement {

    /**
     * Provides with adding new news message with specifying its authors and tags.
     *
     * @param news news message to add
     * @param authorList list of authors that should be marked as authors of this news message
     * @param tagList list of tags that should be marked as tags of this news message
     * @return ID of added news message
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    Long add(News news, List<Long> authorList, List<Long> tagList) throws ServiceException;

    /**
     * Finds, builds and returns full news information (including list of authors, list of tags and list of comments)
     * @param idNews - unique key of news message to find
     * @return found full news information
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    NewsTO find(Long idNews) throws ServiceException;

    /**
     * Finds, builds and returns full news information for updating (including list of authors, list of tags and list of comments)
     * @param idNews - unique key of news message to find
     * @return found full news information
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    NewsAddTO findForUpdate(Long idNews) throws ServiceException;

    /**
     * Finds, builds and returns list of full news information (including list of authors,
     * list of tags and list of comments)
     * @return list of full news information
     * @throws ServiceException if DAOException was caught
     */
    List<NewsTO> findAll() throws ServiceException;

    /**
     * Finds, builds and returns limited list of full news information (including list of authors,
     * list of tags and list of comments).List is limited by page offset and amount of
     * records on page.
     * @return list of full news information
     * @throws ServiceException if DAOException was caught
     */
    List<NewsTO> findAllPaged(Integer offset, Integer count) throws ServiceException;

    /**
     * Provides with removing news message with removing its relations with its authors and tags
     * @param idNews unique key of news message to remove
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void delete(List<Long> idNews) throws ServiceException;

    /**
     * Provides with finding full news message information by authors and tags
     * @param authorList authors of news messages
     * @param tagList tags of news messages
     * @return list of found full news message information
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     *
     */
    List<NewsTO> find(List<Long> authorList, List<Long> tagList) throws ServiceException;

    /**
     * Allows to modify news message (including changing authors and tags).
     *
     * @param news entity to edit
     * @return ID of modified entity
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */

    Long update(NewsAddTO news) throws ServiceException;

    /**
     * Returns previous news concerning current.
     *
     * @param id - unique key of current news
     * @return previous news
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    NewsTO getPrevious(Long id) throws ServiceException;

    /**
     * Returns next news concerning current.
     *
     * @param id - unique key of current news
     * @return next news
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    NewsTO getNext(Long id) throws ServiceException;

    /**
     * Provides with finding news by authors and tags (paginated)
     * @param authorList authors of news messages
     * @param tagList tags of news messages
     * @param offset number of page
     * @param count number of news on the page
     * @return list of found news
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    List<NewsTO> find(List<Long> tagList, List<Long> authorList, int offset, int count) throws ServiceException;
}
