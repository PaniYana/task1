package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import junit.framework.Assert;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Yana_Valchok on 4/8/2016.
 */


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:springconfigtest.xml")
@Transactional
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:news_test_data.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:news_test_data.xml", type = DatabaseOperation.DELETE_ALL)
@ActiveProfiles("hibernate")
public class NewsDAOTest {

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Autowired
    NewsDAO newsDAO;

    @Autowired
    CommentDAO commentDAO;

    @Test
    public void createTest() throws DAOException{
        News news  = new News();
        //news.setId(3);
        news.setTitle("Title");
        news.setShortText("Short text");
        news.setFullText("Full text");
        news.setCreationDate(new Timestamp(1827364));
        news.setModificationDate(new Date(1827364));
        Long id = newsDAO.create(news).getId();
        Assert.assertNotNull(id);
    }

    @Test
    public void getAllTest() throws DAOException{

        List<News> newsList = newsDAO.getAll();
        Assert.assertEquals(2, newsList.size());
    }

    @Test
    public void findTest() throws DAOException{

        News news = newsDAO.find(1L);
        Assert.assertEquals(1L, (long)news.getId());
    }

    @Test
    public void updateTest() throws DAOException{

        News news = new News(1L, "New title", "New short text", "New full text", new Timestamp(18278696), new Date(18278696) );
        Long idNews = newsDAO.update(news).getId();
        org.junit.Assert.assertEquals(news.getId(), idNews);
        //org.junit.Assert.assertEquals(news.getTitle(), updatedNews.getTitle());
    }

    @Test
    public void deleteTest() throws DAOException{

        newsDAO.detachAuthorFromNews(1L, 1L);
        newsDAO.detachAuthorFromNews(1L, 2L);
        newsDAO.detachTagFromNews(1L, 2L);
        newsDAO.detachTagFromNews(1L, 1L);
        commentDAO.delete(1L);
        newsDAO.delete(1L);
    }

    @Test
    public void attachAuthorToNewsTest() throws DAOException{

        newsDAO.attachAuthorToNews(1L, 2L);
    }

    @Test
    public void attachTagToNewsTest() throws DAOException{

       newsDAO.attachTagToNews(1L, 2L);
    }

    @Test
    public void detachAuthorFromNewsTest() throws DAOException{

        newsDAO.detachAuthorFromNews(1L, 1L);
    }

    @Test
    public void detachTagFromNewsTest() throws DAOException{

        newsDAO.detachTagFromNews(1L, 1L);
    }

    @Test
    public void findBySearchCriteriaTest() throws DAOException{

        SearchCriteria searchCriteria = new SearchCriteria();
        List<Long> authorList = new ArrayList<Long>();
        authorList.add(1L);
        authorList.add(2L);
        List<Long> tagList = new ArrayList<Long>();
        tagList.add(1L);
        tagList.add(2L);

        searchCriteria.setAuthorList(authorList);
        searchCriteria.setTagList(tagList);

        List<News> newsList = newsDAO.find(searchCriteria);
        Assert.assertEquals(2, newsList.size());
    }

    @Test
    public void getNewsCountTest() throws DAOException{

        int count = newsDAO.getNewsCount();
        org.junit.Assert.assertEquals(2, count);
    }

    @Test
    public void getNewByAuthorTest() throws DAOException{
        List<News> newsList = newsDAO.getNewsByAuthor(1L);
        Assert.assertEquals(1, newsList.size());
    }

    // TODO
    @Test
    public void getAllPagedTest() throws DAOException{

        List<News> newsList = newsDAO.getAllPaged(1,2);
        Assert.assertEquals(2, newsList.size());
    }

    @Test
    public void attachSeveralAuthorToNewsTest() throws DAOException{

        List<Long> authorList = new ArrayList<>();
        authorList.add(1L);
        authorList.add(2L);
        newsDAO.attachAuthorToNews(1L, authorList);
    }

    @Test
    public void attachSeveralTagToNewsTest() throws DAOException{

        List<Long> tagList = new ArrayList<>();
        tagList.add(1L);
        tagList.add(2L);
        newsDAO.attachTagToNews(1L, tagList);
    }

    @Test
    public void detachSeveralAuthorFromNewsTest() throws DAOException{

        List<Long> authorList = new ArrayList<>();
        authorList.add(1L);
        authorList.add(2L);
        newsDAO.detachAuthorFromNews(1L, authorList);
    }

    @Test
    public void detachSeveralTagFromNewsTest() throws DAOException{

        List<Long> tagList = new ArrayList<>();
        tagList.add(1L);
        tagList.add(2L);
        newsDAO.detachTagFromNews(1L, tagList);
    }

    @Test
    public void getPrevTest() throws DAOException{
        Long result = newsDAO.getPrevious(2L);
        Assert.assertNotNull(result);
    }

    @Test
    public void getNextTest() throws DAOException{
        Long result = newsDAO.getNext(1L);
        Assert.assertNotNull(result);
    }

    @Test
    public void findBySearchCriteriaPagedTest() throws DAOException{

        SearchCriteria searchCriteria = new SearchCriteria();
        List<Long> authorList = new ArrayList<Long>();
        authorList.add(1L);
        authorList.add(2L);
        List<Long> tagList = new ArrayList<Long>();
        tagList.add(1L);
        tagList.add(2L);

        searchCriteria.setAuthorList(authorList);
        searchCriteria.setTagList(tagList);

        int offset = 1;
        int count = 2;
        List<News> newsList = newsDAO.find(searchCriteria, offset, count);
        Assert.assertEquals(2, newsList.size());


    }
}
