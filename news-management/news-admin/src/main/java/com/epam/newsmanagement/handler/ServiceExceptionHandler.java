package com.epam.newsmanagement.handler;

import com.epam.newsmanagement.exception.ServiceException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.StaleObjectStateException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Yana_Valchok on 6/6/2016.
 */
@ControllerAdvice
public class ServiceExceptionHandler {

    private static Logger LOGGER = LogManager.getLogger(ServiceExceptionHandler.class);

    private static final String DEFAULT_ERROR_VIEW = "error-404";
    private static final String ROLE_ERROR_VIEW = "error-403";
    private static final String EXCEPTION = "exception";
    private static final String UPDATE_NEWS_PAGE = "/edit";
    private static final String MESSAGE = "Current news is updating. Try later.";
    private static final String MESSAGE_NAME = "message";
    private static final String ID = "id";

    @ExceptionHandler(value = ServiceException.class)
    public ModelAndView defaultErrorHandler(ServiceException e) throws Exception {

        LOGGER.error(e);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(EXCEPTION, e);
        modelAndView.setViewName(DEFAULT_ERROR_VIEW);
        return modelAndView;
    }

    @ExceptionHandler(value = UsernameNotFoundException.class)
    public ModelAndView authenticationErrorHandler(UsernameNotFoundException e) throws Exception {

        LOGGER.error(e);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(EXCEPTION, e);
        modelAndView.setViewName(ROLE_ERROR_VIEW);
        return modelAndView;
    }

    @ExceptionHandler(value = StaleObjectStateException.class)
    public ModelAndView optimisticLockErrorHandler(@PathVariable(ID) Long id ,StaleObjectStateException e)
            throws Exception {

        LOGGER.error(e);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(MESSAGE_NAME, MESSAGE);
        StringBuilder viewName = new StringBuilder();
        viewName.append("/");
        viewName.append(id);
        viewName.append(UPDATE_NEWS_PAGE);
        modelAndView.setViewName(viewName.toString());
        return modelAndView;
    }
}
