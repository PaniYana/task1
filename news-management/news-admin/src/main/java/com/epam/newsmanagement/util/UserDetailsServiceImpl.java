package com.epam.newsmanagement.util;

import com.epam.newsmanagement.domain.Role;
import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.RoleService;
import com.epam.newsmanagement.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 6/3/2016.
 */
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger LOGGER = LogManager.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = null;
        Role role = null;
        Long userRole = null;
        try {
            user = userService.findByLogin(s);
            if(user != null) {
               userRole = user.getRoleId();
            }
            role = roleService.get(userRole);
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        List<GrantedAuthority> authorityList = new ArrayList<GrantedAuthority>();
        if (role != null) {
            authorityList.add(new SimpleGrantedAuthority(role.getName().toString().toUpperCase()));
        }else{
            throw new UsernameNotFoundException("Such user has no authorities: " + s);
        }
        if (user != null) {
            return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), authorityList);
        }else{
            throw new UsernameNotFoundException("No such user:" + s);
        }
    }
}
