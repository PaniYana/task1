package com.epam.newsmanagement.domain;

import java.io.Serializable;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */
/**
 * Domain class, representation of user entity
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private String login;
    private String password;
    private Long roleId;

    public User(Long id, String name, String login, String password, Long roleId) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
        this.roleId = roleId;
    }

    public User() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if (this == obj){
            return true;
        }
        if (!(obj instanceof User)){
            return false;
        }

        User user = (User) obj;

        if (!id.equals(user.id)) {
            return false;
        }
        if (!name.equals(user.name)){
            return false;
        }
        if (!login.equals(user.login)){
            return false;
        }
        if (!password.equals(user.password)){
            return false;
        }
        if (!roleId.equals(user.roleId)){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + roleId.hashCode();
        return result;
    }
}
