package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Implementation of interface <tt>AuthorDAO</tt>
 * Adapted to the work with Oracle database.
 * @see AuthorDAO
 */
@Repository
@Profile("jdbc")
public class AuthorDAOImpl implements AuthorDAO {

    private static final String AU_AUTHOR_ID = "AU_AUTHOR_ID";
    private static final String AU_AUTHOR_NAME = "AU_AUTHOR_NAME";
    private static final String AU_EXPIRED = "AU_EXPIRED";

    private static final String SQL_GET_BY_ID = "SELECT AU_AUTHOR_ID, AU_AUTHOR_NAME, AU_EXPIRED FROM AUTHORS WHERE AU_AUTHOR_ID = ?";
    private static final String SQL_GET_ALL = "SELECT AU_AUTHOR_ID, AU_AUTHOR_NAME, AU_EXPIRED FROM AUTHORS";
    private static final String SQL_CREATE = "INSERT INTO AUTHORS (AU_AUTHOR_ID, " +
            "AU_AUTHOR_NAME, AU_EXPIRED) VALUES (AUTHORS_SEQ.nextval, ? ,?)";
    private static final String SQL_UPDATE =
            "UPDATE AUTHORS SET AU_AUTHOR_NAME = ?, AU_EXPIRED = ? WHERE AU_AUTHOR_ID = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM AUTHORS WHERE AU_AUTHOR_ID = ?";
    private static final String SQL_GET_NOT_EXPIRED = "SELECT AU_AUTHOR_ID, AU_AUTHOR_NAME, AU_EXPIRED FROM AUTHORS WHERE AU_EXPIRED >= CURRENT_TIMESTAMP OR AU_EXPIRED IS NULL";
    private static final String SQL_GET_AUTHOR_BY_NEWS = "SELECT AU_AUTHOR_ID, AU_AUTHOR_NAME, AU_EXPIRED FROM AUTHORS JOIN NEWS_AUTHORS ON (AU_AUTHOR_ID = NA_AUTHOR_ID) WHERE NA_NEWS_ID = ?";

    @Autowired
    private DataSource datasource;

    /**
     * Implementation of method {@link AuthorDAO#find(Object)}
     * @see AuthorDAO
     */
    public Author find(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_BY_ID);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();

            Author author = null;
            if(resultSet.next()){
                author = buildEntity(resultSet);
            }
            return author;
        }catch(SQLException e){
            throw new DAOException("Method find in AuthorDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link AuthorDAO#getAll()}
     * @see AuthorDAO
     */
    public List<Author> getAll() throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Author> authorList;

        try{
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_ALL);
            resultSet = statement.executeQuery();

            authorList = new ArrayList<Author>();
            Author author;
            while(resultSet.next()){
                author = buildEntity(resultSet);

                authorList.add(author);
            }
            return authorList;
        }catch(SQLException e){
            throw new DAOException("Method getAll in AuthorDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link AuthorDAO#create(Object)}
     * @see AuthorDAO
     */
    public Author create(Author author) throws DAOException {

        final String[] keys = {"AU_AUTHOR_ID"};

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_CREATE,keys);
            statement.setString(1, author.getName());
            statement.setTimestamp(2, author.getExpired());

            statement.executeUpdate();
            Long id = -1L;

            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            return find(id);
        }catch(SQLException e){
            throw new DAOException("Method create in AuthorDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link AuthorDAO#update(Object)}
     * @see AuthorDAO
     */
    public Author update(Author author) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, author.getName());
            statement.setTimestamp(2, author.getExpired());
            statement.setLong(3, author.getId());

            statement.executeUpdate();

            return author;
        }catch(SQLException e){
            throw new DAOException("Method update in AuthorDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link AuthorDAO#delete(Object)} )}
     * @see AuthorDAO
     */
    public void delete(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_DELETE_BY_ID);
            statement.setLong(1, id);

            statement.executeUpdate();

        }catch(SQLException e){
            throw new DAOException("Method delete in AuthorDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link AuthorDAO#getNotExpiredList()}
     * @see AuthorDAO
     */
    public List<Author> getNotExpiredList() throws DAOException{

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Author> authorList;

        try{
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_NOT_EXPIRED);
            resultSet = statement.executeQuery();

            authorList = new ArrayList<Author>();
            Author author;
            while(resultSet.next()){
                author = buildEntity(resultSet);
                authorList.add(author);
            }
            return authorList;
        }catch(SQLException e){
            throw new DAOException("Method getNotExpiredList in AuthorDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    private Author buildEntity(ResultSet resultSet) throws SQLException{
        Author author = new Author();
        author.setId(resultSet.getLong(AU_AUTHOR_ID));
        author.setName(resultSet.getString(AU_AUTHOR_NAME));
        author.setExpired(resultSet.getTimestamp(AU_EXPIRED));
        return author;
    }

    /**
     * Implementation of method {@link AuthorDAO#getAuthorsByNews(Long)}
     * @see AuthorDAO
     */
    public List<Author> getAuthorsByNews(Long idNews) throws DAOException {

        List<Author> authorList = null;

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_AUTHOR_BY_NEWS);
            statement.setLong(1, idNews);
            resultSet = statement.executeQuery();

            Author author = null;
            authorList = new ArrayList<Author>();
            while(resultSet.next()){
                author = buildEntity(resultSet);
                authorList.add(author);
            }
            return authorList;
        }catch(SQLException e){
            throw new DAOException("Method getAuthorsByNews in AuthorDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }
}
