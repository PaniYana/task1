package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Role;

/**
 * Created by Yana_Valchok on 4/15/2016.
 */

/**
 * Extension of GenericService, parametrized by entity <tt>Role</tt> and key type <tt>Long</tt>
 *
 * @see GenericService
 */
public interface RoleService extends GenericService<Role, Long> {
}
