package com.epam.newsmanagement.domain.to;

import com.epam.newsmanagement.domain.Role;
import com.epam.newsmanagement.domain.User;

import java.io.Serializable;

/**
 * Created by Yana_Valchok on 4/13/2016.
 */
/**
 * Domain class that storage, transfer and represent full information about user
 * (including role)
 */
public class UserTO implements Serializable {

    public static final long serialVersionUID = 1L;
    private User user;
    private Role role;

    public UserTO(User user, Role role) {
        this.user = user;
        this.role = role;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if (this == obj){
            return true;
        }
        if (!(obj instanceof UserTO)){
            return false;
        }

        UserTO userTO = (UserTO) obj;

        if (!getUser().equals(userTO.getUser())){
            return false;
        }
        if(getRole().equals(userTO.getRole())){
            return false;
        }
        return true;

    }

    @Override
    public int hashCode() {
        int result = getUser().hashCode();
        result = 31 * result + getRole().hashCode();
        return result;
    }
}
