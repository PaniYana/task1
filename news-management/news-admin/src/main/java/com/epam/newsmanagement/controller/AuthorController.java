package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.validation.AuthorFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 5/24/2016.
 */

@Controller
@RequestMapping("/author")
public class AuthorController {

    private static final String LIST_PAGE = "author-list";
    private static final String REDIRECT = "redirect:/author";
    private static final String AUTHOR_FORM = "authorForm";
    private static final String AUTHOR_LIST = "authorList";
    private static final String AUTHOR_TO_EXPIRE = "authorToExpire";
    private static final String LIST_URL = "/list";
    private static final String ADD_URL = "/add";
    private static final String UPDATE_URL = "/update";
    private static final String EXPIRE_URL = "/expire";

    @Autowired
    private AuthorService authorService;

    @Autowired
    private AuthorFormValidator authorFormValidator;

    @InitBinder(AUTHOR_FORM)
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(authorFormValidator);
    }

    @RequestMapping(value = LIST_URL, method = RequestMethod.GET)
    public ModelAndView getAllAuthors () throws ServiceException {

        ModelAndView modelAndView = new ModelAndView(LIST_PAGE);
        List<Author> authorList = authorService.getNotExpiredList();
        Author author = new Author();
        modelAndView.addObject(AUTHOR_LIST, authorList);
        modelAndView.addObject(AUTHOR_FORM, author);
        modelAndView.addObject(AUTHOR_TO_EXPIRE, author);
        return modelAndView;
    }

    @RequestMapping(value = ADD_URL, method = RequestMethod.POST)
    public ModelAndView saveAuthor(@ModelAttribute(AUTHOR_LIST) @Validated Author author,
                                 BindingResult result) throws ServiceException{

        ModelAndView modelAndView = new ModelAndView(REDIRECT  + LIST_URL);;
        if(!result.hasErrors()){
            authorService.add(author);
        }else{
            modelAndView.addObject(AUTHOR_LIST, author);
            return modelAndView;
        }
        return modelAndView;
    }

    @RequestMapping(value = UPDATE_URL, method = RequestMethod.POST)
    public ModelAndView updateAuthor(@ModelAttribute(AUTHOR_FORM) @Validated Author author,
                                   BindingResult result) throws ServiceException{

        ModelAndView modelAndView = new ModelAndView(REDIRECT  + LIST_URL);;
        if(!result.hasErrors()){
            authorService.update(author);
        }else{
            modelAndView.addObject(AUTHOR_FORM, author);
            return modelAndView;
        }
        return modelAndView;
    }

    @RequestMapping(value = EXPIRE_URL, method = RequestMethod.POST)
    public ModelAndView expireAuthor(@ModelAttribute(AUTHOR_TO_EXPIRE) Author author,
                                     BindingResult result) throws ServiceException{

        ModelAndView modelAndView = new ModelAndView(REDIRECT  + LIST_URL);
        if(!result.hasErrors()){
           authorService.setExpired(author.getId());
        }else{
            modelAndView.addObject(AUTHOR_TO_EXPIRE, author);
            return modelAndView;
        }
        return modelAndView;
    }
}
