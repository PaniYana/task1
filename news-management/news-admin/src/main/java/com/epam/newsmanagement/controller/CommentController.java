package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.validation.CommentFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

/**
 * Created by Yana_Valchok on 5/25/2016.
 */

@Controller
@RequestMapping("/comment")
public class CommentController {

    private static final String REDIRECT = "redirect:/news";
    private static final String SHOW_PAGE = "/show";
    private static final String COMMENT_FORM = "commentForm";
    private static final String ADD_URL = "/add";
    private static final String DELETE_URL = "/delete";
    private static final String COMMENT_TO_DELETE = "commentToDelete";

    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentFormValidator commentFormValidator;

    @InitBinder(COMMENT_FORM)
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(commentFormValidator);
    }

    @RequestMapping(value = ADD_URL, method = RequestMethod.POST)
    public ModelAndView saveComment(@ModelAttribute(COMMENT_FORM) @Validated Comment comment,
                                 BindingResult result) throws ServiceException {

        ModelAndView modelAndView = new ModelAndView(REDIRECT + "/" + comment.getNews().getId() + SHOW_PAGE);
        if(!result.hasErrors()){
            commentService.add(comment);
        }

        return modelAndView;
    }

    @RequestMapping(value = DELETE_URL, method = RequestMethod.POST)
    public ModelAndView deleteComment(@ModelAttribute(COMMENT_TO_DELETE) @Validated Comment comment,
                                    BindingResult result) throws ServiceException {

        ModelAndView modelAndView = new ModelAndView(REDIRECT + "/" + comment.getNews().getId() + SHOW_PAGE);
        if(!result.hasErrors()){
            commentService.delete(comment.getId());
        }

        return modelAndView;
    }
}
