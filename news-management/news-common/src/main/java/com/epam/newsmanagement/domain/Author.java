package com.epam.newsmanagement.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Domain class, representation of author entity
 */
public class Author implements Serializable{

    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private Timestamp expired;
    private Set<News> news;

    public Author(Long id, String name, Timestamp expired) {
        this.id = id;
        this.name = name;
        this.expired = expired;
    }

    public Author() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    public Set<News> getNews() {
        return news;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }


    @Override
    public boolean equals(Object obj) {

        if(obj == null){
            return false;
        }

        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Author)) {
            return false;
        }

        Author author = (Author)obj;

        if (!id.equals(author.id)){
            return false;
        }

        if (!name.equals(author.name)) {
            return false;
        }

        if (!expired.equals(author.expired)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + expired.hashCode();
        return result;
    }
}
