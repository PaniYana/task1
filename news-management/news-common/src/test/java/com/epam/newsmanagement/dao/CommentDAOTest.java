package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Yana Volchok on 10.04.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:springconfigtest.xml")
@Transactional
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "/comment_test_data.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "/comment_test_data.xml", type = DatabaseOperation.DELETE_ALL)
@ActiveProfiles("hibernate")
public class CommentDAOTest {

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Autowired
    CommentDAO commentDAO;

    @Test
    public void createTest() throws DAOException {
        News news = new News(1L, "Title", "Short text" , "Full text" , new Timestamp(1460623612723L), new Date(1460623612723L));

        Comment comment = new Comment();
        comment.setNews(news);
        comment.setText("Comment text");
        comment.setCreationDate(new Timestamp(18278696));
        Long id = commentDAO.create(comment).getId();
        Assert.assertNotNull(id);
        List<Comment> commentList = commentDAO.getAll();
        Assert.assertEquals(3, commentList.size());  //TODO:
        Assert.assertEquals(comment.getText(), commentList.get(2).getText());
    }

    @Test
    public void getAllTest() throws DAOException{

        List<Comment> commentList = commentDAO.getAll();
        Assert.assertEquals(2, commentList.size());
    }

    @Test
    public void findTest() throws DAOException{

        Comment comment = commentDAO.find(1L);
        Assert.assertEquals(1L, (long)comment.getId());
    }

    @Test
    public void updateTest() throws DAOException{
        News news = new News(1L, "Title", "Short text" , "Full text" , new Timestamp(1460623612723L), new Date(1460623612723L));

        Comment comment = new Comment(1L, news, "New comment text", new Timestamp(18278696) );
        Long idComment = commentDAO.update(comment).getId();
        Assert.assertEquals(comment.getId(), idComment);
    }

    @Test
    public void deleteTest() throws DAOException{

        commentDAO.delete(1L);
    }

    @Test
    public void getByNewsIdTest() throws DAOException{

        List<Comment> commentList = commentDAO.getByNewsId(1L);
        Assert.assertEquals(2, commentList.size());
    }


    @Test
    public void createSeveralTest() throws DAOException {
        News news = new News(1L, "Title", "Short text" , "Full text" , new Timestamp(1460623612723L), new Date(1460623612723L));

        List<Comment> comments = new ArrayList<Comment>();
        for(int i = 0; i < 3; i++) {
            Comment comment = new Comment();
            comment.setNews(news);
            comment.setText("Comment text");
            comment.setCreationDate(new Timestamp(18278696));
            comments.add(comment);
        }
        commentDAO.create(comments);

        List<Comment> commentList = commentDAO.getAll();
        Assert.assertEquals(5, commentList.size());
        Assert.assertEquals(comments.get(2).getText(), commentList.get(2).getText());
    }

    @Test
    public void deleteSeveralTest() throws DAOException{
        List<Long> idList = new ArrayList<Long>();
        idList.add(1L);
        idList.add(2L);
        commentDAO.delete(idList);
    }
}
