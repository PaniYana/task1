package com.epam.newsmanagement.validation;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.servlet.mvc.condition.ParamsRequestCondition;

/**
 * Created by Yana_Valchok on 5/24/2016.
 */
@Component
public class AuthorFormValidator implements Validator {

    private static final String NAME = "name";
    private static final String NOT_EMPTY_NAME = "NotEmpty.authorForm.name";
    private static final String REPLACE_REGEXP = "<[^>]*>";

    @Autowired
    AuthorService authorService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Author.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {

        Author author = (Author) o;

        if(author.getName() == null || author.getName().equals("")){
            errors.rejectValue(NAME, NOT_EMPTY_NAME);
        }else{
            author.setName(author.getName().replaceAll(REPLACE_REGEXP, ""));
        }
    }
}