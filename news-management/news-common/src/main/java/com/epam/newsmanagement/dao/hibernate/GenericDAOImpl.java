package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.GenericDAO;
import com.epam.newsmanagement.exception.DAOException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Yana_Valchok on 7/1/2016.
 */
public abstract class GenericDAOImpl<E, L extends Serializable>  extends
        HibernateDaoSupport implements GenericDAO<E, L> {

    protected Class<E> entityClass;

    protected Session getCurrentSession(){
        return getSessionFactory().getCurrentSession();
    }

    public GenericDAOImpl(){}

    public GenericDAOImpl(Class<E> entityClass){
        this.entityClass = entityClass;
    }

    @Override
    @Transactional(readOnly = true)
    public List<E> getAll() throws DAOException
    {
        try {
            Session session = getCurrentSession();
            Criteria criteria = session.createCriteria(entityClass);
            return criteria.list();
        }catch(Exception e){
            throw new DAOException("Method getAll in DAO", e);
        }
    }


    @Override
    @Transactional(readOnly = true)
    public E find(L id) throws DAOException
    {
        try {
            Session session = getCurrentSession();
            return (E)session.get(entityClass, id);
        }catch(Exception e){
            throw new DAOException("Method find in DAO", e);
        }
    }


    @Override
    @Transactional
    public E create(E entity) throws DAOException
    {
        try {
            Session session = getCurrentSession();
            session.saveOrUpdate(entity);
            session.flush();
            return entity;
        }catch(Exception e){
            throw new DAOException("Method create in DAO", e);
        }
    }

    @Override
    @Transactional
    public E update(E entity) throws DAOException, StaleObjectStateException {

       /* int oldVersion = foo.getVersion();
        session.load( foo, foo.getKey() );
        if ( oldVersion!=foo.getVersion ) throw new StaleObjectStateException();
        foo.setProperty("bar");
        session.flush();
        session.connection().commit();*/

        try {
            Session session = getCurrentSession();
            session.merge(entity);
            session.flush();
            return entity;
        }catch(StaleObjectStateException e){
            throw e;
        }catch(Exception e){
            throw new DAOException("Method update in DAO", e);
        }
    }

    @Override
    @Transactional
    public void delete(L id) throws DAOException
    {
        try {
            Session session = getCurrentSession();
            E entity = find(id);
            session.delete(entity);
        }catch(Exception e){
            throw new DAOException("Method delete in DAO", e);
        }
    }
}
