package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/7/2016.
 */

/**
 * Implementation of interface <tt>CommentDAO</tt>
 * Adapted to the work with Oracle database.
 * @see CommentDAO
 */
@Repository
@Profile("jdbc")
public class CommentDAOImpl implements CommentDAO {

    private static final String COMMENT_ID = "COM_COMMENT_ID";
    private static final String NEWS_ID = "COM_NEWS_ID";
    private static final String COMMENT_TEXT = "COM_COMMENT_TEXT";
    private static final String CREATION_DATE = "COM_CREATION_DATE";

    private static final String SQL_GET_BY_ID =
            "SELECT COM_COMMENT_ID, COM_NEWS_ID, COM_COMMENT_TEXT, COM_CREATION_DATE FROM COMMENTS WHERE COM_COMMENT_ID = ?";
    private static final String SQL_GET_ALL =
            "SELECT COM_COMMENT_ID, COM_NEWS_ID, COM_COMMENT_TEXT, COM_CREATION_DATE FROM COMMENTS";
    private static final String SQL_CREATE =
            "INSERT INTO COMMENTS (COM_COMMENT_ID, COM_NEWS_ID, COM_COMMENT_TEXT, COM_CREATION_DATE) VALUES (COMMENTS_SEQ.nextval, ?, ?,?)";
    private static final String SQL_UPDATE =
            "UPDATE COMMENTS SET COM_COMMENT_TEXT = ?, COM_CREATION_DATE = ?  WHERE COM_COMMENT_ID = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM COMMENTS WHERE COM_COMMENT_ID = ?";
    private static final String SQL_GET_BY_NEWS_ID = "SELECT COM_COMMENT_ID, COM_NEWS_ID, COM_COMMENT_TEXT, COM_CREATION_DATE FROM COMMENTS WHERE COM_NEWS_ID = ?";


    @Autowired
    private DataSource datasource;

    @Autowired
    NewsDAO newsDAO;

    /**
     * Implementation of method {@link CommentDAO#find(Object)}
     * @see CommentDAO
     */
    public Comment find(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_BY_ID);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();

            Comment comment = null;
            if(resultSet.next()){
                comment = buildEntity(resultSet);
            }
            return comment;
        }catch(SQLException e){
            throw new DAOException("Method find in CommentDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }


    /**
     * Implementation of method {@link CommentDAO#getAll()}
     * @see CommentDAO
     */
    public List<Comment> getAll() throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Comment> commentList;

        try{
            connection = DataSourceUtils.doGetConnection( datasource);
            statement = connection.prepareStatement(SQL_GET_ALL);
            resultSet = statement.executeQuery();

            commentList = new ArrayList<Comment>();
            Comment comment;
            while(resultSet.next()){
                comment = buildEntity(resultSet);
                commentList.add(comment);
            }
            return commentList;
        }catch(SQLException e){
            throw new DAOException("Method getAll in CommentDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link CommentDAO#create(Object)}
     * @see CommentDAO
     */
    public Comment create(Comment comment) throws DAOException {

        final String[] keys = {"COM_COMMENT_ID"};

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_CREATE, keys);
            statement.setLong(1, comment.getNews().getId());
            statement.setString(2, comment.getText());
            statement.setTimestamp(3, comment.getCreationDate());

            statement.executeUpdate();
            Long id = -1L;
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            return find(id);
        }catch(SQLException e){
            throw new DAOException("Method create in CommentDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link CommentDAO#update(Object)} )}
     * @see CommentDAO
     */
    public Comment update(Comment comment) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, comment.getText());
            statement.setTimestamp(2, comment.getCreationDate());
            statement.setLong(3, comment.getId());

            statement.executeUpdate();
            return comment;

        }catch(SQLException e){
            throw new DAOException("Method update in CommentDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link CommentDAO#delete(Object)}
     * @see CommentDAO
     */
    public void delete(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_DELETE_BY_ID);
            statement.setLong(1, id);

            statement.executeUpdate();

        }catch(SQLException e){
            throw new DAOException("Method delete in CommentDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    private Comment buildEntity(ResultSet resultSet) throws SQLException, DAOException{
        Comment comment = new Comment();
        comment.setId(resultSet.getLong(COMMENT_ID));
        comment.setText(resultSet.getString(COMMENT_TEXT));
        comment.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
        Long newsId = resultSet.getLong(NEWS_ID);
        News news = newsDAO.find(newsId);
        comment.setNews(news);
        return comment;
    }

    /**
     * Implementation of method {@link CommentDAO#getByNewsId(Long)}
     * @see CommentDAO
     */
    public List<Comment> getByNewsId(Long idNews) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Comment> commentList;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_BY_NEWS_ID);
            statement.setLong(1, idNews);
            resultSet = statement.executeQuery();

            commentList = new ArrayList<Comment>();
            Comment comment;
            while(resultSet.next()){
                comment = buildEntity(resultSet);
                commentList.add(comment);
            }
            return commentList;
        }catch(SQLException e){
            throw new DAOException("Method getByNewsId in CommentDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link CommentDAO#create(List)}
     * @see CommentDAO
     */
    @Override
    public void create(List<Comment> comments) throws DAOException {

        final String[] keys = {"COM_COMMENT_ID"};

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_CREATE, keys);

            for(Comment item: comments) {
                statement.setLong(1, item.getNews().getId());
                statement.setString(2, item.getText());
                statement.setTimestamp(3, item.getCreationDate());

                statement.addBatch();
            }

            statement.executeBatch();

        }catch(SQLException e){
            throw new DAOException("Method create in CommentDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link CommentDAO#delete(List)}
     * @see CommentDAO
     */
    @Override
    public void delete(List<Long> idList) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_DELETE_BY_ID);
            for(Long id : idList) {
                statement.setLong(1, id);

                statement.addBatch();
            }
            statement.executeBatch();

        }catch(SQLException e){
            throw new DAOException("Method delete in CommentDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }
}
