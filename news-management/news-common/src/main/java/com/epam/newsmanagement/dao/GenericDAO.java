package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.exception.DAOException;


import java.sql.SQLException;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/5/2016.
 */

/**
 * Interface that allows user to work with DBMS with main CRUD operations
 * Can be parametrized by database entity and unique key type for determine certain DB table.
 *
 * @param <E> entity parameter
 * @param <L> key parameter (ID)
 */
public interface GenericDAO<E, L> {

    /**
     * Represents full list of entities in certain table
     *
     * @return list of entities
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    List<E> getAll() throws DAOException;

    /**
     * Inserts entity into certain table.
     *
     * @param entity to insert
     * @return key of inserted entity
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    E create(E entity) throws DAOException;

    /**
     * Modifies entity in database table with given parameters.
     * Entity is sought by ID in input entity.
     *
     * @param entity to modify
     * @return ID of modified entity
     * @throws DAOException if there were errors in the run-time
     */
    E update(E entity) throws DAOException;

    /**
     * Removes entity that was found by unique key (ID).
     * @param id to find entity
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    void delete(L id) throws DAOException;

    /**
     * Returns entity that was found by unique key (ID).
     * @param id to find certain entity
     * @return found entity
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    E find(L id) throws DAOException;
}
