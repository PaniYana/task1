$(document).ready(function() {
    var newsCount = $('#newsCount').val();
    var pageCount;
    var offset = $('#offset').val();
    if(newsCount % 4 === 0){
        pageCount = newsCount/4;
    }else{
        pageCount = newsCount/4 + 1;
    }
    var searchCriteria = $('#searchCriteriaValue').val();
    if(searchCriteria == 1){
        var authorList = document.getElementById("authorList");
        var tagList = document.getElementById("tagList");
        var values = "&";
        for (var i = 0; i < authorList.options.length; i++) {
            if (authorList.options[i].selected) {
                values += "authorList=" + authorList.options[i].value + "&";
            }
        }
        for(var i = 0; i < tagList.options.length; i++){
            if(tagList.options[i].selected && tagList.options[i] !== tagList.options.length - 1){
                values += "tagList=" + tagList.options[i].value + "&";
            }else if(tagList.options[i].selected){
                values += "tagList=" + tagList.options[i].value;
            }
        }
        $('#pagination-demo').twbsPagination({
            totalPages: pageCount,
            visiblePages: 4,
            href: '/newsmanagement/controller?command=filterCommandImpl&page={{pageNumber}}' + values,
            hrefVariable: '{{pageNumber}}',
            onPageClick: function (event, page) {
                $('#not-spa-demo-content').text('Page ' + page);
            }
        });
    }else {
        $('#pagination-demo').twbsPagination({
            totalPages: pageCount,
            visiblePages: 4,
            href: '/newsmanagement/controller?command=newsCommandImpl&page={{pageNumber}}',
            hrefVariable: '{{pageNumber}}',
            onPageClick: function (event, page) {
                $('#not-spa-demo-content').text('Page ' + page);
            }
        });
    }
});