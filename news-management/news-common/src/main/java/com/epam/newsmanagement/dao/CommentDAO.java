package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Extension of GenericDao, parametrized by entity <tt>Comment</tt> and key type <tt>Long</tt>
 *
 * Provides with more special methods for working with this entity in database.
 * Contains more methods for finding entities.
 * @see GenericDAO
 */
public interface CommentDAO extends GenericDAO<Comment, Long>{

    /**
     * Finds and returns list of comments that were added to the certain news message.
     * @param idNews unique key of news message
     * @return list of comments
     * @throws DAOException if there were some errors or exceptions in the run-time
     */
    List<Comment> getByNewsId(Long idNews) throws DAOException;

    /**
     * Inserts comments into certain table.
     *
     * @param comments to insert
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    void create(List<Comment> comments) throws DAOException;

    /**
     * Removes comments that was found by unique key (ID).
     * @param idList to find entity
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    void delete(List<Long> idList) throws DAOException;
}
