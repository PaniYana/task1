package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.domain.to.UserTO;
import com.epam.newsmanagement.exception.IllegalParameterException;
import com.epam.newsmanagement.exception.ServiceException;
import org.springframework.stereotype.*;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/15/2016.
 */

/**
 * Extension of GenericService, parametrized by entity <tt>User</tt> and key type <tt>Long</tt>
 *
 * Provides with more special for working with this entities
 * @see GenericService
 */
public interface UserService extends GenericService<User, Long> {

    /**
     * Removes entity that was found by unique key (ID).
     *
     * @param idUser unique key to find
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void delete(Long idUser) throws ServiceException;

    /**
     * Provides with finding user by its login.
     * @param login to find
     * @return found user entity
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    User findByLogin(String login) throws ServiceException;
}
