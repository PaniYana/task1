package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Role;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Extension of GenericDao, parametrized by entity <tt>Role</tt> and key type <tt>Long</tt>
 *@see GenericDAO
 */
public interface RoleDAO extends GenericDAO<Role, Long>{
}
