package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.exception.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana Volchok on 10.04.16.
 */
/**
 * Implementation of interface <tt>UserDAO</tt>
 * Adapted to the work with Oracle database.
 * @see UserDAO
 */
@Repository
@Profile("jdbc")
public class UserDAOImpl implements UserDAO{

    private static final String USER_ID = "UT_USER_ID";
    private static final String USER_NAME = "UT_USER_NAME";
    private static final String LOGIN = "UT_LOGIN";
    private static final String PASSWORD = "UT_PASSWORD";
    private static final String ROLE_ID = "UT_ROLE_ID";

    private static final String SQL_GET_BY_ID =
            "SELECT UT_USER_ID, UT_USER_NAME, UT_LOGIN, UT_PASSWORD, UT_ROLE_ID FROM USER_TAB WHERE UT_USER_ID = ?";
    private static final String SQL_GET_ALL =
            "SELECT UT_USER_ID, UT_USER_NAME, UT_LOGIN, UT_PASSWORD, UT_ROLE_ID FROM USER_TAB";
    private static final String SQL_CREATE =
            "INSERT INTO USER_TAB (UT_USER_ID, UT_USER_NAME, UT_LOGIN, UT_PASSWORD, UT_ROLE_ID) VALUES (USERS_TAB_SEQ.nextval, ?, ?, ?, ?)";
    private static final String SQL_UPDATE =
            "UPDATE USERS_TAB SET UT_USER_NAME = ?, UT_LOGIN = ?, UT_PASSWORD = ?,UT_ROLE_ID  = ?  WHERE UT_USER_ID = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM USER_TAB WHERE UT_USER_ID = ?";
    private static final String SQL_GET_BY_LOGIN =
            "SELECT UT_USER_ID, UT_USER_NAME, UT_LOGIN, UT_PASSWORD, UT_ROLE_ID FROM USER_TAB WHERE UT_LOGIN = ?";



    @Autowired
    private DataSource datasource;

    /**
     * Implementation of method {@link UserDAO#find(Object)}
     * @see UserDAO
     */
    public User find(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_BY_ID);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();

            User user = null;
            if(resultSet.next()){
                user = buildEntity(resultSet);
            }
            return user;
        }catch(SQLException e){
            throw new DAOException("Method find in UserDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link UserDAO#getAll()}
     * @see UserDAO
     */
    public List<User> getAll() throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<User> userList;

        try{
            connection = DataSourceUtils.doGetConnection( datasource);
            statement = connection.prepareStatement(SQL_GET_ALL);
            resultSet = statement.executeQuery();

            userList = new ArrayList<User>();
            User user;
            while(resultSet.next()){
                user = buildEntity(resultSet);
                userList.add(user);
            }
            return userList;
        }catch(SQLException e){
            throw new DAOException("Method getAll in UserDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link UserDAO#create(Object)}
     * @see UserDAO
     */
    public User create(User user) throws DAOException {

        final String[] keys = {USER_ID};

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_CREATE, keys);
            statement.setString(1, user.getName());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.setLong(4, user.getRoleId());

            statement.executeUpdate();
            Long id = -1L;
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            return find(id);
        }catch(SQLException e){
             throw new DAOException("Method create in UserDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link UserDAO#update(Object)}
     * @see UserDAO
     */
    public User update(User user) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, user.getName());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.setLong(4, user.getRoleId());
            statement.setLong(5, user.getId());

            int row = statement.executeUpdate();

            return user;

        }catch(SQLException e){
            throw new DAOException("Method update in UserDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link UserDAO#delete(Object)}
     * @see UserDAO
     */
    public void delete(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_DELETE_BY_ID);
            statement.setLong(1, id);

            int row = statement.executeUpdate();

        }catch(SQLException e){
            throw new DAOException("Method delete in UserDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    private User buildEntity(ResultSet resultSet) throws SQLException{
        User user = new User();
        user.setId(resultSet.getLong(USER_ID));
        user.setName(resultSet.getString(USER_NAME));
        user.setLogin(resultSet.getString(LOGIN));
        user.setPassword(resultSet.getString(PASSWORD));
        user.setRoleId(resultSet.getLong(ROLE_ID));
        return user;
    }

    /**
     * Implementation of method {@link UserDAO#find(String)}
     * @see UserDAO
     */
    public User find(String login) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_BY_LOGIN);
            statement.setString(1, login);
            resultSet = statement.executeQuery();

            User user = null;
            if(resultSet.next()){
                user = buildEntity(resultSet);
            }
            return user;
        }catch(SQLException e){
            throw new DAOException("Method find in UserDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }
}
