package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.UserServiceImpl;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/19/2016.
 */
public class UserServiceTest {

    @InjectMocks
    private UserService userService = new UserServiceImpl();

    @Mock
    private UserDAO userDAO;

    @Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAdd() throws ServiceException{

        User user = new User(1L, "K.Johns", "johns1", "password", 2L);
        try {
            Mockito.when(userDAO.create(user)).thenReturn(user);
            Long id = userService.add(user);
            Mockito.verify(userDAO).create(user);
            Assert.assertEquals(user.getId(), id);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGetAll() throws ServiceException{
        List<User> userList = new ArrayList<User>();
        userList.add(new User(1L, "K.Johns", "johns1", "password", 2L));
        userList.add(new User(2L, "J.Green", "jgreen", "password", 2L));
        try {
            Mockito.when(userDAO.getAll()).thenReturn(userList);
            List<User> actualList = userService.getAll();
            Mockito.verify(userDAO).getAll();
            Assert.assertEquals(userList.size(), actualList.size());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGet() throws ServiceException{
        User user = new User(1L, "K.Johns", "johns1", "password", 2L);
        try{
            Mockito.when(userDAO.find(1L)).thenReturn(user);
            User actualUser = userService.get(1L);
            Mockito.verify(userDAO).find(1L);
            Assert.assertEquals(user.getId(), actualUser.getId());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testDelete() throws ServiceException{
        try{
            userService.delete(1L);
            Mockito.verify(userDAO).delete(1L);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testFindByLogin() throws ServiceException{
        try{
            User user = new User(1L, "K.Johns", "johns1", "password", 2L);
            Mockito.when(userDAO.find("johns1")).thenReturn(user);
            User actualUser = userService.findByLogin("johns1");
            Assert.assertEquals(user.getId(), actualUser.getId());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }
}
