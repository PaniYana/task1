package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/13/2016.
 */

/**
 * Implementation of interface <tt>TagService</tt>

 * @see TagService
 */
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagDAO tagDAO;

    private static final Logger LOGGER = LogManager.getLogger(TagServiceImpl.class);

    /**
     * Implementation of method {@link TagService#add(Object)}
     * @see TagService
     */
    @Transactional
    public Long add(Tag tag) throws ServiceException {
        Long createdId = null;
        if(tag != null){
            try {
                createdId = tagDAO.create(tag).getId();
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return createdId;
    }

    /**
     * Implementation of method {@link TagService#getAll()}
     * @see TagService
     */
    @Transactional
    public List<Tag> getAll() throws ServiceException {
        List<Tag> tagList = null;
        try {
            tagList = tagDAO.getAll();
        }catch(DAOException e){
            LOGGER.error(e);
            throw new ServiceException(e.getMessage());
        }
        return tagList;
    }

    /**
     * Implementation of method {@link TagService#get(Object)}
     * @see TagService
     */
    @Transactional
    public Tag get(Long id) throws ServiceException {
        Tag tag = null;
        if(id != null){
            try {
                tag = tagDAO.find(id);
            }catch(DAOException e) {
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return tag;
    }

    /**
     * Implementation of method {@link TagService#addSeveral(List)}
     * @see TagService
     */
    @Transactional
    public List<Long> addSeveral(List<Tag> tagList) throws ServiceException{
        List<Long> idList = null;
        try{
            if(!tagList.isEmpty()){
                idList = tagDAO.create(tagList);
            }
            return idList;
        }catch(DAOException e){
            LOGGER.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link TagService#getTagsByNews(Long)} )}
     * @see TagService
     */
    @Transactional
    public List<Tag> getTagsByNews(Long idNews) throws ServiceException {
        List<Tag> tagList = null;
        try{
            if(idNews != null) {
                tagList = tagDAO.getTagsByNews(idNews);
            }
            return tagList;
        }catch(DAOException e){
            LOGGER.error(e.getCause());
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link TagService#update(Tag)}
     * @see TagService
     */
    @Transactional
    @Override
    public Long update(Tag tag) throws ServiceException {
        Long id = null;
        if(tag != null){
            try {
                id = tagDAO.update(tag).getId();
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return id;
    }

    /**
     * Implementation of method {@link TagService#delete(Long)}
     * @see TagService
     */
    @Transactional
    @Override
    public void delete(Long id) throws ServiceException {
        if(id != null){
            try{
                tagDAO.delete(id);
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
    }
}
