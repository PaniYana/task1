package com.epam.newsmanagement.controller.command;

import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Yana_Valchok on 6/10/2016.
 */
public interface Command {

    String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
