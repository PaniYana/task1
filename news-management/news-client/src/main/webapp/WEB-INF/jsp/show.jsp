<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="header" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="footer" %>
<html>
<head>
    <meta content="IE=edge, chrome=1" http-equiv="X-UA-Compatible"/>
    <title>News Management</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-multiselect.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pagination.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/multiselect.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
</head>
<body>
<header:header/>
<div id="content">
    <div class="container news-container modal-content col-md-offset-1 col-md-10">
        <div id="btn-back">
            <a href="controller?command=newsCommandImpl" class="btn btn-primary btn-sm">BACK</a>
        </div>
        <div class="single-news-container">
            <div id="single-news-wrapper">
                <c:if test="${news != null}">
                <div class="list-news-title">
                    <a><c:out value="${news.news.title}"/></a>
                </div>
                <c:if test="${news.authorList != null}">
                    <div class="author-labels">
                        <c:forEach var="author" items="${news.authorList}">
                            <span class="label label-primary">${author.name}</span>
                        </c:forEach>
                    </div>
                </c:if>
                <p class="news-short-text"><c:out value="${news.news.fullText}"/></p>

                <div class="news-creation-date">
                    <span class="creation-date"><c:out value="${news.news.creationDate}"/></span>
                </div>

                <c:if test="${news.commentList != null}">
                    <div>
                        <c:forEach var="comment" items="${news.commentList}">
                            <div class="panel panel-default">
                                <div class="panel-heading">${comment.creationDate}</div>
                                <div class="panel-body">
                                        ${comment.text}
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </c:if>

                <form class="form-horizontal" method="post" action="controller">

                    <input type="hidden" name="command" value="addCommentCommandImpl">
                    <input type="hidden" name="newsId" value="${news.news.id}">

                    <div class="form-group">
                        <label for="text" class="col-lg-2 control-label">Add comment</label>

                        <div class="col-lg-10">
                            <textarea name="text" class="form-control" rows="3" id="text"></textarea>
                        </div>
                    </div>
                    <div class="col-md-offset-8">
                        <button type="submit" class="btn btn-primary">Post comment</button>
                    </div>
                </form>
            </div>
            <div id="single-news-pagination">
                <ul class="pager">
                    <li><a href="/newsmanagement/controller?command=showPreviousCommandImpl&id=${news.news.id}">Previous</a></li>
                    <li><a href="/newsmanagement/controller?command=showNextCommandImpl&id=${news.news.id}">Next</a></li>
                </ul>
            </div>
            </c:if>
        </div>
    </div>
</div>
<footer:footer/>
</body>
</html>