package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.IllegalParameterException;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/13/2016.
 */

/**
 * Extension of GenericService, parametrized by entity <tt>Tag</tt> and key type <tt>Long</tt>
 *
 * Provides with more special for working with this entities
 * @see GenericService
 */
public interface TagService extends GenericService<Tag, Long> {

    /**
     * Allows to add several tags.
     *
     * @param tagList tags to add
     * @return list of added tags IDs
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    List<Long> addSeveral(List<Tag> tagList) throws ServiceException;

    /**
     * Finds and returns list of tags which were mentioned as tags of certain news message.
     *
     * @param idNews unique key of news message
     * @return list of tags
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    List<Tag> getTagsByNews(Long idNews) throws ServiceException;

    /**
     * Allows to modify tag information.
     *
     * @param tag entity to edit
     * @return ID of modified entity
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    Long update(Tag tag) throws ServiceException;

    /**
     * Removes entity that was found by unique key (ID).
     *
     * @param id - unique key
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void delete(Long id) throws ServiceException;
}
