package com.epam.newsmanagement.controller.command.impl;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Yana_Valchok on 6/14/2016.
 */
@Component
public class AddCommentCommandImpl implements Command {

    private static final String NEWS_ID = "newsId";
    private static final String TEXT = "text";
    private static final String RETURN_QUERY = "command=showCommandImpl&id=";


    @Autowired
    private CommentService commentService;

    @Autowired
    NewsService newsService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String newsIdStr = request.getParameter(NEWS_ID);
        String text = request.getParameter(TEXT);
        Long newsId = null;
        if(newsIdStr != null){
            newsId = Long.parseLong(newsIdStr);
        }else{
            return RETURN_QUERY + newsIdStr;
        }
        try{
            Comment comment = new Comment();
            comment.setText(text);
            News news = newsService.get(newsId);
            comment.setNews(news);
            commentService.add(comment);
        }catch(ServiceException e){
            throw new CommandException(e.getMessage());
        }
        return RETURN_QUERY + newsIdStr;
    }
}
