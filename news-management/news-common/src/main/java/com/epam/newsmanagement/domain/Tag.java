package com.epam.newsmanagement.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */
/**
 * Domain class, representation of tag entity
 */
public class Tag implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private Set<News> news;

    public Tag() {
    }

    public Tag(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<News> getNews() {
        return news;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if (this == obj){
            return true;
        }
        if (!(obj instanceof Tag)){
            return false;
        }

        Tag tag = (Tag) obj;

        if (!id.equals(tag.id)){
            return false;
        }
        if (!name.equals(tag.name)){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
