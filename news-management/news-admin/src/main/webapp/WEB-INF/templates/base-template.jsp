<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>



<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>
        <tiles:insertAttribute name="title" />
    </title>

    <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>

    <spring:url value="/resources/css/bootstrap-multiselect.css" var="bootstrapMultiselectCss"/>
    <link href="${bootstrapMultiselectCss}" rel="stylesheet"/>

    <spring:url value="/resources/css/style.css" var="styleCss"/>
    <link href="${styleCss}" rel="stylesheet"/>

    <spring:url value="/resources/js/jquery-1.12.4.min.js" var="jQuery"/>
    <script type="text/javascript" src="${jQuery}"></script>

    <spring:url value="/resources/js/bootstrap.min.js" var="bootstrap"/>
    <script type="text/javascript" src="${bootstrap}"></script>

    <spring:url value="/resources/js/bootstrap-multiselect.js" var="bootstrapMultiselectJs"/>
    <script type="text/javascript" src="${bootstrapMultiselectJs}"></script>

    <spring:url value="/resources/js/multiselect.js" var="multiselectJs"/>
    <script type="text/javascript" src="${multiselectJs}"></script>

    <spring:url value="/resources/js/author-update.js" var="authorUpdate"/>
    <script type="text/javascript" src="${authorUpdate}"></script>

    <spring:url value="/resources/js/tag-update.js" var="tagUpdate"/>
    <script type="text/javascript" src="${tagUpdate}"></script>

    <spring:url value="/resources/js/highlighting.js" var="highlighting"/>
    <script type="text/javascript" src="${highlighting}"></script>


</head>
<body>
    <div>
        <tiles:insertAttribute name="header"/>
    </div>
    <div>
        <tiles:insertAttribute name="sidebar"/>
    </div>
    <div>
        <tiles:insertAttribute name="content"/>
    </div>
    <div>
        <tiles:insertAttribute name="footer"/>
    </div>
</body>

</html>