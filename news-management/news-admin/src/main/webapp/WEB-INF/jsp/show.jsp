
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div id="content">
    <div class="container news-container modal-content col-md-offset-3 col-md-9">
        <div class="single-news-container">
            <div id="single-news-wrapper">
            <c:if test="${news != null}">
                <div class="list-news-title">
                    <a><c:out value="${news.news.title}"/></a>
                </div>
                    <c:if test="${news.authorList != null}">
                        <div class="author-labels">
                            <c:forEach var="author" items="${news.authorList}">
                                <span class="label label-primary">${author.name}</span>
                            </c:forEach>
                        </div>
                    </c:if>
                <p class="news-short-text"><c:out value="${news.news.fullText}"/></p>

                <div class="news-creation-date">
                    <span class="creation-date"><c:out value="${news.news.creationDate}"/></span>
                </div>

                <c:if test="${news.commentList != null}">
                    <spring:url value="/comment/delete" var="delete"/>
                    <div>
                        <c:forEach var="comment" items="${news.commentList}">
                            <form:form method="post" modelAttribute="commentToDelete" action="${delete}">
                                <div class="panel panel-default">
                                    <div class="panel-heading">${comment.creationDate}
                                        <button type="submit" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="panel-body">
                                            ${comment.text}
                                    </div>
                                    <form:hidden path="id" value="${comment.id}"/>
                                    <form:hidden path="news.id" value="${news.news.id}"/>
                                </div>
                            </form:form>
                        </c:forEach>
                    </div>
                </c:if>

                <spring:url value="/comment/add" var="add"/>
                <spring:htmlEscape defaultHtmlEscape="true" />
                <form:form class="form-horizontal" method="post" modelAttribute="commentForm" action="${add}">

                    <form:hidden path="news.id" value="${news.news.id}"/>
                    <form:hidden path="news.version" value="${news.news.version}"/>
                    <form:hidden path="news.title" value="${news.news.title}"/>
                    <form:hidden path="news.shortText" value="${news.news.shortText}"/>
                    <form:hidden path="news.fullText" value="${news.news.fullText}"/>
                    <form:hidden path="news.creationDate" value="${news.news.creationDate}"/>
                    <form:hidden path="news.modificationDate" value="${news.news.modificationDate}"/>
                    <spring:bind path="text">
                        <div class="form-group">
                            <label for="text" class="col-lg-2 control-label">Add comment</label>

                            <div class="col-lg-10">
                                <form:textarea path="text" htmlEscape="true" class="form-control" rows="3" id="text"/>
                            </div>
                        </div>
                    </spring:bind>
                    <div class="col-md-offset-8">
                        <button type="submit" class="btn btn-primary">Post comment</button>
                    </div>
                </form:form>
                </div>
                <div id="single-news-pagination">
                    <ul class="pager">
                        <li><a href="/news/${news.news.id}/prev">Previous</a></li>
                        <li><a href="/news/${news.news.id}/next">Next</a></li>
                    </ul>
                </div>
            </c:if>
        </div>
    </div>
</div>
