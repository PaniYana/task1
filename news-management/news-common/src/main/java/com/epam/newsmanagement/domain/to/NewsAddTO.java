package com.epam.newsmanagement.domain.to;

import com.epam.newsmanagement.domain.News;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Yana_Valchok on 5/23/2016.
 */
public class NewsAddTO implements Serializable {

    public static final long serialVersionUID = 1L;
    private News news;
    private List<Long> authorList;
    private List<Long> tagList;
    private List<Long> commentList;

    public NewsAddTO(News news, List<Long> authorList, List<Long> tagList, List<Long> commentList) {
        this.news = news;
        this.authorList = authorList;
        this.tagList = tagList;
        this.commentList = commentList;
    }

    public NewsAddTO() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Long> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Long> authorList) {
        this.authorList = authorList;
    }

    public List<Long> getTagList() {
        return tagList;
    }

    public void setTagList(List<Long> tagList) {
        this.tagList = tagList;
    }

    public List<Long> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Long> commentList) {
        this.commentList = commentList;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof NewsTO)) {
            return false;
        }

        NewsTO newsTO = (NewsTO) obj;

        if (!getNews().equals(newsTO.getNews())){
            return false;
        }
        if (!getAuthorList().equals(newsTO.getAuthorList())){
            return false;
        }
        if (!getTagList().equals(newsTO.getTagList())){
            return false;
        }
        if(!getCommentList().equals(newsTO.getCommentList())){
            return false;
        }

        return true;

    }

    @Override
    public int hashCode() {
        int result = getNews().hashCode();
        result = 31 * result + getAuthorList().hashCode();
        result = 31 * result + getTagList().hashCode();
        result = 31 * result + getCommentList().hashCode();
        return result;
    }
}