<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="navbar navbar-default navbar-fixed-top clearfix">
    <div>
        <a class="navbar-brand">News portal</a>
    </div>

    <div id="logout-wrapper">
        <div class="localization-links">
            <a>EN</a>
            <a>RU</a>
        </div>
    </div>
</div>