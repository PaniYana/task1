package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.exception.DAOException;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Yana_Valchok on 7/5/2016.
 */
public class CommentDAOImpl extends GenericDAOImpl<Comment, Long> implements CommentDAO {

    private static final String HQL_GET_BY_NEWS_ID = "FROM Comment c WHERE c.news.id = :newsId";

    public CommentDAOImpl(Class<Comment> entityClass) {
        super(entityClass);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Comment> getByNewsId(Long idNews) throws DAOException {
        try {
            Query query = getCurrentSession().createQuery(HQL_GET_BY_NEWS_ID);
            query.setParameter("newsId", idNews);
            List list = query.list();
            return (List<Comment>)list;
        }catch(Exception e){
            throw new DAOException("Method getAuthorsByNews in AuthorDAO", e);
        }
    }

    @Transactional
    @Override
    public void create(List<Comment> comments) throws DAOException {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            for(Comment comment: comments) {
                session.save(comment);
            }
            session.getTransaction().commit();
        }catch(Exception e){
            throw new DAOException("Method create in CommentDAO", e);
        }
    }

    @Transactional
    @Override
    public void delete(List<Long> idList) throws DAOException {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            Comment comment = null;
            for(Long id: idList) {
                comment = find(id);
                session.delete(comment);
            }
            session.getTransaction().commit();
        }catch(Exception e){
            throw new DAOException("Method delete in CommentDAO", e);
        }
    }
}
