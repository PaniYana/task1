package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;

import com.epam.newsmanagement.domain.criteria.SearchCriteria;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Extension of GenericDao, parametrized by entity <tt>News</tt> and key type <tt>Long</tt>
 *
 * Provides with more special methods for working with this entity in database.
 * Contains more methods for adding, removing and finding entities.
 * @see GenericDAO
 */
public interface NewsDAO extends GenericDAO<News, Long>{


    /**
     * Creates relation between news and author records by adding new record to intermediate table.
     * @param idNews - unique key of news message
     * @param idAuthor -  unique key of author that should be marked as author of the certain news message
     * @throws DAOException if there were errors or exceptions in the run-time
    */
    void attachAuthorToNews(Long idNews, Long idAuthor) throws DAOException;

    /**
     * Creates relation between news and tag records by adding new record to intermediate table.
     * @param idNews - unique key of news message
     * @param idTag -  unique key of tag that should be added to the certain news message
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    void attachTagToNews(Long idNews, Long idTag) throws DAOException;

    /**
     * Removes relation between news and author records by deleting record from intermediate table.
     * @param idNews - unique key of news message
     * @param idAuthor -  unique key of author that was marked as author of the certain news message
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    void detachAuthorFromNews(Long idNews, Long idAuthor) throws DAOException;

    /**
     * Removes relation between news and tag records by deleting record from intermediate table.
     * @param idNews - unique key of news message
     * @param idTag -  unique key of tag that was added to the certain news message
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    void detachTagFromNews(Long idNews, Long idTag) throws DAOException;

    /**
     * Provides with finding news by <tt>SearchCriteria</tt> object, that contains list of authors and
     * list of tags.
     * @param searchCriteria - object that contains parameters for searching news messages
     * @return list of found news messages
     * @throws DAOException if there were errors or exceptions in the runtime
     *
     */
    List<News> find(SearchCriteria searchCriteria) throws DAOException;

    /**
     * Provides with counting news message records.
     * @return number of news message at the moment
     * @throws DAOException if there were errors or exceptions in the runtime
     */
    int getNewsCount() throws DAOException;

    /**
     * Finds and returns list of news that were mentioned as news of certain author.
     * @param idAuthor - unique key of author which news should be found
     * @return list of found news
     * @throws DAOException if there were errors or exceptions in the runtime
     */
    List<News> getNewsByAuthor(Long idAuthor) throws DAOException;

    /**
     * Represents part of list of news in certain table. List is limited by page offset and
     * amount of records on the page
     *
     * @return list of news
     * @throws DAOException if there were errors or exceptions in the runtime
     */
    List<News> getAllPaged(Integer offset, Integer count) throws DAOException;

    /**
     * Creates relations between news and tag records by adding new records to intermediate table.
     * @param idNews - unique key of news message
     * @param idTagList - list of tags that should be added to the certain news message
     * @throws DAOException if there were errors or exceptions in the runtime
     */
    void attachTagToNews(Long idNews, List<Long> idTagList) throws DAOException;

    /**
     * Creates relations between news and author records by adding new records to intermediate table.
     * @param idNews - unique key of news message
     * @param idAuthorList - list of authors that should be added to the certain news message
     * @throws DAOException if there were errors or exceptions in the runtime
     */
    void attachAuthorToNews(Long idNews, List<Long> idAuthorList) throws DAOException;

    /**
     * Removes relations between news and author records by deleting records from intermediate table.
     * @param idNews - unique key of news message
     * @param idAuthorList -  list of author ids that were marked as authors of the certain news message
     * @throws DAOException if there were errors or exceptions in the runtime
     */
    void detachAuthorFromNews(Long idNews, List<Long> idAuthorList) throws DAOException;

    /**
     * Removes relations between news and tag records by deleting records from intermediate table.
     * @param idNews - unique key of news message
     * @param idTagList -  list of tags that were added to the certain news message
     * @throws DAOException if there were errors or exceptions in the runtime
     */
    void detachTagFromNews(Long idNews, List<Long> idTagList) throws DAOException;

    /**
     * Returns previous news after current news.
     * @param id - unique key of current news
     * @return previous news
     * @throws DAOException if there were errors or exceptions in the runtime
     */
    Long getPrevious(Long id) throws DAOException;

    /**
     * Returns next news after current news.
     * @param id - unique key of current news
     * @return next news
     * @throws DAOException if there were errors or exceptions in the runtime
     */
    Long getNext(Long id) throws DAOException;

    /**
     * Provides with finding news by <tt>SearchCriteria</tt> object, that contains list of authors and
     * list of tags (paginated).
     * @param searchCriteria - object that contains parameters for searching news messages
     * @return list of found news messages
     * @throws DAOException if there were errors or exceptions in the runtime
     *
     */
    List<News> find(SearchCriteria searchCriteria, int offset, int count) throws DAOException;
}
