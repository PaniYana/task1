package com.epam.newsmanagement.validation;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Yana_Valchok on 5/24/2016.
 */
@Component
public class TagFormValidator implements Validator {

    private static final String NAME = "name";
    private static final String NOT_EMPTY_NAME = "NotEmpty.authorForm.name";
    private static final String REPLACE_REGEXP = "<[^>]*>";

    @Autowired
    TagService tagService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Tag.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {

        Tag tag = (Tag) o;

        if(tag.getName() == null || tag.getName().equals("")){
            errors.rejectValue(NAME, NOT_EMPTY_NAME);
        }else{
            tag.setName(tag.getName().replaceAll(REPLACE_REGEXP, ""));
        }
    }
}
