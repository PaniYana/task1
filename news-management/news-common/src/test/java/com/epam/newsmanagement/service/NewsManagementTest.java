package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsManagementImpl;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/15/2016.
 */
public class NewsManagementTest {

    @InjectMocks
    private NewsManagement newsManagement = new NewsManagementImpl();

    @Mock
    private AuthorService authorService;

    @Mock
    private TagService tagService;

    @Mock
    private NewsService newsService;

    @Mock
    private CommentService commentService;

    @Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddNewsWithAuthorAndTags() throws ServiceException{
        try {
            News news = new News(1L, "Title", "Short text", "Full text", new Timestamp(1460623612723L), new Date(1460623612723L));
            Mockito.when(newsService.add(news)).thenReturn(news.getId());

            List<Long> authorList = new ArrayList<Long>();
            authorList.add(1L);
            authorList.add(2L);

            List<Long> tagList = new ArrayList<Long>();
            tagList.add(1L);
            tagList.add(2L);


            Long idNews = newsManagement.add(news, authorList, tagList);
            Mockito.verify(newsService).add(news);
            Mockito.verify(newsService).attachAuthorToNews(news.getId(), authorList);
            Mockito.verify(newsService).attachAuthorToNews(news.getId(), tagList);
            Assert.assertEquals(news.getId(), idNews);

        }catch(ServiceException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testFind() throws ServiceException{
        NewsTO newsTO = new NewsTO();
        News news = new News(1L, "Title", "Short text", "Full text", new Timestamp(1460623612723L), new Date(1460623612723L));
        newsTO.setNews(news);
        newsTO.setAuthorList(Collections.<Author>emptyList());
        newsTO.setCommentList(Collections.<Comment>emptyList());
        newsTO.setTagList(Collections.<Tag>emptyList());

        Mockito.when(newsService.get(Mockito.anyLong())).thenReturn(news);
        NewsTO actualTO = newsManagement.find(1L);
        Mockito.verify(newsService).get(1L);
        Assert.assertEquals(newsTO.getAuthorList().size(), actualTO.getAuthorList().size());
    }

    @Test
    public void testDeleteNewsWithAuthorAndTags() throws ServiceException{
        List<Long> list = new ArrayList<Long>();
        list.add(1L);
        newsManagement.delete(list);
        Mockito.verify(newsService).delete(1L);
    }


    @Test
    public void testFindBySearchCriteria() throws ServiceException{
        News news = new News(1L, "Title", "Short text", "Full text", new Timestamp(1460623612723L), new Date(1460623612723L));

        List<NewsTO> newsTOList = Collections.emptyList();
        List<Author> authorList = Collections.emptyList();
        List<Tag> tagList = Collections.emptyList();
        List<Comment> commentList = Collections.emptyList();

        List<Long> authorIdList = Collections.emptyList();
        List<Long> tagIdList = Collections.emptyList();

        Mockito.when(newsService.get(Mockito.anyLong())).thenReturn(news);
        List<NewsTO> actualTOList = newsManagement.find(authorIdList, tagIdList);
        Assert.assertEquals(newsTOList.size(), actualTOList.size());
    }

    @Test
    public void testFindAll() throws ServiceException{
        List<News> newsList = new ArrayList<News>();
        newsList.add(new News(1L, "Title", "Short text", "Full text", new Timestamp(1460623612723L), new Date(1460623612723L)));
        Mockito.when(newsService.getAll()).thenReturn(newsList);

        List<NewsTO> newsTOList = newsManagement.findAll();
        Mockito.verify(newsService).getAll();
        Assert.assertEquals(newsList.size(), newsTOList.size());
    }

}
