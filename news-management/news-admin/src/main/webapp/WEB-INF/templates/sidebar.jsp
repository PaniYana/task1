<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="sidebar" class="col-md-3">
    <div class="list-group table-of-contents">
        <a class="list-group-item" href="/news/list">News list</a>
        <a class="list-group-item" href="/news/add">Add news</a>
        <a class="list-group-item" href="/author/list">Add/Update authors</a>
        <a class="list-group-item" href="/tag/list">Add/Update tags</a>
    </div>
</div>