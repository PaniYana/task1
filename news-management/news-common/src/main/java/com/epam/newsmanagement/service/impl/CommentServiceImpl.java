package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Yana_Valchok on 4/13/2016.
 */

/**
 * Implementation of interface <tt>CommentService</tt>

 * @see CommentService
 */
//@Service("CommentService")
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDAO commentDAO;

    private static final Logger LOGGER = LogManager.getLogger(CommentServiceImpl.class);

    /**
     * Implementation of method {@link CommentService#delete(Long)}
     * @see CommentService
     */
    @Transactional
    public void delete(Long id) throws ServiceException {
        if(id != null){
            try{
                commentDAO.delete(id);
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
    }

    /**
     * Implementation of method {@link CommentService#add(Object)}
     * @see CommentService
     */
    @Transactional
    public Long add(Comment comment) throws ServiceException {
        Long createdId = null;
        if(comment != null){
            if(comment.getCreationDate() == null){
                comment.setCreationDate(new Timestamp(new Date().getTime()));
            }
            try {
               /* List<Comment> commentList = commentDAO.getByNewsId(comment.getNews().getId());
                commentList.add(comment);
                comment.getNews().setComments(commentList);*/
                createdId = commentDAO.create(comment).getId();
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return createdId;
    }

    /**
     * Implementation of method {@link CommentService#getAll()} )}
     * @see CommentService
     */
    @Transactional
    public List<Comment> getAll() throws ServiceException {
        List<Comment> commentList = null;
        try {
            commentList = commentDAO.getAll();
        }catch(DAOException e){
            LOGGER.error(e);
            throw new ServiceException(e.getMessage());
        }
        return commentList;
    }

    /**
     * Implementation of method {@link CommentService#get(Object)}
     * @see CommentService
     */
    @Transactional
    public Comment get(Long id) throws ServiceException {
        Comment comment = null;
        if(id != null){
            try {
                comment = commentDAO.find(id);
            }catch(DAOException e) {
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return comment;
    }

    /**
     * Implementation of method {@link CommentService#addSeveral(List)}
     * @see CommentService
     */
    @Transactional
    public void addSeveral(List<Comment> commentList) throws ServiceException {
        List<Long> idList = null;
        try{
            if(commentList != null) {
                commentDAO.create(commentList);
            }
        }catch(DAOException e){
            LOGGER.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link CommentService#deleteSeveral(List)}
     * @see CommentService
     */
    @Transactional
    public void deleteSeveral(List<Long> commentList) throws ServiceException {
        try{
            if(commentList != null) {
                commentDAO.delete(commentList);
            }
        }catch(DAOException e){
            LOGGER.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link CommentService#findByNewsId(Long)}
     * @see CommentService
     */
    @Transactional
    public List<Comment> findByNewsId(Long idNews) throws ServiceException{
        List<Comment> commentList = null;
        try{
            if(idNews != null) {
                commentList = commentDAO.getByNewsId(idNews);
            }
            return commentList;
        }catch(DAOException e){
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }
}
