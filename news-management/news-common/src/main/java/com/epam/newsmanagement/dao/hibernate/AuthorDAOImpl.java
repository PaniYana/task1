package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by Yana_Valchok on 7/1/2016.
 */
public class AuthorDAOImpl extends GenericDAOImpl<Author, Long> implements AuthorDAO {

    private static final String HQL_GET_AUTHOR_BY_NEWS = "SELECT a FROM News n JOIN n.authors a WHERE n.id = :NA_NEWS_ID";

    public AuthorDAOImpl(Class<Author> entityClass) {
        super(entityClass);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Author> getNotExpiredList() throws DAOException {
        try {
            Session session = getCurrentSession();
            Timestamp currentTimestamp = new Timestamp(new Date().getTime());
            Criterion criterion1 = Restrictions.ge("expired", currentTimestamp);
            Criterion criterion2 = Restrictions.isNull("expired");
            Criteria criteria = session.createCriteria(Author.class)
                    .add(Restrictions.or(criterion1, criterion2));
            return criteria.list();
        }catch(Exception e){
            throw new DAOException("Method getNotExpiredList in AuthorDAO", e);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Author> getAuthorsByNews(Long idNews) throws DAOException {
        try {
            Query query = getCurrentSession().createQuery(HQL_GET_AUTHOR_BY_NEWS);
            query.setParameter("NA_NEWS_ID", idNews);
            List list = query.list();
            return (List<Author>)list;
        }catch(Exception e){
            throw new DAOException("Method getAuthorsByNews in AuthorDAO", e);
        }
    }
}
