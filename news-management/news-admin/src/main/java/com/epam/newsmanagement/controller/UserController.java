package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Yana_Valchok on 6/1/2016.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    private static final String LOGIN_PAGE = "login";
    private static final String LOGIN_URL = "/login";

    @RequestMapping(value = LOGIN_URL, method = RequestMethod.GET)
    public ModelAndView getLoginPage(){
        User user = new User();
        ModelAndView modelAndView = new ModelAndView(LOGIN_PAGE);
        return modelAndView;
    }

}
