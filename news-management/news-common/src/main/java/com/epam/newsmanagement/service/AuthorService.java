package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.IllegalParameterException;
import com.epam.newsmanagement.exception.ServiceException;
import org.springframework.stereotype.*;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/12/2016.
 */

/**
 * Extension of GenericService, parametrized by entity <tt>Author</tt> and key type <tt>Long</tt>
 *
 * Provides with more special for working with this entities
 * @see GenericService
 */
public interface AuthorService extends GenericService<Author, Long> {

    /**
     * Removes entity that was found by unique key (ID).
     *
     * @param idAuthor - unique key
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void delete(Long idAuthor) throws ServiceException;

    /**
     * Modifies author entity by marking it 'expired', so it won't be displayable in author list
     * @param idAuthor - unique key for finding and modifying entity
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void setExpired(Long idAuthor) throws ServiceException;

    /**
     * Returns list of authors that weren't marked as expired.
     *
     * @return list of not expired authors
     * @throws ServiceException if DAOException was caught
     */
    List<Author> getNotExpiredList() throws ServiceException;

    /**
     * Finds and returns list of authors which were mentioned as authors of certain news message.
     *
     * @param idNews unique key of news message
     * @return list of authors
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    List<Author> getAuthorsByNews(Long idNews) throws ServiceException;

    /**
     * Allows to modify author information.
     *
     * @param author entity to edit
     * @return ID of modified entity
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    Long update(Author author) throws ServiceException;

}
