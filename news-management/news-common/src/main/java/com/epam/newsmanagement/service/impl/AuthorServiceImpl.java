package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/12/2016.
 */

/**
 * Implementation of interface <tt>AuthorService</tt>

 * @see AuthorService
 */
@Service
public class AuthorServiceImpl implements AuthorService {

    private static final Logger LOGGER = LogManager.getLogger(AuthorServiceImpl.class);
    private static final Long TIME_FOR_EXPIRATION = 1460623612723L;

    @Autowired
    private AuthorDAO authorDAO;

    /**
     * Implementation of method {@link AuthorService#add(Object)}
     * @see AuthorService
     */
    @Transactional
    public Long add(Author author) throws ServiceException {
        Long createdId = null;
        if(author != null){
            try{
                createdId = authorDAO.create(author).getId();
            }catch(DAOException e){
                LOGGER.error(e);
                throw  new ServiceException(e.getMessage());
            }
        }
        return createdId;
    }

    /**
     * Implementation of method {@link AuthorService#delete(Long)} )}
     * @see AuthorService
     */
    @Transactional
    public void delete(Long id) throws ServiceException {
        if(id != null){
            try{
                authorDAO.delete(id);
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
    }

    /**
     * Implementation of method {@link AuthorService#getAll()}
     * @see AuthorService
     */
    @Transactional
    public List<Author> getAll() throws ServiceException {
        List<Author> authorList = null;
        try{
            authorList = authorDAO.getAll();
        }catch(DAOException e){
            LOGGER.error(e);
            throw new ServiceException(e.getMessage());
        }
        return authorList;
    }

    /**
     * Implementation of method {@link AuthorService#get(Object)} )}
     * @see AuthorService
     */
    @Transactional
    public Author get(Long id) throws ServiceException {
        Author author = null;
        if(id != null){
            try {
                author = authorDAO.find(id);
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return author;
    }

    /**
     * Implementation of method {@link AuthorService#setExpired(Long)}
     * @see AuthorService
     */
    @Transactional
    public void setExpired(Long id) throws ServiceException {
        if(id != null){
            Author author = get(id);
            author.setExpired(new Timestamp(TIME_FOR_EXPIRATION));
            try {
                authorDAO.update(author);
            }catch(DAOException e){
                LOGGER.error(e);
                throw  new ServiceException(e.getMessage());
            }
        }
    }

    /**
     * Implementation of method {@link AuthorService#getNotExpiredList()}
     * @see AuthorService
     */
    @Transactional
    public List<Author> getNotExpiredList() throws ServiceException{

        List<Author> authorList = null;
        try{
            authorList = authorDAO.getNotExpiredList();
            return authorList;
        }catch(DAOException e){
            LOGGER.error(e);
            throw  new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link AuthorService#getAuthorsByNews(Long)}
     * @see AuthorService
     */
    @Transactional
    public List<Author> getAuthorsByNews(Long idNews) throws ServiceException{
        try{
            return authorDAO.getAuthorsByNews(idNews);
        }catch(DAOException e){
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link AuthorService#update(Author)}
     * @see AuthorService
     */
    @Transactional
    @Override
    public Long update(Author author) throws ServiceException {
        Long id = null;
        if(author != null){
            try {
                id = authorDAO.update(author).getId();
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return id;
    }
}
