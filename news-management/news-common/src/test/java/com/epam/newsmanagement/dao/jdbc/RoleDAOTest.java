package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.Role;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;

/**
 * Created by Yana Volchok on 10.04.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:springconfigtest.xml")
@Transactional
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "/role_test_data.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "/role_test_data.xml", type = DatabaseOperation.DELETE_ALL)
@ActiveProfiles("jdbc")
public class RoleDAOTest {

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Autowired
    RoleDAO roleDAO;

    @Test
    public void createTest() throws DAOException {
        Role role = new Role();
        role.setName("Role Name");
        Long id = roleDAO.create(role).getId();
        Assert.assertNotNull(id);
        List<Role> roleList = roleDAO.getAll();
        Assert.assertEquals(3, roleList.size());
        Assert.assertEquals(role.getName(), roleList.get(2).getName());
    }

    @Test
    public void getAllTest() throws DAOException{

        List<Role> roleList = roleDAO.getAll();
        Assert.assertEquals(2, roleList.size());
    }

    @Test
    public void getByIdTest() throws DAOException{

        Role role = roleDAO.find(1L);
        Assert.assertEquals(1L, (long)role.getId());
    }

    @Test
    public void updateTest() throws DAOException{

        Role role = new Role(1L, "New Name");
        Long idRole = roleDAO.update(role).getId();
        Assert.assertEquals(role.getId(), idRole);
    }

    @Test
    public void deleteTest() throws DAOException{

        roleDAO.delete(1L);
    }
}
