package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.exception.IllegalParameterException;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/13/2016.
 */

/**
 * Extension of GenericService, parametrized by entity <tt>Comment</tt> and key type <tt>Long</tt>
 *
 * Provides with more special for working with this entities
 * @see GenericService
 */
public interface CommentService extends GenericService<Comment, Long> {

    /**
     * Removes entity that was found by unique key (ID).
     *
     * @param idComment - unique key
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void delete(Long idComment) throws ServiceException;

    /**
     * Allows to add several comment.
     *
     * @param commentList comments to add
     * @return list of added comment ID
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void addSeveral(List<Comment> commentList) throws ServiceException;

    /**
     * Allows to remove several comment.
     *
     * @param commentList to remove
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void deleteSeveral(List<Long> commentList) throws ServiceException;

    /**
     * Finds and returns list of comments that were added to the certain news message.
     * @param idNews unique key of news message
     * @return list of found comments
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    List<Comment> findByNewsId(Long idNews) throws ServiceException;
}
