package com.epam.newsmanagement.service;

import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.exception.IllegalParameterException;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/11/2016.
 */
/**
 * Logic layer interface.
 * Provides different operations with DAO layer and domains.
 * Contains partly CRUD operations (except <tt>delete</tt>).
 * Can be parametrized by domain and unique key type.
 *
 * @param <E> entity parameter
 * @param <I> key parameter (ID)
 */
public interface GenericService<E, I> {

    /**
     * Provides with adding entity to the total list.
     * @param entity to insert
     * @return ID - unique key of added entity
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    I add(E entity) throws ServiceException;

    /**
     * Returns list of all entities of certain type.
     * @return list of entities
     * @throws ServiceException if DAOException was caught
     */
    List<E> getAll() throws ServiceException;

    /**
     * Returns entity that was found by unique key (ID).
     * @param id - unique key to find certain entity
     * @return found entity
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    E get(I id) throws ServiceException;
}
