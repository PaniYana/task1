package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/14/2016.
 */
public class AuthorServiceTest {

    @InjectMocks
    private AuthorService authorService = new AuthorServiceImpl();

    @Mock
    private AuthorDAO authorDAO;

    @Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAdd() throws ServiceException{
        try {

            Author author = new Author(1L, "Name" , new Timestamp(1460623612723L));
            Mockito.when(authorDAO.create(author)).thenReturn(author);

            Long id = authorService.add(author);
            Assert.assertEquals(1L, (long)id);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testDelete() throws ServiceException {
        try {
            authorService.delete(1L);
            Mockito.verify(authorDAO).delete(1L);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGetAll() throws ServiceException{
        try{
            List<Author> authorList = new ArrayList<Author>(2);
            authorList.add(new Author(1L, "Name", new Timestamp(1460623612723L)));
            authorList.add(new Author(2L, "Name", new Timestamp(1460623612723L)));
            Mockito.when(authorDAO.getAll()).thenReturn(authorList);
            List<Author> actualAuthorList = authorService.getAll();
            Mockito.verify(authorDAO).getAll();
            Assert.assertEquals(authorList.size(), actualAuthorList.size());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGet() throws ServiceException{
        try{
            Author author = new Author(1L, "Name1", new Timestamp(1460623612723L));
            Mockito.when(authorDAO.find(1L)).thenReturn(author);
            Author actualAuthor = authorService.get(1L);
            Mockito.verify(authorDAO).find(1L);
            Assert.assertEquals(author.getId(), actualAuthor.getId());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testSetExpired() throws ServiceException{
        try{
            Author author = new Author(1L, "Name" , new Timestamp(1460623612223L));
            Mockito.when(authorDAO.find(1L)).thenReturn(author);

            authorService.setExpired(1L);
            Mockito.verify(authorDAO).update(author);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGetNotExpiredList() throws ServiceException{
        try{
            List<Author> authorList = new ArrayList<Author>(2);
            authorList.add(new Author(1L, "Name", new Timestamp(1460723909065L)));
            authorList.add(new Author(2L, "Name", new Timestamp(1460723909065L)));
            Mockito.when(authorDAO.getNotExpiredList()).thenReturn(authorList);
            List<Author> actualAuthorList = authorService.getNotExpiredList();
            Mockito.verify(authorDAO).getNotExpiredList();
            Assert.assertEquals(authorList.size(), actualAuthorList.size());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGetAuthorsByNews() throws ServiceException{
        try{
            List<Author> authorList = new ArrayList<Author>();
            authorList.add(new Author(1L, "Name", new Timestamp(1460623612723L)));
            authorList.add(new Author(2L, "Name", new Timestamp(1460623612723L)));
            Mockito.when(authorDAO.getAuthorsByNews(Mockito.anyLong())).thenReturn(authorList);

            List<Author> actualList = authorService.getAuthorsByNews(1L);
            Mockito.verify(authorDAO).getAuthorsByNews(1L);
            Assert.assertEquals(authorList.size(), actualList.size());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
    }
    }
}
