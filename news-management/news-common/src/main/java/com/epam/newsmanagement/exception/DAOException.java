package com.epam.newsmanagement.exception;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Exception class that can be thrown in case of error or exception in any DAO class method
 */
public class DAOException extends Exception {

    public DAOException(){

    }

    public DAOException(String text, Exception e){
        super(text, e);
    }
}
