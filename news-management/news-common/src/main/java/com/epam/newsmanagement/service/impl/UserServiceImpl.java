package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.IllegalParameterException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/19/2016.
 */

/**
 * Implementation of interface <tt>UserService</tt>

 * @see UserService
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDAO userDAO;

    /**
     * Implementation of method {@link UserService#add(Object)}
     * @see UserService
     */
    @Transactional
    public Long add(User user) throws ServiceException {
        if(user != null){
            try{
                return userDAO.create(user).getId();
            }catch(DAOException e){
                throw new ServiceException(e.getMessage());
            }
        }else{
            throw new IllegalParameterException();
        }
    }

    /**
     * Implementation of method {@link UserService#getAll()}
     * @see UserService
     */
    @Transactional
    public List<User> getAll() throws ServiceException {
        try{
            return userDAO.getAll();
        }catch(DAOException e){
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Implementation of method {@link UserService#get(Object)}
     * @see UserService
     */
    @Transactional
    public User get(Long id) throws ServiceException {
        if(id != null){
            try {
                return userDAO.find(id);
            }catch(DAOException e){
                throw new ServiceException(e.getMessage());
            }
        }else{
            throw new IllegalParameterException();
        }
    }

    /**
     * Implementation of method {@link UserService#delete(Long)}
     * @see UserService
     */
    @Transactional
    public void delete(Long id) throws ServiceException {
        if(id != null){
            try{
                userDAO.delete(id);
            }catch(DAOException e){
                throw new ServiceException(e.getMessage());
            }
        }else{
            throw new IllegalParameterException();
        }
    }

    /**
     * Implementation of method {@link UserService#findByLogin(String)}
     * @see UserService
     */
    @Transactional
    public User findByLogin(String login) throws ServiceException {

        if(login != null && !login.isEmpty()){
            try{
                return userDAO.find(login);
            }catch(DAOException e){
                throw new ServiceException(e.getMessage());
            }
        }else{
            throw new IllegalParameterException();
        }
    }
}
