package com.epam.newsmanagement.domain.criteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.epam.newsmanagement.dao.NewsDAO;

/**
 * Created by Yana_Valchok on 4/7/2016.
 */

/**
 * Utility class that is used for storing and transfer information that will be used
 * as parameters for searching news.
 * Used in {@link NewsDAO#find(SearchCriteria) as argument}
 */
public class SearchCriteria implements Serializable{

    private static final long serialVersionUID = 1L;
    private List<Long> authorList;
    private List<Long> tagList;

    public SearchCriteria(List<Long> authorList, List<Long> tagList) {
        this.authorList = authorList;
        this.tagList = tagList;
    }

    public SearchCriteria() {

        tagList = new ArrayList<Long>();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<Long> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Long> authorList) {
        this.authorList = authorList;
    }

    public List<Long> getTagList() {
        return tagList;
    }

    public void setTagList(List<Long> tagList) {
        this.tagList = tagList;
    }

    public void addTag(Long tagId){
        tagList.add(tagId);
    }

    public boolean isEmpty(){
        if(authorList.size() == 0 && tagList.size() == 0){
            return true;
        }
        return false;
    }
}
