package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import org.hibernate.type.LongType;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Yana_Valchok on 7/4/2016.
 */

public class NewsDAOImpl  extends GenericDAOImpl<News, Long> implements NewsDAO {

    private static final String SQL_ATTACH_AUTHOR_TO_NEWS = "INSERT INTO \"NEWS_AUTHORS\" (NA_NEWS_ID,NA_AUTHOR_ID) VALUES (:NA_NEWS_ID, :NA_AUTHOR_ID)";
    private static final String SQL_ATTACH_TAG_TO_NEWS = "INSERT INTO \"NEWS_TAGS\" (NT_NEWS_ID, NT_TAG_ID) VALUES (:NT_NEWS_ID, :NT_TAG_ID)";
    private static final String SQL_DETACH_AUTHOR_FROM_NEWS =
            "DELETE FROM \"NEWS_AUTHORS\" WHERE NA_NEWS_ID = :NA_NEWS_ID AND NA_AUTHOR_ID = :NA_AUTHOR_ID";
    private static final String SQL_DETACH_TAG_FROM_NEWS =
            "DELETE FROM \"NEWS_TAGS\" WHERE NT_NEWS_ID = :NT_NEWS_ID AND NT_TAG_ID = :NT_TAG_ID";
    /*private static final String SQL_SEARCH_NEWS = "SELECT DISTINCT NEWS.NE_NEWS_ID,  NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT," +
            " NEWS.NE_CREATION_DATE, NEWS.NE_MODIFICATION_DATE FROM " +
            "NEWS JOIN NEWS_AUTHORS ON (NEWS.NE_NEWS_ID = NEWS_AUTHORS.NA_NEWS_ID) " +
            "JOIN NEWS_TAGS ON (NEWS.NE_NEWS_ID = NEWS_TAGS.NT_NEWS_ID) " +
            "LEFT JOIN COMMENTS ON (NEWS.NE_NEWS_ID = COMMENTS.COM_NEWS_ID)";*/
    private static final String HQL_GET_NEWS_BY_AUTHOR =
            "SELECT n FROM Author a JOIN a.news n WHERE a.id = :NA_AUTHOR_ID";
    /*private static final String SQL_GET_ALL_PAGED =
            "SELECT NEWS.NE_NEWS_ID,  NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, NEWS.NE_CREATION_DATE," +
                    "NEWS.NE_MODIFICATION_DATE, COUNT(COMMENTS.COM_COMMENT_ID) COMMENTS_COUNT FROM NEWS LEFT JOIN COMMENTS" +
                    " ON NEWS.NE_NEWS_ID = COMMENTS.COM_NEWS_ID " +
                    "GROUP BY COMMENTS.COM_NEWS_ID, NEWS.NE_NEWS_ID, NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT," +
                    "NEWS.NE_CREATION_DATE, NEWS.NE_MODIFICATION_DATE ORDER BY COMMENTS_COUNT DESC, NEWS.NE_MODIFICATION_DATE DESC";
*/
    private static final String HQL_GET_ALL_PAGED = "Select news From News news left join news.comments as comments group by news.id,news.title,news.shortText,news.fullText," +
            "news.creationDate,news.modificationDate,news.version order by count(comments) desc ,news.modificationDate desc ";

    private static final String SQL_GET_PREV = "SELECT PREV_NEWS from (SELECT NE_NEWS_ID,LAG(NE_NEWS_ID,1) OVER(ORDER BY COMMENTS_COUNT DESC, NE_MODIFICATION_DATE DESC) AS PREV_NEWS " +
            "FROM (SELECT NEWS.NE_NEWS_ID,  NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, NEWS.NE_CREATION_DATE, " +
            "NEWS.NE_MODIFICATION_DATE, COUNT(COMMENTS.COM_COMMENT_ID) COMMENTS_COUNT FROM NEWS LEFT JOIN COMMENTS ON NEWS.NE_NEWS_ID = COMMENTS.COM_NEWS_ID  GROUP BY COMMENTS.COM_NEWS_ID, NEWS.NE_NEWS_ID, " +
            "NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, " +
            "NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, " +
            "NEWS.NE_CREATION_DATE, NEWS.NE_MODIFICATION_DATE " +
            "ORDER BY COMMENTS_COUNT DESC, NEWS.NE_MODIFICATION_DATE DESC)) WHERE NE_NEWS_ID = :NE_NEWS_ID";
    private static final String SQL_GET_NEXT = "SELECT NEXT_NEWS from (SELECT NE_NEWS_ID,LEAD(NE_NEWS_ID,1) OVER(ORDER BY COMMENTS_COUNT DESC, NE_MODIFICATION_DATE DESC) AS NEXT_NEWS " +
            "FROM (SELECT NEWS.NE_NEWS_ID,  NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, NEWS.NE_CREATION_DATE, " +
            "NEWS.NE_MODIFICATION_DATE, COUNT(COMMENTS.COM_COMMENT_ID) COMMENTS_COUNT FROM NEWS LEFT JOIN COMMENTS ON NEWS.NE_NEWS_ID = COMMENTS.COM_NEWS_ID  GROUP BY COMMENTS.COM_NEWS_ID, NEWS.NE_NEWS_ID, " +
            "NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, " +
            "NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, " +
            "NEWS.NE_CREATION_DATE, NEWS.NE_MODIFICATION_DATE " +
            "ORDER BY COMMENTS_COUNT DESC, NEWS.NE_MODIFICATION_DATE DESC)) WHERE NE_NEWS_ID = :NE_NEWS_ID";
    private static final String PREV_NEWS = "PREV_NEWS";
    private static final String NEXT_NEWS = "NEXT_NEWS";
    private static final String SQL_SEARCH_NEWS_PAGED = "SELECT  NE_NEWS_ID, NE_TITLE, NE_SHORT_TEXT, NE_FULL_TEXT, NE_CREATION_DATE," +
            " NE_MODIFICATION_DATE, NE_VERSION FROM(" +
            " SELECT a.NE_NEWS_ID,a.NE_TITLE,a.NE_SHORT_TEXT,a.NE_FULL_TEXT, a.NE_CREATION_DATE,a.NE_MODIFICATION_DATE," +
            " a.NE_VERSION, rownum rw FROM(" +
            " SELECT DISTINCT NEWS.NE_NEWS_ID, NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT,NEWS.NE_CREATION_DATE," +
            " NEWS.NE_MODIFICATION_DATE, NEWS.NE_VERSION, COUNT(COMMENTS.COM_COMMENT_ID) COMMENTS_COUNT " +
            " FROM NEWS JOIN NEWS_AUTHORS ON (NEWS.NE_NEWS_ID = NEWS_AUTHORS.NA_NEWS_ID)" +
            " JOIN NEWS_TAGS ON (NEWS.NE_NEWS_ID = NEWS_TAGS.NT_NEWS_ID ) " +
            " LEFT JOIN COMMENTS ON (NEWS.NE_NEWS_ID = COMMENTS.COM_NEWS_ID)";
    private static final String HQL_SEARCH_NEWS_PAGED =
            "Select news From News news left join news.comments as comments join news.authors authors join news.tags tags where authors.id in :authorList and tags.id in :tagList group by news.id,news.title,news.shortText,news.fullText, " +
                    "news.creationDate,news.modificationDate,news.version order by count(comments) desc ,news.modificationDate desc ";

    private static final String HQL_SEARCH_NEWS_PAGED_AUTHORS =
            "Select news From News news left join news.comments as comments join news.authors authors join news.tags tags where authors.id in :authorList group by news.id,news.title,news.shortText,news.fullText, " +
                    "news.creationDate,news.modificationDate,news.version order by count(comments) desc ,news.modificationDate desc ";

    private static final String HQL_SEARCH_NEWS_PAGED_TAGS =
            "Select news From News news left join news.comments as comments join news.authors authors join news.tags tags where tags.id in :tagList group by news.id,news.title,news.shortText,news.fullText, " +
                    "news.creationDate,news.modificationDate,news.version order by count(comments) desc ,news.modificationDate desc ";

    /* private static final String HQL_SEARCH_NEWS_GROUP_BY_CLAUSE =
            "group by news.id,news.title,news.shortText,news.fullText, " +
            "news.creationDate,news.modificationDate,news.version order by count(comments) desc ,news.modificationDate desc ";*/
    /*private static final String SQL_SEARCH_NEWS_GROUP_BY_CLAUSE = "GROUP BY NEWS.NE_NEWS_ID,NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT,NEWS.NE_FULL_TEXT,NEWS.NE_CREATION_DATE," +
            " NEWS.NE_MODIFICATION_DATE,NE_TITLE, NEWS.NE_VERSION" +
            " ORDER BY COMMENTS_COUNT DESC, NEWS.NE_MODIFICATION_DATE DESC " +
            ") as a)";*/

    public NewsDAOImpl(Class<News> entityClass) {
        super(entityClass);
    }

    @Transactional
    @Override
    public void attachAuthorToNews(Long idNews, Long idAuthor) throws DAOException {
        try {
            SQLQuery query = getCurrentSession().createSQLQuery(SQL_ATTACH_AUTHOR_TO_NEWS);
            query.setParameter("NA_NEWS_ID", idNews);
            query.setParameter("NA_AUTHOR_ID", idAuthor);
            query.executeUpdate();
        }catch(Exception e){
            throw new DAOException("Method attachAuthorToNews in NewsDAO", e);
        }
    }

    @Transactional
    @Override
    public void attachTagToNews(Long idNews, Long idTag) throws DAOException {
        try {
            SQLQuery query = getCurrentSession().createSQLQuery(SQL_ATTACH_TAG_TO_NEWS);
            query.setParameter("NT_NEWS_ID", idNews);
            query.setParameter("NT_TAG_ID", idTag);
            query.executeUpdate();
        }catch(Exception e){
            throw new DAOException("Method attachTagToNews in NewsDAO", e);
        }
    }

    @Transactional
    @Override
    public void detachAuthorFromNews(Long idNews, Long idAuthor) throws DAOException {
        try {
            SQLQuery query = getCurrentSession().createSQLQuery(SQL_DETACH_AUTHOR_FROM_NEWS);
            query.setParameter("NA_NEWS_ID", idNews);
            query.setParameter("NA_AUTHOR_ID", idAuthor);
            query.executeUpdate();
        }catch(Exception e){
            throw new DAOException("Method detachAuthorFromNews in NewsDAO", e);
        }
    }

    @Transactional
    @Override
    public void detachTagFromNews(Long idNews, Long idTag) throws DAOException {
        try {
            SQLQuery query = getCurrentSession().createSQLQuery(SQL_DETACH_TAG_FROM_NEWS);
            query.setParameter("NT_NEWS_ID", idNews);
            query.setParameter("NT_TAG_ID", idTag);
            query.executeUpdate();
        }catch(Exception e){
            throw new DAOException("Method detachAuthorFromNews in NewsDAO", e);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<News> find(SearchCriteria searchCriteria) throws DAOException {
        try {
            /*StringBuilder fullQuery = new StringBuilder(SQL_SEARCH_NEWS_PAGED);
            buildQuery(fullQuery, searchCriteria);
            fullQuery.append(SQL_SEARCH_NEWS_GROUP_BY_CLAUSE);
            SQLQuery query = getCurrentSession().createSQLQuery(fullQuery.toString()).addEntity(News.class);
            List<News> newsList = (List<News>)query.list();
            return newsList;*/

            if(searchCriteria.isEmpty()){
                return getAll();
            }else if(searchCriteria.getAuthorList().isEmpty()){
                Query query = getCurrentSession().createQuery(HQL_SEARCH_NEWS_PAGED_TAGS);
                query.setParameterList("tagList", searchCriteria.getTagList());
                List<News> newsList = (List<News>)query.list();
                return newsList;
            }else if(searchCriteria.getTagList().isEmpty()){
                Query query = getCurrentSession().createQuery(HQL_SEARCH_NEWS_PAGED_AUTHORS);
                query.setParameterList("authorList", searchCriteria.getAuthorList());
                List<News> newsList = (List<News>)query.list();
                return newsList;
            }else {
                Query query = getCurrentSession().createQuery(HQL_SEARCH_NEWS_PAGED);
                query.setParameterList("authorList", searchCriteria.getAuthorList());
                query.setParameterList("tagList", searchCriteria.getTagList());
                List<News> newsList = (List<News>) query.list();
                return newsList;
            }
            /*StringBuilder fullQuery = new StringBuilder(HQL_SEARCH_NEWS_PAGED);
            buildQuery(fullQuery, searchCriteria);
            fullQuery.append(HQL_SEARCH_NEWS_GROUP_BY_CLAUSE);
            Query query = getCurrentSession().createQuery(HQL_SEARCH_NEWS_PAGED);
            if(searchCriteria.getAuthorList() != null) {
                query.setParameterList("authorList", searchCriteria.getAuthorList());
            }
            if(searchCriteria.getTagList() != null) {
                query.setParameterList("tagList", searchCriteria.getTagList());
            }
            List<News> newsList = (List<News>)query.list();
            return newsList;*/
        } catch (Exception e) {
            throw new DAOException("Method find in NewsDAO", e);
        }
    }
    @Transactional(readOnly = true)
    @Override
    public int getNewsCount() throws DAOException {
        try {
            Session session = getCurrentSession();
            Criteria criteria = session.createCriteria(entityClass)
                    .setProjection(Projections.rowCount());
            Long result = (long)criteria.uniqueResult();
            return result.intValue();
        }catch(Exception e){
            throw new DAOException("Method getAll in DAO", e);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<News> getNewsByAuthor(Long idAuthor) throws DAOException {
        try {
            org.hibernate.Query query = getCurrentSession().createQuery(HQL_GET_NEWS_BY_AUTHOR);
            query.setParameter("NA_AUTHOR_ID", idAuthor);
            List list = query.list();
            return (List<News>)list;
        }catch(Exception e){
            throw new DAOException("Method getAuthorsByNews in NewsDAO", e);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<News> getAllPaged(Integer offset, Integer count) throws DAOException {
        try {
            /*SQLQuery query = getCurrentSession().createSQLQuery(SQL_GET_ALL_PAGED).addEntity(News.class);*/
            Query query = getCurrentSession().createQuery(HQL_GET_ALL_PAGED);
            /*query.setParameter("OFFSET", offset);
            query.setParameter("COUNT", count);*/
            /*query.setFirstResult((offset * count) + 1 );
            query.setMaxResults(((offset-1)* count) + 1);*/

            query.setFirstResult((offset * count) - (count - 2) );
            query.setMaxResults(count);
            List<News> newsList = (List<News>)query.list();
            return newsList;
        } catch (Exception e) {
            throw new DAOException("Method getAllPaged in NewsDAO", e);
        }
    }

    @Transactional
    @Override
    public void attachTagToNews(Long idNews, List<Long> idTagList) throws DAOException {
        try {
            SQLQuery query = getCurrentSession().createSQLQuery(SQL_ATTACH_TAG_TO_NEWS);
            query.setParameter("NT_NEWS_ID", idNews);
            for(Long idTag: idTagList) {
                query.setParameter("NT_TAG_ID", idTag);
                query.executeUpdate();
            }
        }catch(Exception e){
            throw new DAOException("Method attachTagToNews in NewsDAO", e);
        }
    }

    @Transactional
    @Override
    public void attachAuthorToNews(Long idNews, List<Long> idAuthorList) throws DAOException {
        try {
            SQLQuery query = getCurrentSession().createSQLQuery(SQL_ATTACH_AUTHOR_TO_NEWS);
            query.setParameter("NA_NEWS_ID", idNews);
            for(Long idAuthor: idAuthorList) {
                query.setParameter("NA_AUTHOR_ID", idAuthor);
                query.executeUpdate();
            }
        }catch(Exception e){
            throw new DAOException("Method attachAuthorToNews in NewsDAO", e);
        }
    }

    @Transactional
    @Override
    public void detachAuthorFromNews(Long idNews, List<Long> idAuthorList) throws DAOException {
        try {
            SQLQuery query = getCurrentSession().createSQLQuery(SQL_DETACH_AUTHOR_FROM_NEWS);
            query.setParameter("NA_NEWS_ID", idNews);
            for(Long idAuthor: idAuthorList) {
                query.setParameter("NA_AUTHOR_ID", idAuthor);
                query.executeUpdate();
            }
        }catch(Exception e){
            throw new DAOException("Method detachAuthorFromNews in NewsDAO", e);
        }
    }

    @Transactional
    @Override
    public void detachTagFromNews(Long idNews, List<Long> idTagList) throws DAOException {
        try {
            SQLQuery query = getCurrentSession().createSQLQuery(SQL_DETACH_TAG_FROM_NEWS);
            query.setParameter("NT_NEWS_ID", idNews);
            for(Long idTag: idTagList) {
                query.setParameter("NT_TAG_ID", idTag);
                query.executeUpdate();
            }
        }catch(Exception e){
            throw new DAOException("Method detachTagFromNews in NewsDAO", e);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Long getPrevious(Long id) throws DAOException {
        try {
            SQLQuery query = getCurrentSession().createSQLQuery(SQL_GET_PREV).addScalar("PREV_NEWS", LongType.INSTANCE);;
            query.setParameter("NE_NEWS_ID", id);
            Long result = (long)query.uniqueResult();
            return result.longValue();
        }catch(Exception e){
            throw new DAOException("Method getPrevious in NewsDAO", e);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Long getNext(Long id) throws DAOException {
        try {
            SQLQuery query = getCurrentSession().createSQLQuery(SQL_GET_NEXT).addScalar("NEXT_NEWS", LongType.INSTANCE);;
            query.setParameter("NE_NEWS_ID", id);
            Long result = (long)query.uniqueResult();
            return result.longValue();
        }catch(Exception e){
            throw new DAOException("Method getNext in NewsDAO", e);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<News> find(SearchCriteria searchCriteria, int offset, int count) throws DAOException {
        try {
            /*StringBuilder fullQuery = new StringBuilder(SQL_SEARCH_NEWS_PAGED);
            buildQuery(fullQuery, searchCriteria);
            fullQuery.append(SQL_SEARCH_NEWS_GROUP_BY_CLAUSE);
            SQLQuery query = getCurrentSession().createSQLQuery(fullQuery.toString()).addEntity(News.class);
            query.setFirstResult((offset * count) - (count - 1) );
            query.setMaxResults(count);
            List<News> newsList = (List<News>)query.list();
            return newsList;*/

            if(searchCriteria.isEmpty()){
                return getAllPaged(offset, count);
            }else if(searchCriteria.getAuthorList().isEmpty()){
                Query query = getCurrentSession().createQuery(HQL_SEARCH_NEWS_PAGED_TAGS);
                query.setParameterList("tagList", searchCriteria.getTagList());

                query.setFirstResult((offset * count) - (count - 1) );
                query.setMaxResults(count);
                List<News> newsList = (List<News>)query.list();
                return newsList;
            }else if(searchCriteria.getTagList().isEmpty()){
                Query query = getCurrentSession().createQuery(HQL_SEARCH_NEWS_PAGED_AUTHORS);
                query.setParameterList("authorList", searchCriteria.getAuthorList());
                query.setFirstResult((offset * count) - (count - 1) );
                query.setMaxResults(count);
                List<News> newsList = (List<News>)query.list();
                return newsList;
            }else {
                Query query = getCurrentSession().createQuery(HQL_SEARCH_NEWS_PAGED);
                query.setParameterList("authorList", searchCriteria.getAuthorList());
                query.setParameterList("tagList", searchCriteria.getTagList());
                query.setFirstResult((offset * count) - (count - 1));
                query.setMaxResults(count);
                List<News> newsList = (List<News>) query.list();
                return newsList;
            }

            /*StringBuilder fullQuery = new StringBuilder(HQL_SEARCH_NEWS_PAGED);
            buildQuery(fullQuery, searchCriteria);
            fullQuery.append(HQL_SEARCH_NEWS_GROUP_BY_CLAUSE);
            Query query = getCurrentSession().createQuery(HQL_SEARCH_NEWS_PAGED);
            if(searchCriteria.getAuthorList() != null) {
                query.setParameterList("authorList", searchCriteria.getAuthorList());
            }
            if(searchCriteria.getTagList() != null) {
                query.setParameterList("tagList", searchCriteria.getTagList());
            }
            query.setFirstResult((offset * count) - (count - 1) );
            query.setMaxResults(count);
            List<News> newsList = (List<News>)query.list();
            return newsList;*/
        } catch (Exception e) {
            throw new DAOException("Method find in NewsDAO", e);
        }
    }

     /*private StringBuilder buildQuery(StringBuilder query, SearchCriteria searchCriteria) {

        if (!searchCriteria.isEmpty()) {

            query.append(" where ");
            if (!searchCriteria.getAuthorList().isEmpty()) {
                query.append("authors.id in :authorList ");
            }
            if (!searchCriteria.getTagList().isEmpty()) {
                query.append("and tags.id in :tagList ");
            }

        }
        return query;
    }*/

    private StringBuilder buildQuery(StringBuilder fullQuery, SearchCriteria searchCriteria){

        if(!searchCriteria.isEmpty()){

            fullQuery.append(" WHERE ");
            if(!searchCriteria.getAuthorList().isEmpty()){
                Iterator<Long> authorIterator = searchCriteria.getAuthorList().iterator();
                fullQuery.append("NA_AUTHOR_ID IN (");
                while(authorIterator.hasNext()){
                    fullQuery.append("'");
                    fullQuery.append(authorIterator.next());
                    fullQuery.append("'");
                    if(authorIterator.hasNext()){
                        fullQuery.append(", ");
                    }else{
                        fullQuery.append(")");
                    }
                }
                if(!searchCriteria.getTagList().isEmpty()){
                    fullQuery.append(" AND ");
                }
            }
            if(!searchCriteria.getTagList().isEmpty()){
                Iterator<Long> tagIterator = searchCriteria.getTagList().iterator();
                fullQuery.append("NT_TAG_ID IN (");
                while(tagIterator.hasNext()){
                    fullQuery.append("'");
                    fullQuery.append(tagIterator.next());
                    fullQuery.append("'");
                    if(tagIterator.hasNext()){
                        fullQuery.append(", ");
                    }else{
                        fullQuery.append(")");
                    }
                }

            }

        }
        fullQuery.append(")");
        return fullQuery;
    }
}
