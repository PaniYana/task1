package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;

/**
 * Created by Yana_Valchok on 4/11/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:springconfigtest.xml")
@Transactional
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "/user_test_data.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "/user_test_data.xml", type = DatabaseOperation.DELETE_ALL)
@ActiveProfiles("jdbc")
public class UserDAOTest {

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Autowired
    UserDAO userDAO;

    @Test
    public void createTest() throws DAOException {
        User user = new User();
        user.setName("Name");
        user.setLogin("Login");
        user.setPassword("password");
        user.setRoleId(2L);
        Long id = userDAO.create(user).getId();
        Assert.assertNotNull(id);
        List<User> userList = userDAO.getAll();
        Assert.assertEquals(3, userList.size());
        Assert.assertEquals(user.getName(), userList.get(2).getName());
    }

    @Test
    public void getAllTest() throws DAOException{

        List<User> userList = userDAO.getAll();
        Assert.assertEquals(2, userList.size());
    }

    @Test
    public void getByIdTest() throws DAOException{

        User user = userDAO.find(1L);
        Assert.assertEquals(1L, (long)user.getId());
    }

    @Test
    public void updateTest() throws DAOException{

        User user = new User(1L, "Name" , "Login" , "password", 2L);
        Long idUser = userDAO.update(user).getId();
        Assert.assertEquals(user.getId(), idUser);
    }

    @Test
    public void deleteTest() throws DAOException{

        userDAO.delete(1L);
    }

    @Test
    public void findByLoginTest() throws DAOException{

        userDAO.find("johns1");
    }
}
