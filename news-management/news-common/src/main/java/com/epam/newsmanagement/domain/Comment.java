package com.epam.newsmanagement.domain;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Domain class, representation of comment entity
 */
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String text;
    private Timestamp creationDate;
    private News news;


    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Comment(Long id, News news ,String text, Timestamp creationDate) {
        this.id = id;
        this.text = text;
        this.creationDate = creationDate;
        this.news = news;

    }

    public Comment() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creatipnDate) {
        this.creationDate = creatipnDate;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if (this == obj){
            return true;
        }
        if (!(obj instanceof Comment)){
            return false;
        }

        Comment comment = (Comment) obj;

        if (!id.equals(comment.id)){
            return false;
        }
        if (!news.equals(comment.news)){
            return false;
        }
        if (!text.equals(comment.text)){
            return false;
        }
        if (!creationDate.equals(comment.creationDate)){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + text.hashCode();
        result = 31 * result + creationDate.hashCode();
        result = 31 * result + news.hashCode();
        return result;
    }
}
