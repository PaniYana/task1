package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.IllegalParameterException;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/12/2016.
 */

/**
 * Extension of GenericService, parametrized by entity <tt>News</tt> and key type <tt>Long</tt>
 *
 * Provides with more special for working with this entities
 * @see GenericService
 */
public interface NewsService extends GenericService<News, Long> {


    /**
     * Removes entity that was found by unique key (ID).
     *
     * @param idNews - unique key
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void delete(Long idNews) throws ServiceException;

    /**
     * Allows to modify news message.
     *
     * @param news entity to edit
     * @return ID of modified entity
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    Long update(News news) throws ServiceException;

    /**
     * Returns current amount of news messages in news list.
     *
     * @return count of news messages
     * @throws ServiceException if DAOException was caught
     */
    int getNewsCount() throws ServiceException;

    /**
     * Allows to mark author as author of certain news message, adds author to news message's author list
     *
     * @param idNews unique key of news message
     * @param idAuthor unique key of author
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void attachAuthorToNews(Long idNews, Long idAuthor) throws ServiceException;

    /**
     * Allows to mark tag as tag of certain news message, adds tag to news message's tag list
     *
     * @param idNews unique key of news message
     * @param idTag unique key of tag
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void attachTagToNews(Long idNews, Long idTag) throws ServiceException;

    /**
    * Provides with finding news by authors and tags
    * @param authorList authors of news messages
    * @param tagList tags of news messages
    * @return list of found news
    * @throws ServiceException if DAOException was caught
    * @throws IllegalParameterException if given argument is wrong or illegal
    */
    List<News> find(List<Long> tagList, List<Long> authorList) throws ServiceException;

    /**
     * Allows to unmark author as author of certain news message, removes author from news message's author list
     *
     * @param idNews unique key of news message
     * @param idAuthor unique key of author
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void detachAuthorFromNews(Long idNews, Long idAuthor) throws ServiceException;

    /**
     * Provides with ummarking tag as tag of certain news message, removes tag to news message's tag list
     *
     * @param idNews unique key of news message
     * @param idTag unique key of tag
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void detachTagFromNews(Long idNews , Long idTag) throws ServiceException;

    /**
     * Allows to mark several tags as tags of certain news message, adds tags to news message's tag list
     *
     * @param idNews unique key of news message
     * @param tagList list of tags to add
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void attachTagToNews(Long idNews, List<Long> tagList) throws ServiceException;

    /**
     * Allows to mark several authors as authors of certain news message, adds authors to news message's author list
     *
     * @param idNews unique key of news message
     * @param authorList list of authors to add
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void attachAuthorToNews(Long idNews, List<Long> authorList) throws ServiceException;

    /**
     * Allows to unmark authors as authors of certain news message, removes authors from news message's author list
     *
     * @param idNews unique key of news message
     * @param idAuthorList list of authors
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void detachAuthorFromNews(Long idNews, List<Long> idAuthorList) throws ServiceException;

    /**
     * Provides with ummarking tags as tags of certain news message, removes tags to news message's tag list
     *
     * @param idNews unique key of news message
     * @param idTagList list of tags
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    void detachTagFromNews(Long idNews , List<Long> idTagList) throws ServiceException;

    /**
     * Returns limited list of all news of certain type.List is limited by page offset and amount of
     * records on page.
     * @return list of news
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    List<News> getAllPaged(Integer offset, Integer count) throws ServiceException;

    /**
     * Returns previous news concerning current;
     * @param id - unique key of current news;
     * @return previous news
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    News getPrevious(Long id) throws ServiceException;

    /**
     * Returns next news concerning current;
     * @param id - unique key of current news;
     * @return next news
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    News getNext(Long id) throws ServiceException;

    /**
     * Provides with finding news by authors and tags (paginated)
     * @param authorList authors of news messages
     * @param tagList tags of news messages
     * @return list of found news
     * @throws ServiceException if DAOException was caught
     * @throws IllegalParameterException if given argument is wrong or illegal
     */
    List<News> find(List<Long> tagList, List<Long> authorList, int offset, int count) throws ServiceException;
}
