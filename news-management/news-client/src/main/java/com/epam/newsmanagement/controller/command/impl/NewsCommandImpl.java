package com.epam.newsmanagement.controller.command.impl;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagement;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Yana_Valchok on 6/10/2016.
 */
@Component
public class NewsCommandImpl implements Command {

    private static final String PAGE = "page";
    private static final int COUNT = 3;
    private static final String RETURN_PAGE = "/news/list";
    private static final String NEWS_LIST = "newsList";
    private static final String OFFSET = "offset";
    private static final String AUTHOR_LIST = "authorList";
    private static final String TAG_LIST = "tagList";
    private static final String NEWS_COUNT = "newsCount";

    @Autowired
    private NewsManagement newsManagement;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @Autowired
    private NewsService newsService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String offset = request.getParameter(PAGE);
        List<NewsTO> newsList = null;
        List<Author> authorList = null;
        List<Tag> tagList = null;
        int newsCount = 0;
        if(offset == null) {
            offset = "1";
        }
        try {
            newsList = newsManagement.findAllPaged(Integer.parseInt(offset),COUNT);
            authorList = authorService.getAll();
            tagList = tagService.getAll();
            newsCount = newsService.getNewsCount();
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage());
        }
        request.setAttribute(NEWS_LIST, newsList);
        request.setAttribute(OFFSET, offset);
        request.setAttribute(AUTHOR_LIST, authorList);
        request.setAttribute(TAG_LIST, tagList);
        request.setAttribute(NEWS_COUNT, newsCount);
        return RETURN_PAGE;
    }
}
