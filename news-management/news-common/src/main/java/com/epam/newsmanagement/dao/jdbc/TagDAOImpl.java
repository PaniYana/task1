package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/7/2016.
 */

/**
 * Implementation of interface <tt>TagDAO</tt>
 * Adapted to the work with Oracle database.
 * @see TagDAO
 */
@Repository
@Profile("jdbc")
public class TagDAOImpl implements TagDAO{

    private static final String TAG_ID = "TAG_TAG_ID";
    private static final String TAG_NAME = "TAG_TAG_NAME";

    final String SQL_GET_ALL = "SELECT TAG_TAG_ID, TAG_TAG_NAME FROM TAGS";
    final String SQL_CREATE = "INSERT INTO TAGS (TAG_TAG_ID, TAG_TAG_NAME) VALUES (TAGS_SEQ.nextval, ?)";
    final String SQL_UPDATE =
            "UPDATE TAGS SET TAG_TAG_NAME = ? WHERE TAG_TAG_ID = ?";
    final String SQL_GET_BY_ID = "SELECT TAG_TAG_ID, TAG_TAG_NAME FROM TAGS WHERE TAG_TAG_ID = ?";
    final String SQL_DELETE_BY_ID = "DELETE FROM TAGS WHERE TAG_TAG_ID = ?";
    final String SQL_GET_TAG_BY_NEWS = "SELECT TAG_TAG_ID, TAG_TAG_NAME FROM TAGS JOIN NEWS_TAGS ON (TAG_TAG_ID = NT_TAG_ID) WHERE NT_NEWS_ID = ?";



    @Autowired
    private DataSource datasource;

    /**
     * Implementation of method {@link TagDAO#getAll()}
     * @see TagDAO
     */
    public List<Tag> getAll() throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Tag> tagList;

        try{
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_ALL);
            resultSet = statement.executeQuery();

            tagList = new ArrayList<Tag>();
            Tag tag;
            while(resultSet.next()){
                tag = getEntityFromResultSet(resultSet);

                tagList.add(tag);
            }
            return tagList;
        }catch(SQLException e){
            throw new DAOException("Method getAll in TagDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link TagDAO#create(Object)}
     * @see TagDAO
     */
    public Tag create(Tag tag) throws DAOException {

        final String[] keys = {"TAG_TAG_ID"};

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_CREATE, keys);
            statement.setString(1, tag.getName());

            statement.executeUpdate();
            Long id = -1L;
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            return find(id);
        }catch(SQLException e){
            throw new DAOException("Method create in TagDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link TagDAO#update(Object)}
     * @see TagDAO
     */
    public Tag update(Tag tag) throws DAOException {

        final String[] keys = {"TAG_TAG_ID"};

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_UPDATE, keys);
            statement.setString(1, tag.getName());
            statement.setLong(2, tag.getId());

            statement.executeUpdate();

            return tag;

        }catch(SQLException e){
            throw new DAOException("Method create in TagDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link TagDAO#find(Object)}
     * @see TagDAO
     */
    public Tag find(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_BY_ID);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();

            Tag tag = null;
            if(resultSet.next()){
                tag = getEntityFromResultSet(resultSet);

            }
            return tag;
        }catch(SQLException e){
            throw new DAOException("Method find in TagDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link TagDAO#delete(Object)}
     * @see TagDAO
     */
    public void delete(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_DELETE_BY_ID);
            statement.setLong(1, id);

            statement.executeUpdate();

        }catch(SQLException e){
            throw new DAOException("Method delete in TagDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    private Tag getEntityFromResultSet(ResultSet resultSet) throws SQLException{
        Tag tag = new Tag();
        tag.setId(resultSet.getLong(TAG_ID));
        tag.setName(resultSet.getString(TAG_NAME));
        return tag;
    }

    /**
     * Implementation of method {@link TagDAO#getTagsByNews(Long)}
     * @see TagDAO
     */
    public List<Tag> getTagsByNews(Long idNews) throws DAOException {

        List<Tag> tagList;

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_TAG_BY_NEWS);
            statement.setLong(1, idNews);
            resultSet = statement.executeQuery();

            Tag tag;
            tagList = new ArrayList<Tag>();
            while(resultSet.next()){
                tag = getEntityFromResultSet(resultSet);

                tagList.add(tag);
            }
            return tagList;
        }catch(SQLException e){
            throw new DAOException("Method getTagsByNews in TagDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    @Override
    public List<Long> create(List<Tag> tags) throws DAOException {

        final String[] keys = {"TAG_TAG_ID"};

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_CREATE, keys);

            for(Tag item: tags) {
                statement.setString(1, item.getName());
                statement.addBatch();
            }

            statement.executeBatch();
            List<Long> idList = new ArrayList<Long>();
            resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                idList.add(resultSet.getLong(1));
            }
            return idList;
        }catch(SQLException e){
            throw new DAOException("Method create in TagDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }
}
