package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.Role;

/**
 * Created by Yana_Valchok on 7/5/2016.
 */
public class RoleDAOImpl extends GenericDAOImpl<Role, Long> implements RoleDAO {

    public RoleDAOImpl(Class<Role> entityClass) {
        super(entityClass);
    }
}
