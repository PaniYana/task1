package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.exception.DAOException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Yana_Valchok on 7/5/2016.
 */
public class UserDAOImpl extends GenericDAOImpl<User, Long> implements UserDAO {

    public UserDAOImpl(Class<User> entityClass) {
        super(entityClass);
    }

    @Transactional(readOnly = true)
    @Override

    public User find(String login) throws DAOException {
        try {
            Session session = getCurrentSession();
            Criteria criteria = session.createCriteria(User.class)
                    .add(Restrictions.eq("login", login));
            return (User)criteria.uniqueResult();
            /*User user = session.load(User.class, login);
            return user;*/
        }catch(Exception e){
            throw new DAOException("Method getNotExpiredList in AuthorDAO", e);
        }
    }
}
