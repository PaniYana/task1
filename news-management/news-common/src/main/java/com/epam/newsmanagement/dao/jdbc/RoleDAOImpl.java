package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.Role;
import com.epam.newsmanagement.exception.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/7/2016.
 */

/**
 * Implementation of interface <tt>RoleDAO</tt>
 * Adapted to the work with Oracle database.
 * @see RoleDAO
 */
@Repository
@Profile("jdbc")
public class RoleDAOImpl implements RoleDAO {

    private static final String ROLE_ID = "RO_ROLE_ID";
    private static final String ROLE_NAME = "RO_ROLE_NAME";

    private static final String SQL_GET_BY_ID = "SELECT RO_ROLE_ID, RO_ROLE_NAME FROM ROLES WHERE RO_ROLE_ID = ?";
    private static final String SQL_GET_ALL = "SELECT RO_ROLE_ID, RO_ROLE_NAME FROM ROLES";
    final String CREATE_SQL = "INSERT INTO ROLES (RO_ROLE_ID, RO_ROLE_NAME) VALUES (ROLES_SEQ.nextval, ?)";
    final String UPDATE_SQL =
            "UPDATE ROLES SET RO_ROLE_NAME = ? WHERE RO_ROLE_ID = ?";
    final String DELETE_BY_ID_SQL = "DELETE FROM ROLES WHERE RO_ROLE_ID = ?";



    @Autowired
    private DataSource datasource;

    /**
     * Implementation of method {@link RoleDAO#find(Object)}
     * @see RoleDAO
     */
    public Role find(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_BY_ID);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();

            Role role = null;
            if(resultSet.next()){
                role = buildEntity(resultSet);

            }
            return role;
        }catch(SQLException e){
            throw new DAOException("Method find in RoleDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link RoleDAO#getAll()}
     * @see RoleDAO
     */
    public List<Role> getAll() throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Role> roleList;

        try{
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_ALL);
            resultSet = statement.executeQuery();

            roleList = new ArrayList<Role>();
            Role role;
            while(resultSet.next()){
                role = buildEntity(resultSet);

                roleList.add(role);
            }
            return roleList;
        }catch(SQLException e){
            throw new DAOException("Method getAll in RoleDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link RoleDAO#create(Object)}
     * @see RoleDAO
     */
    public Role create(Role role) throws DAOException {

        final String[] keys = {"RO_ROLE_ID"};

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(CREATE_SQL, keys);
            statement.setString(1, role.getName());

            statement.executeUpdate();
            Long id = -1L;
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            return find(id);
        }catch(SQLException e){
            throw new DAOException("Method create in RoleDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link RoleDAO#update(Object)}
     * @see RoleDAO
     */
    public Role update(Role role) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(UPDATE_SQL);
            statement.setString(1, role.getName());
            statement.setLong(2, role.getId());
            statement.executeUpdate();

            return role;

        }catch(SQLException e){
            throw new DAOException("Method update in RoleDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link RoleDAO#delete(Object)}
     * @see RoleDAO
     */
    public void delete(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(DELETE_BY_ID_SQL);
            statement.setLong(1, id);

            statement.executeUpdate();

        }catch(SQLException e){
            throw new DAOException("Method delete in RoleDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    private Role buildEntity(ResultSet resultSet) throws SQLException{
        Role role =  new Role();
        role.setId(resultSet.getLong(ROLE_ID));
        role.setName(resultSet.getString(ROLE_NAME));
        return role;
    }
}
