<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div id="content" class="wrapper">
    <c:url value="/login_url" var="loginUrl"/>
    <form id="login-form" class="form-horizontal col-md-6 col-md-offset-3" method="post" action="${loginUrl}">
        <c:if test="${param.error != null}">
            <p>Invalid username / password</p>
        </c:if>
            <div class="form-group">

                <label for="login" class="col-lg-2 control-label">Login</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="login" name="j_username" placeholder="Login"/>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-lg-2 control-label">Password</label>
                <div class="col-lg-10">
                    <input type="password" class="form-control" id="password" name="j_password" placeholder="Password"/>
                </div>
            </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn-lg btn-block btn-primary">Login</button>
            </div>
        </div>
    </form>
</div>
