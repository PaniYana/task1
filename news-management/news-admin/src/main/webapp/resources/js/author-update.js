$(document).ready(function() {
        $('.edit-button').click(function(){
            var input = $(this).data("author-id");

            $('#author-name-' + input).prop('disabled', false);

            $('#edit-button-' + input).toggleClass('hidden-button');
            $('#update-button-' + input).toggleClass('hidden-button');
            $('#expire-button-' + input).toggleClass('hidden-button');
            $('#cancel-button-' + input).toggleClass('hidden-button');
    });

    $('.cancel-button').click(function(){
        var input = $(this).data("author-id");
        $('#author-name-' + input).prop('disabled', true);

        $('#edit-button-' + input).toggleClass('hidden-button');
        $('#update-button-' + input).toggleClass('hidden-button');
        $('#expire-button-' + input).toggleClass('hidden-button');
        $('#cancel-button-' + input).toggleClass('hidden-button');
    });

});
