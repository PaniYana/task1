package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.domain.to.NewsAddTO;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.IllegalParameterException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.*;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/15/2016.
 */

/**
 * Implementation of interface <tt>NewsManagement</tt>

 * @see NewsManagement
 */
@Service
public class NewsManagementImpl implements NewsManagement {

    @Autowired
    private CommentService commentService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    /**
     * Implementation of method {@link NewsManagement#add(News, List, List)}
     * @see NewsManagement
     */
    @Transactional
    public Long add(News news, List<Long> authorList, List<Long> tagList) throws ServiceException {
        if(news != null && authorList != null){
            Long idNews = newsService.add(news);

            newsService.attachAuthorToNews(idNews, authorList);
            if(tagList != null){

                newsService.attachTagToNews(idNews, tagList);
            }
            return idNews;
        }else{
            throw new IllegalParameterException();
        }
    }

    /**
     * Implementation of method {@link NewsManagement#find(Long)}
     * @see NewsManagement
     */
    @Transactional
    public NewsTO find(Long id) throws ServiceException {
        if(id != null) {
            return buildNewsTO(id);
        }else{
            throw new IllegalParameterException();
        }
    }

    @Transactional
    public NewsAddTO findForUpdate(Long id) throws ServiceException {
        if(id != null) {
            News news = newsService.get(id);
            List<Author> authorList = authorService.getAuthorsByNews(id);
            List<Long> authorIdList = new ArrayList<Long>();
            for(Author author: authorList){
                authorIdList.add(author.getId());
            }
            List<Tag> tagList = tagService.getTagsByNews(id);
            List<Long> tagIdList = new ArrayList<Long>();
            for(Tag tag: tagList){
                tagIdList.add(tag.getId());
            }
            NewsAddTO newsTO = new NewsAddTO();
            newsTO.setNews(news);
            newsTO.setAuthorList(authorIdList);
            newsTO.setTagList(tagIdList);
            return newsTO;
        }else{
            throw new IllegalParameterException();
        }
    }

    /**
     * Implementation of method {@link NewsManagement#delete(List)} )}
     * @see NewsManagement
     */
    @Transactional
    public void delete(List<Long> idNews) throws ServiceException {

        for(Long id: idNews) {
            NewsTO news = find(id);
            List<Author> authorList = news.getAuthorList();
            List<Tag> tagList = news.getTagList();
            if (id != null && authorList != null && tagList != null) {
                List<Long> tagIdList = buildIdTagList(tagList);
                newsService.detachTagFromNews(id, tagIdList);
                List<Long> authorIdList = buildIdAuthorList(authorList);
                newsService.detachAuthorFromNews(id, authorIdList);
                deleteNewsComments(id);
                newsService.delete(id);
            } else {
                throw new IllegalParameterException();
            }
        }
    }

    private List<Long> buildIdAuthorList(List<Author> authorList){
        List<Long> authorIdList = new ArrayList<Long>();
        for(Author author: authorList){
            authorIdList.add(author.getId());
        }
        return authorIdList;
    }

    private List<Long> buildIdTagList(List<Tag> tagList){
        List<Long> tagIdList = new ArrayList<Long>();
        for(Tag tag: tagList){
            tagIdList.add(tag.getId());
        }
        return tagIdList;
    }

    private void deleteNewsComments(Long idNews) throws ServiceException{
        List<Comment> commentList = commentService.findByNewsId(idNews);
        for(Comment comment: commentList){
            commentService.delete(comment.getId());
        }
    }

    /**
     * Implementation of method {@link NewsManagement#find(List, List)}
     * @see NewsManagement
     */
    @Transactional
    public List<NewsTO> find(List<Long> authorList, List<Long> tagList) throws ServiceException {
        if(authorList != null && tagList != null) {
            List<News> newsList = newsService.find(authorList, tagList);
            List<NewsTO> newsTOList = new ArrayList<NewsTO>();
            for (News item : newsList) {
                newsTOList.add(buildNewsTO(item.getId()));
            }
            return newsTOList;
        }else{
            return findAll();
        }
    }

    /**
     * Implementation of method {@link NewsManagement#findAll()}
     * @see NewsManagement
     */
    @Transactional
    public List<NewsTO> findAll() throws ServiceException {
        List<News> newsList = newsService.getAll();
        List<NewsTO> newsTOList = new ArrayList<NewsTO>();
        for(News item: newsList){
            newsTOList.add(buildNewsTO(item.getId()));
        }
        return newsTOList;
    }

    /**
     * Implementation of method {@link NewsManagement#findAllPaged(Integer, Integer)}
     * @see NewsManagement
     */
    @Transactional
    public List<NewsTO> findAllPaged(Integer offset, Integer count) throws ServiceException {
        List<News> newsList = newsService.getAllPaged(offset, count);
        List<NewsTO> newsTOList = new ArrayList<NewsTO>();
        for(News item: newsList){
            newsTOList.add(buildNewsTO(item.getId()));
        }
        return newsTOList;
    }

    @Transactional
    private NewsTO buildNewsTO(Long idNews)throws ServiceException{
        News news = newsService.get(idNews);
        List<Author> authorList = authorService.getAuthorsByNews(idNews);
        List<Tag> tagList = tagService.getTagsByNews(idNews);
        List<Comment> commentList = commentService.findByNewsId(idNews);
        NewsTO newsTO = new NewsTO();
        newsTO.setNews(news);
        newsTO.setAuthorList(authorList);
        newsTO.setTagList(tagList);
        newsTO.setCommentList(commentList);
        return newsTO;
    }

    @Transactional
    private NewsTO buildNewsTO(Long idNews, News news)throws ServiceException{
        List<Author> authorList = authorService.getAuthorsByNews(idNews);
        List<Tag> tagList = tagService.getTagsByNews(idNews);
        List<Comment> commentList = commentService.findByNewsId(idNews);
        NewsTO newsTO = new NewsTO();
        newsTO.setNews(news);
        newsTO.setAuthorList(authorList);
        newsTO.setTagList(tagList);
        newsTO.setCommentList(commentList);
        return newsTO;
    }

    /**
     * Implementation of method {@link NewsManagement#update(NewsAddTO)}
     * @see NewsManagement
     */
    @Transactional
    @Override
    public Long update(NewsAddTO news) throws ServiceException, StaleObjectStateException {
        Long newsId = newsService.update(news.getNews());
        NewsTO newsToUpdate = find(newsId);
        List<Long> authorIdList = new ArrayList<Long>();
        List<Long> tagIdList = new ArrayList<Long>();
        for(Author author: newsToUpdate.getAuthorList()){
            authorIdList.add(author.getId());
        }
        for(Tag tag: newsToUpdate.getTagList()){
            tagIdList.add(tag.getId());
        }
        setNewAuthors(news, authorIdList);
        setNewTags(news, tagIdList);
        return news.getNews().getId();
    }

    private void setNewAuthors(NewsAddTO news, List<Long> authorList) throws ServiceException{
        List<Long> authors = news.getAuthorList();
        List<Long> authorsToAttach = new ArrayList<Long>(authors);
        authorsToAttach.removeAll(authorList);
        newsService.attachAuthorToNews(news.getNews().getId(), authorsToAttach);
        List<Long> authorsToDetach = new ArrayList<Long>(authorList);
        authorsToDetach.removeAll(authors);
        newsService.detachAuthorFromNews(news.getNews().getId(), authorsToDetach);
    }

    private void setNewTags(NewsAddTO news, List<Long> tagList) throws ServiceException{
        List<Long> tags = news.getTagList();
        List<Long> tagsToAttach = new ArrayList<Long>(tags);
        tagsToAttach.removeAll(tagList);
        newsService.attachTagToNews(news.getNews().getId(), tagsToAttach);
        List<Long> tagsToDetach = new ArrayList<Long>(tagList);
        tagsToDetach.removeAll(tags);
        newsService.detachTagFromNews(news.getNews().getId(), tagsToDetach);
    }

    /**
     * Implementation of method {@link NewsManagement#getPrevious(Long)}
     * @see NewsManagement
     */
    @Transactional
    @Override
    public NewsTO getPrevious(Long id) throws ServiceException {
        if(id != null) {
            News news = newsService.getPrevious(id);
            return buildNewsTO(id, news);
        }else{
            throw new IllegalParameterException();
        }
    }

    /**
     * Implementation of method {@link NewsManagement#getNext(Long)}
     * @see NewsManagement
     */
    @Transactional
    @Override
    public NewsTO getNext(Long id) throws ServiceException {
        if(id != null) {
            News news = newsService.getNext(id);
            return buildNewsTO(id, news);
        }else{
            throw new IllegalParameterException();
        }
    }

    /**
     * Implementation of method {@link NewsManagement#find(List, List, int, int)}
     * @see NewsManagement
     */
    @Transactional
    @Override
    public List<NewsTO> find(List<Long> tagList, List<Long> authorList, int offset, int count) throws ServiceException {
        if(authorList != null && tagList != null) {
            List<News> newsList = newsService.find(authorList, tagList, offset, count);
            List<NewsTO> newsTOList = new ArrayList<NewsTO>();
            for (News item : newsList) {
                newsTOList.add(buildNewsTO(item.getId()));
            }
            return newsTOList;
        }else{
            return findAllPaged(offset, count);
        }
    }
}
