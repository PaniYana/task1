$(document).ready(function() {
    $('.edit-button').click(function(){
        var input = $(this).data("tag-id");

        $('#tag-name-' + input).prop('disabled', false);

        $('#edit-button-' + input).toggleClass('hidden-button');
        $('#update-button-' + input).toggleClass('hidden-button');
        $('#delete-button-' + input).toggleClass('hidden-button');
        $('#cancel-button-' + input).toggleClass('hidden-button');
    });

    $('.cancel-button').click(function(){
        var input = $(this).data("tag-id");
        $('#tag-name-' + input).prop('disabled', true);

        $('#edit-button-' + input).toggleClass('hidden-button');
        $('#update-button-' + input).toggleClass('hidden-button');
        $('#delete-button-' + input).toggleClass('hidden-button');
        $('#cancel-button-' + input).toggleClass('hidden-button');
    });

});
