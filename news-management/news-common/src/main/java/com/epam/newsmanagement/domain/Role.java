package com.epam.newsmanagement.domain;

import java.io.Serializable;

/**
 * Created by Yana_Valchok on 4/7/2016.
 */
/**
 * Domain class, representation of role entity
 */
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;

    public Role(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Role() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if (this == obj){
            return true;
        }
        if (!(obj instanceof Role)){
            return false;
        }

        Role role = (Role) obj;

        if (!id.equals(role.id)){
            return false;
        }
        if (!name.equals(role.name)){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
