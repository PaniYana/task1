package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Extension of GenericDao, parametrized by entity <tt>User</tt> and key type <tt>Long</tt>
 *
 * Provides with more special methods for working with this entity in database.
 * Contains more methods for finding entities.
 * @see GenericDAO
 */
public interface UserDAO extends GenericDAO<User, Long>{


    /**
     * Provides with finding user entity in database table by login.
     * @param login - parameter to find user
     * @return found user entity
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    User find(String login) throws DAOException;
}
