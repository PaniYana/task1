<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="navbar navbar-default navbar-fixed-top clearfix">
    <div>
        <a class="navbar-brand">News portal - Administration</a>
    </div>
    <sec:authorize access="isAuthenticated()">
        <div id="hello-message" class="col-md-2">Hello, <sec:authentication property="principal.username" /></div>
        <div id="logout-wrapper">
                    <a id="logout" class="btn btn-sm btn-danger" href="/logout" role="button">Logout</a>
                </sec:authorize>
                <div class="localization-links">
                    <a>EN</a>
                    <a>RU</a>
                </div>
        </div>
</div>
