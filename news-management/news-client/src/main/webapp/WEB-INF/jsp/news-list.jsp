<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="header" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="footer" %>
<html>
<head>
    <meta content="IE=edge, chrome=1" http-equiv="X-UA-Compatible"/>
    <title>News Management</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-multiselect.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pagination.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/multiselect.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
</head>
<body>
<header:header/>
<div id="content" class="wrapper">

    <div class="container jumbotron col-md-offset-1 col-md-10">

        <form class="form-horizontal" method="get" action="controller">
            <input type="hidden" name="command" value="filterCommandImpl">

            <div class="news-row search-bar">
                <div class="author-select-filter form-group">
                    <div class="col-lg-10">
                        <label for="authorList"></label>
                        <select name="authorList" size="5" class="form-control" multiple="multiple" id="authorList">
                            <c:forEach items="${authorList}" var="author">
                                <c:choose>
                                    <c:when test="${authorListSelected != null}">
                                        <c:set var="flag" value="false"/>
                                        <c:forEach items="${authorListSelected}" var="selected">
                                            <c:if test="${author.id == selected}">
                                                <c:set var="flag" value="true"/>
                                            </c:if>
                                        </c:forEach>
                                        <c:choose>
                                            <c:when test="${flag == true}">
                                                <option selected value="${author.id}">${author.name}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option  value="${author.id}">${author.name}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${author.id}">${author.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="tag-select-filter form-group">
                    <div class="col-lg-10">
                        <label for="tagList"></label>
                        <select name="tagList" size="5" multiple="multiple" class="form-control" id="tagList">

                            <c:forEach items="${tagList}" var="tag">
                                <c:choose>
                                    <c:when test="${tagListSelected != null}">
                                        <c:set var="flag" value="false"/>
                                        <c:forEach items="${tagListSelected}" var="selected">
                                            <c:if test="${tag.id == selected}">
                                                <c:set var="flag" value="true"/>
                                            </c:if>
                                        </c:forEach>
                                        <c:choose>
                                            <c:when test="${flag == true}">
                                                <option selected value="${tag.id}">${tag.name}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${tag.id}">${tag.name}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${tag.id}">${tag.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="btn-wrapper buttons-filter col-lg-10 col-lg-offset-2">
                        <button type="submit" id="btn-filter" class="btn btn-primary btn-sm">Filter</button>
                        <a href="controller?command=newsCommandImpl" class="btn btn-default btn-sm">Reset</a>
                    </div>
                </div>
            </div>
        </form>
        <c:if test="${newsList != null}">

            <c:if test="${not empty authorListSelected or not empty tagListSelected}">
                <input type="hidden" id="searchCriteriaValue" value="1"/>
            </c:if>
            <input type="hidden" id="searchCriteriaValue" value="0"/>

            <c:forEach var="news" items="${newsList}">

                <div class="news-record row">

                    <div class="list-news-title">
                        <a href="/newsmanagement/controller?command=showCommandImpl&id=${news.news.id}"><c:out
                                value="${news.news.title}"/></a>
                    </div>
                    <c:if test="${news.authorList != null}">
                        <div class="author-labels">
                            <c:forEach var="author" items="${news.authorList}">
                                <span class="label label-primary">${author.name}</span>
                            </c:forEach>
                        </div>
                    </c:if>
                    <p class="news-short-text"><c:out value="${news.news.shortText}"/></p>

                    <div class="news-creation-date">
                        <span class="creation-date"><c:out value="${news.news.creationDate}"/></span>
                    </div>
                    <c:if test="${news.tagList != null}">
                        <div class="tag-labels">
                            <c:forEach var="tag" items="${news.tagList}">
                                <span class="label label-default">${tag.name}</span>
                            </c:forEach>
                        </div>
                    </c:if>

                </div>
            </c:forEach>

        </c:if>

        <div class="page col-md-offset-3">
            <input id="newsCount" type="hidden" value="${newsCount}"/>
            <input id="offset" type="hidden" value="${offset}"/>
            <ul id="pagination-demo" class="pagination">
                <li class="first"><a href="">&laquo;</a></li>
                <li class="prev"></li>
                <li class="next"><a href="">&gt;</a></li>
                <li class="last"><a href="">&raquo;</a></li>
            </ul>
        </div>
    </div>

</div>
<footer:footer/>
</body>
</html>