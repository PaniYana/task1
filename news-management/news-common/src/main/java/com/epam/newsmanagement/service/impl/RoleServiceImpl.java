package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.Role;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.RoleService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/15/2016.
 */

/**
 * Implementation of interface <tt>RoleService</tt>

 * @see RoleService
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleDAO roleDAO;

    private static final Logger LOGGER = LogManager.getLogger(RoleServiceImpl.class);

    /**
     * Implementation of method {@link RoleService#add(Object)}
     * @see RoleService
     */
    @Transactional
    public Long add(Role role) throws ServiceException {
        Long createdId = null;
        if(role != null){
            try {
                createdId = roleDAO.create(role).getId();
            }catch(DAOException e){
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return createdId;
    }

    /**
     * Implementation of method {@link RoleService#getAll()}
     * @see RoleService
     */
    @Transactional
    public List<Role> getAll() throws ServiceException {
        List<Role> roleList = null;
        try {
            roleList = roleDAO.getAll();
        }catch(DAOException e){
            LOGGER.error(e);
            throw new ServiceException(e.getMessage());
        }
        return roleList;
    }

    /**
     * Implementation of method {@link RoleService#get(Object)}
     * @see RoleService
     */
    @Transactional
    public Role get(Long id) throws ServiceException {
        Role role = null;
        if(id != null){
            try {
                role = roleDAO.find(id);
            }catch(DAOException e) {
                LOGGER.error(e);
                throw new ServiceException(e.getMessage());
            }
        }
        return role;
    }
}
