package com.epam.newsmanagement.domain.to;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/13/2016.
 */

/**
 * Domain class that storage, transfer and represent full information about news message
 * (including authors, tags and comments)
 */
public class NewsTO implements Serializable {

    public static final long serialVersionUID = 1L;
    private News news;
    private List<Author> authorList;
    private List<Tag> tagList;
    private List<Comment> commentList;

    public NewsTO(News news, List<Author> authorList, List<Tag> tagList, List<Comment> commentList) {
        this.news = news;
        this.authorList = authorList;
        this.tagList = tagList;
        this.commentList = commentList;
    }

    public NewsTO() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof NewsTO)) {
            return false;
        }

        NewsTO newsTO = (NewsTO) obj;

        if (!getNews().equals(newsTO.getNews())){
            return false;
        }
        if (!getAuthorList().equals(newsTO.getAuthorList())){
            return false;
        }
        if (!getTagList().equals(newsTO.getTagList())){
            return false;
        }
        if(!getCommentList().equals(newsTO.getCommentList())){
            return false;
        }

        return true;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result += ((news == null) ? 0 : news.hashCode());
        result = 31 * result + ((authorList == null) ? 0 : authorList.hashCode());
        result = 31 * result + ((tagList == null) ? 0 : tagList.hashCode());
        result = 31 * result + ((commentList == null) ? 0 : commentList.hashCode());
        return result;
    }
}
