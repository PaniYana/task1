package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.TagServiceImpl;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/14/2016.
 */
public class TagServiceTest {

    @InjectMocks
    private TagService tagService =  new TagServiceImpl();

    @Mock
    private TagDAO tagDAO;

    @Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testAdd() throws ServiceException {
        try {

            Tag tag = new Tag(1L, "Tag text");
            Mockito.when(tagDAO.create(tag)).thenReturn(tag);

            Long id = tagService.add(tag);
            Assert.assertEquals(1L, (long)id);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGetAll() throws ServiceException{
        try{
            List<Tag> tagList = new ArrayList<Tag>(2);
            tagList.add(new Tag(1L, "tag text"));
            tagList.add(new Tag(2L, "tag text"));
            Mockito.when(tagService.getAll()).thenReturn(tagList);
            List<Tag> actualTagList = tagService.getAll();
            Mockito.verify(tagDAO).getAll();
            Assert.assertEquals(tagList.size(), actualTagList.size());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGet() throws ServiceException{
        try{
            Tag tag = new Tag(1L, "Tag text");
            Mockito.when(tagDAO.find(1L)).thenReturn(tag);
            Tag actualTag = tagService.get(1L);
            Mockito.verify(tagDAO).find(1L);
            Assert.assertEquals(tag.getId(), actualTag.getId());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testAddSeveral() throws ServiceException{
        try {
            List<Tag> tagList = new ArrayList<Tag>();
            tagList.add(new Tag(1L,"Text"));
            tagList.add(new Tag(2L, "Text"));
            tagList.add(new Tag(3L,"Text"));

            List<Long> idList = new ArrayList<Long>();
            idList.add(1L);
            idList.add(2L);
            idList.add(3L);

            Mockito.when(tagDAO.create(tagList)).thenReturn(idList);

            List<Long> actualIdList = tagService.addSeveral(tagList);
            Mockito.verify(tagDAO).create(tagList);

            Assert.assertEquals(idList.size(), actualIdList.size());
        }catch (DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGetTagsByNews() throws ServiceException{
        List<Tag> tagList = new ArrayList<Tag>();
        tagList.add(new Tag(1L, "tag text"));
        tagList.add(new Tag(2L, "tag text"));
        try{
            Mockito.when(tagDAO.getTagsByNews(Mockito.anyLong())).thenReturn(tagList);
            List<Tag> actualList = tagService.getTagsByNews(1L);
            Mockito.verify(tagDAO).getTagsByNews(1L);
            Assert.assertEquals(tagList.size(), actualList.size());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }
}
