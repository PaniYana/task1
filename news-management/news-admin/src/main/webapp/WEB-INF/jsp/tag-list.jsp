
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div id="content">

    <div class="container modal-content jumbotron col-md-offset-3 col-md-9">

        <spring:url value="/tag/update" var="update"/>
        <spring:url value="/tag/delete" var="delete"/>

        <c:if test="${tagList != null}">

            <c:forEach var="tag" items="${tagList}">
        <div class="row">
            <form:form method="post" modelAttribute="tagForm" action="${update}">
                <form:hidden path="id" value="${tag.id}"/>
                <spring:bind path="name">
                <div class="list-row">
                    <label class="tag-label col-lg-2 control-label" for="tag-name-${tag.id}">Tag</label>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <form:input path="name" class="form-control"
                                    id="tag-name-${tag.id}" type="text" value="${tag.name}" disabled="true"/>
                    </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 button-block">
                    <a id="edit-button-${tag.id}" data-tag-id="${tag.id}" class="btn btn-info btn-xs edit-button">Edit</a>
                    <button type="submit" id="update-button-${tag.id}" class="hidden-button btn btn-info btn-xs button-in-list">Update</button>
                </spring:bind>
            </form:form>
                <form:form method="post" modelAttribute="tagToDelete" action="${delete}">
                    <form:hidden path="id" value="${tag.id}"/>
                    <button type="submit" id="delete-button-${tag.id}" class="hidden-button btn btn-info btn-xs button-in-list">Delete</button>
                </form:form>
                <a id="cancel-button-${tag.id}" data-tag-id="${tag.id}" class="hidden-button btn btn-info btn-xs cancel-button">Cancel</a>
                </div>
            </div>
        </div>
            </c:forEach>

        </c:if>

        <spring:url value="/tag/add" var="add" />

        <form:form class="add-form" method="post" modelAttribute="tagForm" action="${add}">
            <spring:bind path="name">
                <div class="form-group">
                    <label for="name" class="col-lg-2 control-label">Add Tag</label>
                    <div class="col-lg-8">
                        <form:input path="name" class="form-control" id="name"/>
                        <form:errors path="name" class="control-label" />
                    </div>
                </div>
            </spring:bind>
            <button type="submit" class="btn btn-success btn-xs">Save</button>
        </form:form>
    </div>
</div>