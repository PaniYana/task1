/*
package com.epam.newsmanagement.domain;

import java.io.Serializable;

*/
/**
 * Created by Yana_Valchok on 7/12/2016.
 *//*

public abstract class Entity implements Serializable{

    private static final long serialVersionUID = 1L;
    private Long id;
    private long version;

    public Entity(long version, Long id) {
        this.version = version;
        this.id = id;
    }

    public Entity() {
    }

    public Entity(Long id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entity)) return false;

        Entity entity = (Entity) o;

        if (getVersion() != entity.getVersion()) return false;
        return getId().equals(entity.getId());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + (int) (getVersion() ^ (getVersion() >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "id=" + id +
                ", version=" + version +
                '}';
    }
}
*/
