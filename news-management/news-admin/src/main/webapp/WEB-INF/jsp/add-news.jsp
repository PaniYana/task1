<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="content">

    <div id="add-news-form" class="container jumbotron col-md-offset-3 col-md-9">

        <spring:url value="/news/add" var="addOrEditNews"/>
        <c:if test="${newsForm.news.id != null}">
            <spring:url value="/news/edit" var="addOrEditNews"/>
        </c:if>

        <c:if test="${message != null}">
            <p>${message}</p>
        </c:if>

        <form:form class="form-horizontal " method="post" modelAttribute="newsForm" action="${addOrEditNews}">

            <c:if test="${newsForm.news.id != null}">
                <form:hidden path="news.id" />
                <form:hidden path="news.creationDate" />
                <form:hidden path="news.version"/>
            </c:if>

            <fieldset class="form-group">
                <spring:bind path="news.title">
                    <div class="form-group  ${status.error ? 'has-error' : ''}">
                        <label for="title" class="col-lg-2 control-label">Title</label>
                        <div class="col-lg-10">
                                <form:input path="news.title" type="text" class="form-control" id="title" placeholder="Title"/>
                                <form:errors path="news.title" class="control-label" />

                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="news.shortText">
                    <div class="form-group  ${status.error ? 'has-error' : ''}">
                        <label for="shortText" class="col-lg-2 control-label">Brief</label>
                        <div class="col-lg-10">
                            <form:textarea path="news.shortText" class="form-control" rows="3" id="shortText"/>
                            <form:errors path="news.shortText" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="news.fullText">
                    <div class="form-group  ${status.error ? 'has-error' : ''}">
                        <label for="fullText" class="col-lg-2 control-label">Content</label>
                        <div class="col-lg-10">
                            <form:textarea path="news.fullText" class="form-control" rows="5" id="fullText"/>
                            <form:errors path="news.fullText" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="authorList">
                    <div class="form-group  ${status.error ? 'has-error' : ''}">
                        <label for="authorList" class="col-lg-2 control-label">Authors</label>
                        <div class="col-lg-10">
                            <form:select path="authorList" class="form-control" multiple="multiple" id="authorList">
                                <c:forEach items="${authorList}" var="author">
                                    <form:option value="${author.id}">${author.name}</form:option>
                                </c:forEach>
                            </form:select>
                            <form:errors path="authorList" class="control-label" />
                        </div>
                    </div>
                </spring:bind>
                <spring:bind path="tagList">
                    <div class="form-group  ${status.error ? 'has-error' : ''}">
                        <label for="tagList" class="col-lg-2 control-label">Tags</label>
                        <div class="col-lg-10">
                            <form:select path="tagList"
                                         multiple="multiple" class="form-control" id="tagList">
                                <c:forEach items="${tagList}" var="tag">
                                    <form:option value="${tag.id}">${tag.name}</form:option>
                                </c:forEach>
                            </form:select>
                            <form:errors path="tagList" class="control-label" />
                        </div>
                    </div>
                </spring:bind>
                <div class="form-group  ${status.error ? 'has-error' : ''}">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </fieldset>
        </form:form>

    </div>

</div>