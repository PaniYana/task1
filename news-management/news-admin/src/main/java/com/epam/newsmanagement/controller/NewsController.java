package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.domain.to.NewsAddTO;
import com.epam.newsmanagement.domain.to.NewsDeleteTO;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.*;
import com.epam.newsmanagement.validation.NewsFormValidator;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 5/6/2016.
 */

@Controller
@RequestMapping("/news")
public class NewsController {

    private static final String ADD_PAGE = "add-news";
    private static final String LIST_PAGE = "news-list";
    private static final String LIST = "list";
    private static final String REDIRECT = "redirect:/news";
    private static final String SHOW_PAGE = "show";
    private static final String FILTER = "?filter";
    private static final String NEWS_FORM = "newsForm";
    private static final String AUTHOR_LIST = "authorList";
    private static final String TAG_LIST = "tagList";
    private static final String NEWS_TO_DELETE = "newsToDelete";
    private static final String NEWS_LIST = "newsList";
    private static final String COMMENT_FORM = "commentForm";
    private static final String COMMENT_TO_DELETE = "commentToDelete";
    private static final String NEWS_COUNT = "newsCount";
    private static final String NEWS = "news";
    private static final String SEARCH_CRITERIA = "searchCriteria";
    private static final String OFFSET = "offset";
    private static final String PAGE= "page";
    private static final String DEFAULT_PAGE= "1";

    private static final String LIST_URL = "/list";
    private static final String ADD_URL = "/add";
    private static final String SHOW_URL = "/{id}/show";
    private static final String ID = "id";
    private static final String EDIT_ID_URL = "/{id}/edit";
    private static final String EDIT_URL = "/edit";
    private static final String DELETE_URL = "/delete";
    private static final String FILTER_URL = "/filter";
    private static final String PREV_URL = "/{id}/prev";
    private static final String NEXT_URL = "/{id}/next";
    private static final String MESSAGE = "Current news is updating. Try later.";
    private static final String MESSAGE_NAME = "message";

    @Autowired
    private NewsManagement newsManagement;

    @Autowired
    private NewsService newsService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @Autowired
    private NewsFormValidator newsFormValidator;

    @Autowired
    private CommentService commentService;

    private static final int DEFAULT_OFFSET = 1;
    private static final int COUNT = 3;

    @InitBinder(NEWS_FORM)
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(newsFormValidator);
    }


    @RequestMapping(value = LIST_URL, method = RequestMethod.GET)
    public ModelAndView getAllNews (@RequestParam(name = PAGE, required = false, defaultValue = DEFAULT_PAGE)Integer page) throws ServiceException{

        ModelAndView modelAndView = new ModelAndView(LIST_PAGE);
        List<NewsTO> newsList = newsManagement.findAllPaged(page, COUNT);
        NewsDeleteTO newsDeleteTO = new NewsDeleteTO();
        int newsCount = newsService.getNewsCount();
        modelAndView.addObject(NEWS_LIST , newsList);
        modelAndView.addObject(OFFSET, DEFAULT_OFFSET);
        modelAndView.addObject(NEWS_TO_DELETE, newsDeleteTO);
        List<Author> authorList = authorService.getAll();
        List<Tag> tagList = tagService.getAll();
        SearchCriteria searchCriteria = new SearchCriteria();
        modelAndView.addObject(SEARCH_CRITERIA, searchCriteria);
        modelAndView.addObject(AUTHOR_LIST, authorList);
        modelAndView.addObject(TAG_LIST, tagList);
        modelAndView.addObject(NEWS_COUNT, newsCount);
        return modelAndView;
    }


    @RequestMapping(value = ADD_URL, method = RequestMethod.GET)
    public ModelAndView getAddPage() throws ServiceException{

        ModelAndView modelAndView = new ModelAndView(ADD_PAGE);
        NewsAddTO news = new NewsAddTO();
        news.setNews(new News());
        news.setAuthorList(new ArrayList<Long>());
        news.setTagList(new ArrayList<Long>());
        news.setCommentList(new ArrayList<Long>());
        List<Author> authorList = authorService.getAll();
        List<Tag> tagList = tagService.getAll();
        modelAndView.addObject(NEWS_FORM, news);
        modelAndView.addObject(AUTHOR_LIST, authorList);
        modelAndView.addObject(TAG_LIST, tagList);

        return modelAndView;
    }

    @RequestMapping(value = ADD_URL, method = RequestMethod.POST)
    public ModelAndView saveNews(@ModelAttribute(NEWS_FORM) @Validated NewsAddTO newsForm,
                                 BindingResult result) throws ServiceException{

        Long id = 1L;
        if(!result.hasErrors()){
            newsForm.setCommentList(new ArrayList<Long>());
            id = newsManagement.add(newsForm.getNews(), newsForm.getAuthorList(), newsForm.getTagList());
        }else{
            ModelAndView modelAndView = new ModelAndView(ADD_PAGE);
            modelAndView.addObject(NEWS_FORM, newsForm);
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView(REDIRECT + "/" + id + "/" + SHOW_PAGE);

        return modelAndView;
    }

    @RequestMapping(value = SHOW_URL, method = RequestMethod.GET)
    public ModelAndView showNews(@PathVariable(ID) Long id ) throws ServiceException{
        ModelAndView modelAndView = new ModelAndView(SHOW_PAGE);
        NewsTO news = newsManagement.find(id);
        List<Comment> commentList = commentService.findByNewsId(id);
        news.setCommentList(commentList);
        Comment comment = new Comment();
        modelAndView.addObject(NEWS, news);
        modelAndView.addObject(COMMENT_FORM, comment);
        modelAndView.addObject(COMMENT_TO_DELETE, comment);
        return modelAndView;
    }

    @RequestMapping(value = EDIT_ID_URL, method = RequestMethod.GET)
    public ModelAndView getEditPage(@ModelAttribute("newsForm") NewsAddTO newsForm,RedirectAttributes redirectAttributes,
                                    @PathVariable(ID) Long id , HttpServletRequest request) throws ServiceException{
        ModelAndView modelAndView = new ModelAndView(ADD_PAGE);
        /*NewsAddTO newsForm = (NewsAddTO)request.getAttribute(NEWS_FORM);*/
        NewsAddTO news = newsManagement.findForUpdate(id);
        /*NewsAddTO newsForm = (NewsAddTO)redirectAttributes.getFlashAttributes().get(NEWS_FORM);*/
        if(newsForm == null || newsForm.getNews() == null) {
            modelAndView.addObject(NEWS_FORM, news);
        }/*else if(news.getNews().getVersion().equals(newsForm.getNews().getVersion())){
            modelAndView.addObject(NEWS_FORM, news);
        }*/else{
            modelAndView.addObject(NEWS_FORM, newsForm);
        }
        List<Author> authorList = authorService.getAll();
        List<Tag> tagList = tagService.getAll();
        modelAndView.addObject(AUTHOR_LIST , authorList);
        modelAndView.addObject(TAG_LIST, tagList);
        return modelAndView;
    }

    @RequestMapping(value = EDIT_URL, method = RequestMethod.POST)
    public ModelAndView editNews(@ModelAttribute(NEWS_FORM) @Validated NewsAddTO newsForm,
                                 BindingResult result,  RedirectAttributes redirectAttributes) throws ServiceException{
        ModelAndView modelAndView;
        try {
            if (!result.hasErrors()) {
                newsManagement.update(newsForm);
                modelAndView = new ModelAndView(REDIRECT + "/" + newsForm.getNews().getId() + "/" + SHOW_PAGE);
            } else {
                modelAndView = new ModelAndView(ADD_PAGE);
                modelAndView.addObject(NEWS_FORM, newsForm);
                return modelAndView;
            }
            return modelAndView;
        }catch(StaleObjectStateException e){
           /* modelAndView = new ModelAndView(new RedirectView(REDIRECT + "/" + newsForm.getNews().getId() + EDIT_URL));*/
            modelAndView = new ModelAndView(REDIRECT + "/" + newsForm.getNews().getId() + EDIT_URL);
            newsForm.getNews().setVersion(newsForm.getNews().getVersion() + 1);
            redirectAttributes.addFlashAttribute(NEWS_FORM, newsForm);
            /*redirectAttributes.addAttribute("string", "string");*/
            /*modelAndView = new ModelAndView(ADD_PAGE);
            modelAndView.addObject(NEWS_FORM, newsForm);*/
            redirectAttributes.addFlashAttribute(MESSAGE_NAME, MESSAGE);
            return modelAndView;
        }
    }

    @RequestMapping(value = DELETE_URL, method = RequestMethod.POST)
    public ModelAndView deleteNews(@ModelAttribute(NEWS_TO_DELETE) @Validated NewsDeleteTO news,
                                 BindingResult result) throws ServiceException{
        ModelAndView modelAndView = new ModelAndView(REDIRECT + "/" + LIST);
        if(!result.hasErrors()){
            newsManagement.delete(news.getIdList());
        }else{
            modelAndView = new ModelAndView(ADD_PAGE);
            modelAndView.addObject(NEWS_TO_DELETE , news);
            return modelAndView;
        }

        return modelAndView;
    }

    @RequestMapping(value = FILTER_URL, method = RequestMethod.GET)
    public ModelAndView filterNews(@ModelAttribute(SEARCH_CRITERIA) SearchCriteria searchCriteria,
                                   BindingResult result, 
                                   @RequestParam(name = PAGE, required = false, defaultValue = DEFAULT_PAGE)Integer page) throws ServiceException{
        ModelAndView modelAndView = new ModelAndView(LIST_PAGE);
        if(!result.hasErrors()){
            List<NewsTO> newsList = newsManagement.find(searchCriteria.getAuthorList(), searchCriteria.getTagList(), page, COUNT);
            modelAndView.addObject(NEWS_LIST, newsList);
        }
        List<Author> authorList = authorService.getAll();
        List<Tag> tagList = tagService.getAll();
        modelAndView.addObject(AUTHOR_LIST, authorList);
        modelAndView.addObject(TAG_LIST, tagList);
        modelAndView.addObject(SEARCH_CRITERIA, searchCriteria);

        NewsDeleteTO newsDeleteTO = new NewsDeleteTO();
        int newsCount = newsManagement.find(searchCriteria.getAuthorList(), searchCriteria.getTagList()).size();
        modelAndView.addObject(OFFSET, DEFAULT_OFFSET);
        modelAndView.addObject(NEWS_TO_DELETE, newsDeleteTO);
        modelAndView.addObject(NEWS_COUNT , newsCount);
        return modelAndView;
    }

    @RequestMapping(value = PREV_URL, method = RequestMethod.GET)
    public ModelAndView showPrev(@PathVariable(ID) Long id ) throws ServiceException{
        ModelAndView modelAndView = new ModelAndView(SHOW_PAGE);
        NewsTO news = newsManagement.getPrevious(id);
        Comment comment = new Comment();
        modelAndView.addObject(NEWS, news);
        modelAndView.addObject(COMMENT_FORM , comment);
        modelAndView.addObject(COMMENT_TO_DELETE, comment);
        return modelAndView;
    }

    @RequestMapping(value = NEXT_URL, method = RequestMethod.GET)
    public ModelAndView showNext(@PathVariable(ID) Long id ) throws ServiceException{
        ModelAndView modelAndView = new ModelAndView(SHOW_PAGE);
        NewsTO news = newsManagement.getNext(id);
        Comment comment = new Comment();
        modelAndView.addObject(NEWS, news);
        modelAndView.addObject(COMMENT_FORM , comment);
        modelAndView.addObject(COMMENT_TO_DELETE, comment);
        return modelAndView;
    }
}
