<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div id="content" class="wrapper">


    <div class="container jumbotron col-md-offset-3 col-md-9">

        <spring:url value="/news/filter" var="filter"/>

            <form:form class="form-horizontal" method="get" modelAttribute="searchCriteria" action="${filter}">
                <div class="news-row search-bar">
                    <spring:bind path="authorList">
                        <div class="author-select-filter form-group">
                            <div class="col-lg-10">
                                <form:select path="authorList" size="5" class="form-control" multiple="multiple" id="authorList">

                                    <c:forEach items="${authorList}" var="author">
                                        <form:option value="${author.id}">${author.name}</form:option>
                                    </c:forEach>

                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="tagList">
                        <div class="tag-select-filter form-group">
                            <div class="col-lg-10">
                                <form:select path="tagList" size="5"
                                             multiple="multiple" class="form-control" id="tagList">

                                    <c:forEach items="${tagList}" var="tag">
                                        <form:option value="${tag.id}">${tag.name}</form:option>
                                    </c:forEach>

                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <div class="form-group">
                        <div class="btn-wrapper buttons-filter col-lg-10 col-lg-offset-2">
                            <button type="submit" id="btn-filter" class="btn btn-primary btn-sm">Filter</button>
                            <a href="/news/list" class="btn btn-default btn-sm">Reset</a>
                        </div>
                    </div>
                </div>
            </form:form>
        <c:if test="${newsList != null}">

            <c:if test="${not empty searchCriteria.authorList or not empty searchCriteria.tagList}">
                <input type="hidden" id="searchCriteriaValue" value="1"/>
            </c:if>
            <input type="hidden" id="searchCriteriaValue" value="0"/>
            <spring:url value="/news/delete" var="delete"/>

            <form:form class="form-horizontal" method="post" multiple="multiple" modelAttribute="newsToDelete" action="${delete}">

            <c:forEach var="news" items="${newsList}">

                <div class="news-record row">

                    <div class="list-news-title">
                        <a href="/news/${news.news.id}/show"><c:out value="${news.news.title}"/></a>
                    </div>
                    <c:if test="${news.authorList != null}">
                        <div class="author-labels">
                            <c:forEach var="author" items="${news.authorList}">
                                <span class="label label-primary">${author.name}</span>
                            </c:forEach>
                        </div>
                    </c:if>
                    <p class="news-short-text"><c:out value="${news.news.shortText}"/></p>

                    <div class="news-creation-date">
                        <span class="creation-date"><c:out value="${news.news.creationDate}"/></span>
                    </div>
                    <c:if test="${news.tagList != null}">
                        <div class="tag-labels">
                            <c:forEach var="tag" items="${news.tagList}">
                                <span class="label label-default">${tag.name}</span>
                            </c:forEach>
                        </div>
                    </c:if>
                    <div class="btn-edit">
                        <a href="/news/${news.news.id}/edit" class="btn btn-warning btn-xs">Edit</a>
                    </div>

                    <div class="checkbox">
                        <label>
                            <form:checkbox path="idList" class="checkbox" value="${news.news.id}"/>
                        </label>
                    </div>
                </div>
            </c:forEach>

            <button type="submit" class="btn btn-primary">Delete</button>

            </form:form>
        </c:if>

        <div class="page col-md-offset-3">
            <input id="newsCount" type="hidden" value="${newsCount}"/>
            <input id="offset" type="hidden" value="${offset}"/>
            <ul id="pagination-demo" class="pagination">
                <li class="first"><a href="">&laquo;</a></li>
                <li class="prev"></li>
                <li class="next"><a href="">&gt;</a></li>
                <li class="last"><a href="">&raquo;</a></li>
            </ul>
        </div>
    </div>
    <spring:url value="/resources/js/pagination.js" var="pagination"/>
    <spring:url value="/resources/js/jquery.twbsPagination.min.js" var="twbsPagination"/>

    <script type="text/javascript" src="${pagination}"></script>
    <script type="text/javascript" src="${twbsPagination}"></script>
</div>