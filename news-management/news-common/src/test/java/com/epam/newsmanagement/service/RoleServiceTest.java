package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.Role;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.RoleServiceImpl;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/20/2016.
 */
public class RoleServiceTest {

    @InjectMocks
    private RoleService roleService = new RoleServiceImpl();

    @Mock
    private RoleDAO roleDAO;

    @Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAdd() throws ServiceException {
        try {
            Role role = new Role(1L, "Role name");
            Mockito.when(roleDAO.create(role)).thenReturn(role);

            Long createdId = roleService.add(role);
            Assert.assertEquals(1L, (long)createdId);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGetAll() throws ServiceException {
        try{
            List<Role> roleList = new ArrayList<Role>(2);
            roleList.add(new Role(1L,"Role name"));
            roleList.add(new Role(2L, "Role name"));
            Mockito.when(roleDAO.getAll()).thenReturn(roleList);
            List<Role> actualRoleList = roleDAO.getAll();
            Mockito.verify(roleDAO).getAll();
            Assert.assertEquals(roleList.size(), actualRoleList.size());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGet() throws ServiceException{
        try{
            Role role = new Role(1L,"Role name");
            Mockito.when(roleDAO.find(1L)).thenReturn(role);
            Role actualRole = roleService.get(1L);
            Mockito.verify(roleDAO).find(1L);
            Assert.assertEquals(role.getId(), actualRole.getId());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }
}
