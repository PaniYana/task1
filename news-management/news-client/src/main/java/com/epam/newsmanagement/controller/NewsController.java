package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Yana_Valchok on 6/10/2016.
 */
public class NewsController extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(NewsController.class);
    private static final String COMMAND = "command";
    private static final String DEFAULT_PAGE = "index.jsp";
    private static final String COMMAND_PATTERN = "command=";

    private ApplicationContext context;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext servletContext = this.getServletContext();
        context =
                WebApplicationContextUtils.getRequiredWebApplicationContext(
                        this.getServletContext());

    }
    
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void moveNextPage(String page, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try{
            if (page == null) {
                RequestDispatcher dispatcher = request.getRequestDispatcher(DEFAULT_PAGE);
                dispatcher.forward(request, response);
            } else {
                if (page.contains(COMMAND_PATTERN)) {
                    response.sendRedirect(request.getRequestURI() + "?" + page);
                } else {
                    RequestDispatcher dispatcher = request.getRequestDispatcher(page);
                    dispatcher.forward(request, response);
                }

            }
        }catch(Exception exception){
            try{
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                LOGGER.error(exception);
            }catch(IOException e){
                LOGGER.error(exception.getMessage(),e.getMessage(), e);
            }

        }
    }

    private void process(HttpServletRequest request, HttpServletResponse response){
        try {
            Command command = (Command)context.getBean(request.getParameter(COMMAND));
            String page = null;
            if(command != null) {
                page = command.execute(request, response);
            }
            moveNextPage(page, request, response);
        } catch (CommandException e) {
            try{
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                LOGGER.error( e.getMessage(), e);
            }catch(IOException exception){
                LOGGER.error(exception.getMessage(), exception, e.getMessage(), e);
            }
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
    }
}
