package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/14/2016.
 */
public class CommentServiceTest {

    @InjectMocks
    private CommentService commentService =  new CommentServiceImpl();

    @Mock
    private CommentDAO commentDAO;

    @Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAdd() throws ServiceException {
        try {

            News news = new News(1L, "Title", "Short text" , "Full text" , new Timestamp(1460623612723L), new Date(1460623612723L));
            Comment comment = new Comment(1L, news,"Text of comment", new Timestamp(1460623612723L));
            Mockito.when(commentDAO.create(comment)).thenReturn(comment);

            Long id = commentService.add(comment);
            Assert.assertEquals(1L, (long)id);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testDelete() throws ServiceException {
        try {
            commentService.delete(1L);
            Mockito.verify(commentDAO).delete(1L);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGetAll() throws ServiceException{
        News news = new News(1L, "Title", "Short text" , "Full text" , new Timestamp(1460623612723L), new Date(1460623612723L));

        try{
            List<Comment> commentList = new ArrayList<Comment>(2);
            commentList.add(new Comment(1L, news,"Text of comment", new Timestamp(1460623612723L)));
            commentList.add(new Comment(2L, news,"Text of comment", new Timestamp(1460623612723L)));
            Mockito.when(commentService.getAll()).thenReturn(commentList);
            List<Comment> actualCommentList = commentDAO.getAll();
            Mockito.verify(commentDAO).getAll();
            Assert.assertEquals(commentList.size(), actualCommentList.size());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testGet() throws ServiceException{
        News news = new News(1L, "Title", "Short text" , "Full text" , new Timestamp(1460623612723L), new Date(1460623612723L));

        try{
            Comment comment = new Comment(1L, news,  "Some text", new Timestamp(1460623612723L));
            Mockito.when(commentDAO.find(1L)).thenReturn(comment);
            Comment actualComment = commentService.get(1L);
            Mockito.verify(commentDAO).find(1L);
            Assert.assertEquals(comment.getId(), actualComment.getId());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testAddSeveral() throws ServiceException{
        News news = new News(1L, "Title", "Short text" , "Full text" , new Timestamp(1460623612723L), new Date(1460623612723L));

        try {
            List<Comment> commentList = new ArrayList<Comment>();
            commentList.add(new Comment(1L, news, "Text",new Timestamp(1460623612723L) ));
            commentList.add(new Comment(2L, news, "Text",new Timestamp(1460623612723L) ));
            commentList.add(new Comment(3L, news, "Text",new Timestamp(1460623612723L) ));
            Mockito.when(commentDAO.getAll()).thenReturn(commentList);
            List<Long> idList = new ArrayList<Long>();
            idList.add(1L);
            idList.add(2L);
            idList.add(3L);

            commentService.addSeveral(commentList);
            Mockito.verify(commentDAO).create(commentList);

            List<Comment> actualIdList = commentService.getAll();
            Assert.assertEquals(idList.size(), actualIdList.size());
        }catch (DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }

    @Test
    public void testDeleteSeveral() throws ServiceException{
        try {
            List<Long> idList = new ArrayList<Long>();
            idList.add(1L);
            idList.add(2L);
            idList.add(3L);

            commentService.deleteSeveral(idList);
            Mockito.verify(commentDAO).delete(idList);
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }

    }

    @Test
    public void testFindByNewsId() throws ServiceException{
        News news = new News(1L, "Title", "Short text" , "Full text" , new Timestamp(1460623612723L), new Date(1460623612723L));

        try{
            List<Comment> commentList = new ArrayList<Comment>();
            commentList.add(new Comment(1L, news, "Text",new Timestamp(1460623612723L) ));
            commentList.add(new Comment(2L, news, "Text",new Timestamp(1460623612723L) ));

            Mockito.when(commentDAO.getByNewsId(Mockito.anyLong())).thenReturn(commentList);

            List<Comment> actualList = commentService.findByNewsId(1L);
            Mockito.verify(commentDAO).getByNewsId(1L);
            Assert.assertEquals(commentList.size(), actualList.size());
        }catch(DAOException e){
            Mockito.doThrow(ServiceException.class);
        }
    }
}
