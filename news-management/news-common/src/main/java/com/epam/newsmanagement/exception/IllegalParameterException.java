package com.epam.newsmanagement.exception;

/**
 * Created by Yana_Valchok on 4/13/2016.
 */
/**
 * Extension of <tt>ServiceException</tt>
 * Exception class that can be thrown if any service class method get wrong or illegal parameter
 * @see ServiceException
 */
public class IllegalParameterException extends ServiceException {

    public IllegalParameterException(){}

    public IllegalParameterException(String message){
        super(message);
    }

    public IllegalParameterException(DAOException exception){
        super(exception);
    }

    public IllegalParameterException(Throwable exception){
        super(exception);
    }
}
