package com.epam.newsmanagement.controller.command.impl;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Yana_Valchok on 6/14/2016.
 */
@Component
public class ShowNextCommandImpl implements Command {

    private static final String RETURN_PAGE = "/news/show";
    private static final String ID = "id";
    private static final String NEWS = "news";

    @Autowired
    private NewsManagement newsManagement;


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String idStr = request.getParameter(ID);
        Long id;
        NewsTO news = null;
        if(idStr != null){
            id = Long.parseLong(idStr);
        }else{
            id = 1L;
        }
        try{
            news = newsManagement.getNext(id);
        }catch(ServiceException e){
            throw  new CommandException(e.getMessage());
        }
        request.setAttribute(NEWS, news);
        return RETURN_PAGE;
    }
}
