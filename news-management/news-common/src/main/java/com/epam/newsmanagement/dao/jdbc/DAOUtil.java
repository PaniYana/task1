package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.exception.DAOException;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Utility class for DAO that provides closing statements and returning connections to the connection pool.
 */
final class DAOUtil {

    /**
     * Provides closing statement and returning connection to the connection pool in datasource
     * @param datasource
     * @param connection
     * @param statement
     * @throws DAOException if there were some errors or exceptions in the run-time
     */
    public static void closeConnection(DataSource datasource, Connection connection, PreparedStatement statement)
            throws DAOException{
        try {
            statement.close();
            DataSourceUtils.doReleaseConnection(connection, datasource);
        }catch(SQLException e){
            throw new DAOException("Method closeConnection in DAOUtil", e);
        }
    }

}
