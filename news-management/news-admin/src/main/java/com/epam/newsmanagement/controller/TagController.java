package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.validation.TagFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Yana_Valchok on 5/24/2016.
 */
@Controller
@RequestMapping("/tag")
public class TagController {

    private static final String LIST_PAGE = "tag-list";
    private static final String REDIRECT = "redirect:/tag";
    private static final String TAG_FORM = "tagForm";
    private static final String TAG_TO_DELETE = "tagToDelete";
    private static final String TAG_LIST = "tagList";

    private static final String DELETE_URL = "/delete";
    private static final String LIST_URL = "/list";
    private static final String UPDATE_URL = "/update";
    private static final String ADD_URL = "/add";

    @Autowired
    private TagService tagService;

    @Autowired
    private TagFormValidator tagFormValidator;

    @InitBinder(TAG_FORM)
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(tagFormValidator);
    }

    @RequestMapping(value = LIST_URL, method = RequestMethod.GET)
    public ModelAndView getAllTags () throws ServiceException {

        ModelAndView modelAndView = new ModelAndView(LIST_PAGE);
        List<Tag> tagList = tagService.getAll();
        Tag tag = new Tag();
        modelAndView.addObject(TAG_LIST, tagList);
        modelAndView.addObject(TAG_FORM, tag);
        modelAndView.addObject(TAG_TO_DELETE, tag);
        return modelAndView;
    }

    @RequestMapping(value = ADD_URL, method = RequestMethod.POST)
    public ModelAndView saveTag(@ModelAttribute(TAG_FORM) @Validated Tag tag,
                                 BindingResult result) throws ServiceException{

        ModelAndView modelAndView = new ModelAndView(REDIRECT  + LIST_URL);
        if(!result.hasErrors()){
            tagService.add(tag);
        }else{
            modelAndView.addObject(TAG_FORM, tag);
            return modelAndView;
        }
        return modelAndView;
    }

    @RequestMapping(value = UPDATE_URL, method = RequestMethod.POST)
    public ModelAndView updateTag(@ModelAttribute(TAG_FORM) @Validated Tag tag,
                                     BindingResult result) throws ServiceException{

        ModelAndView modelAndView = new ModelAndView(REDIRECT  + LIST_URL);
        if(!result.hasErrors()){
            tagService.update(tag);
        }else{
            modelAndView.addObject(TAG_FORM, tag);
            return modelAndView;
        }
        return modelAndView;
    }

    @RequestMapping(value = DELETE_URL, method = RequestMethod.POST)
    public ModelAndView deleteTag(@ModelAttribute() Tag tag,
                                     BindingResult result) throws ServiceException{

        ModelAndView modelAndView = new ModelAndView(REDIRECT  + LIST_URL);
        if(!result.hasErrors()){
            tagService.delete(tag.getId());
        }else{
            modelAndView.addObject(TAG_TO_DELETE, tag);
            return modelAndView;
        }
        return modelAndView;
    }
}
