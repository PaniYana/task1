package com.epam.newsmanagement.domain.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 5/26/2016.
 */
public class NewsDeleteTO implements Serializable {

    private List<Long> idList;

    public NewsDeleteTO(List<Long> idList) {
        this.idList = idList;
    }

    public NewsDeleteTO() {

        idList = new ArrayList<Long>();
    }

    public List<Long> getIdList() {
        return idList;
    }

    public void setIdList(List<Long> idList) {
        this.idList = idList;
    }

    @Override
    public String toString() {
        return "NewsDeleteTO{" +
                "idList=" + idList +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof NewsDeleteTO)) {
            return false;
        }

        NewsDeleteTO that = (NewsDeleteTO) obj;

        if(getIdList().equals(that.getIdList())){
            return true;
        }else{
            return false;
        }

    }

    @Override
    public int hashCode() {
        int result = getIdList().hashCode();
        return result;
    }
}
