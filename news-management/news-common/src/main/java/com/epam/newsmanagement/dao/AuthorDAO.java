package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Extension of GenericDao, parametrized by entity <tt>Author</tt> and key type <tt>Long</tt>
 *
 * Provides with more special methods for working with this entity in database.
 * Contains more methods for finding entities.
 * @see GenericDAO
 */
public interface AuthorDAO extends GenericDAO<Author, Long>{

    /**
     * Finds and returns list of authors that wasn't marked as expired.
     * @return list of authors
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    List<Author> getNotExpiredList() throws DAOException;

    /**
     * Finds and returns list of authors which were mentioned as authors of certain news message.
     * @param idNews unique key of news message
     * @return list of authors
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    List<Author> getAuthorsByNews(Long idNews) throws DAOException;
}
