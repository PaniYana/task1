package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Implementation of interface <tt>NewsDAO</tt>
 * Adapted to the work with Oracle database.
 * @see NewsDAO
 */
@Repository
@Profile("jdbc")
public class NewsDAOImpl implements NewsDAO {

    private static final String NE_NEWS_ID = "NE_NEWS_ID";
    private static final String NE_TITLE = "NE_TITLE";
    private static final String NE_SHORT_TEXT = "NE_SHORT_TEXT";
    private static final String NE_FULL_TEXT = "NE_FULL_TEXT";
    private static final String NE_CREATION_DATE = "NE_CREATION_DATE";
    private static final String NE_MODIFICATION_DATE = "NE_MODIFICATION_DATE";

    private static final String SQL_GET_BY_ID =
            "SELECT NE_NEWS_ID, NE_TITLE, NE_SHORT_TEXT, NE_FULL_TEXT, NE_CREATION_DATE, NE_MODIFICATION_DATE FROM NEWS WHERE NE_NEWS_ID = ?";
    private static final String SQL_GET_ALL =
            "SELECT NEWS.NE_NEWS_ID,  NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, NEWS.NE_CREATION_DATE," +
                    " NEWS.NE_MODIFICATION_DATE, COUNT(COMMENTS.COM_COMMENT_ID) COMMENTS_COUNT FROM NEWS LEFT JOIN " +
                    "COMMENTS ON NEWS.NE_NEWS_ID = COMMENTS.COM_NEWS_ID GROUP BY COMMENTS.COM_NEWS_ID, NEWS.NE_NEWS_ID," +
                    " NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, \n" +
                    "NEWS.NE_CREATION_DATE, NEWS.NE_MODIFICATION_DATE ORDER BY COMMENTS_COUNT DESC, NEWS.NE_MODIFICATION_DATE DESC";
    private static final String SQL_CREATE = "INSERT INTO NEWS (NE_NEWS_ID, NE_TITLE, NE_SHORT_TEXT, NE_FULL_TEXT, NE_CREATION_DATE, NE_MODIFICATION_DATE) " +
            "VALUES (NEWS_SEQ.nextval, ? ,? , ? , ?, ?)";
    private static final String SQL_UPDATE =
            "UPDATE NEWS SET NE_TITLE = ?, NE_SHORT_TEXT = ?, NE_FULL_TEXT = ?, NE_CREATION_DATE = ?, NE_MODIFICATION_DATE = ?" +
                    "WHERE NE_NEWS_ID = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM NEWS WHERE NE_NEWS_ID = ?";
    private static final String SQL_ATTACH_AUTHOR_TO_NEWS = "INSERT INTO NEWS_AUTHORS (NA_NEWS_ID,NA_AUTHOR_ID) VALUES (?, ?)";
    private static final String SQL_ATTACH_TAG_TO_NEWS = "INSERT INTO NEWS_TAGS (NT_NEWS_ID, NT_TAG_ID) VALUES (?, ?)";
    private static final String SQL_DETACH_AUTHOR_FROM_NEWS =
            "DELETE FROM NEWS_AUTHORS WHERE NA_NEWS_ID = ? AND NA_AUTHOR_ID = ?";
    private static final String SQL_DETACH_TAG_FROM_NEWS =
            "DELETE FROM NEWS_TAGS WHERE NT_NEWS_ID = ? AND NT_TAG_ID = ?";
    private static final String SEARCH_NEWS_SQL = "SELECT DISTINCT NEWS.NE_NEWS_ID,  NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT," +
            " NEWS.NE_CREATION_DATE, NEWS.NE_MODIFICATION_DATE FROM " +
            "NEWS JOIN NEWS_AUTHORS ON (NEWS.NE_NEWS_ID = NEWS_AUTHORS.NA_NEWS_ID) " +
            "JOIN NEWS_TAGS ON (NEWS.NE_NEWS_ID = NEWS_TAGS.NT_NEWS_ID) " +
            "LEFT JOIN COMMENTS ON (NEWS.NE_NEWS_ID = COMMENTS.COM_NEWS_ID)";
    private static final String SQL_COUNT_NEWS = "SELECT DISTINCT COUNT(NE_NEWS_ID) NEWS_COUNT FROM NEWS";
    private static final String SQL_GET_NEWS_BY_AUTHOR =
            "SELECT NEWS.NE_NEWS_ID, NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, NEWS.NE_CREATION_DATE, " +
                    "NEWS.NE_MODIFICATION_DATE FROM NEWS JOIN NEWS_AUTHORS ON (NEWS.NE_NEWS_ID = NEWS_AUTHORS.NA_NEWS_ID)" +
                    "WHERE NA_AUTHOR_ID = ?";
    private static final String SQL_GET_ALL_PAGED =
            "SELECT  NE_NEWS_ID, NE_TITLE, NE_SHORT_TEXT, NE_FULL_TEXT, NE_CREATION_DATE, NE_MODIFICATION_DATE, COMMENTS_COUNT FROM " +
                    "(" +
                    "SELECT a.NE_NEWS_ID,a.NE_TITLE,a.NE_SHORT_TEXT,a.NE_FULL_TEXT,a.NE_CREATION_DATE,a.NE_MODIFICATION_DATE,a.COMMENTS_COUNT, rownum rw FROM" +
                    "(" +
                    "SELECT NEWS.NE_NEWS_ID,  NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, NEWS.NE_CREATION_DATE," +
                    "NEWS.NE_MODIFICATION_DATE, COUNT(COMMENTS.COM_COMMENT_ID) COMMENTS_COUNT FROM NEWS LEFT JOIN COMMENTS ON NEWS.NE_NEWS_ID = COMMENTS.COM_NEWS_ID " +
                    "GROUP BY COMMENTS.COM_NEWS_ID, NEWS.NE_NEWS_ID,NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT," +
                    "NEWS.NE_CREATION_DATE, NEWS.NE_MODIFICATION_DATE ORDER BY COMMENTS_COUNT DESC, NEWS.NE_MODIFICATION_DATE DESC" +
                    ") a "+
                    "WHERE rownum < ((? * ?) + 1 )"+
                    ") "+
                    "WHERE rw >= (((?-1) * ?) + 1)";
    private static final String SQL_GET_PREV = "SELECT PREV_NEWS from (SELECT NE_NEWS_ID,LAG(NE_NEWS_ID,1) OVER(ORDER BY COMMENTS_COUNT DESC, NE_MODIFICATION_DATE DESC) AS PREV_NEWS " +
            "FROM (SELECT NEWS.NE_NEWS_ID,  NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, NEWS.NE_CREATION_DATE, " +
            "NEWS.NE_MODIFICATION_DATE, COUNT(COMMENTS.COM_COMMENT_ID) COMMENTS_COUNT FROM NEWS LEFT JOIN COMMENTS ON NEWS.NE_NEWS_ID = COMMENTS.COM_NEWS_ID  GROUP BY COMMENTS.COM_NEWS_ID, NEWS.NE_NEWS_ID, " +
            "NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, " +
            "NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, " +
            "NEWS.NE_CREATION_DATE, NEWS.NE_MODIFICATION_DATE " +
            "ORDER BY COMMENTS_COUNT DESC, NEWS.NE_MODIFICATION_DATE DESC)) WHERE NE_NEWS_ID = ?";
   private static final String SQL_GET_NEXT = "SELECT NEXT_NEWS from (SELECT NE_NEWS_ID,LEAD(NE_NEWS_ID,1) OVER(ORDER BY COMMENTS_COUNT DESC, NE_MODIFICATION_DATE DESC) AS NEXT_NEWS " +
           "FROM (SELECT NEWS.NE_NEWS_ID,  NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, NEWS.NE_CREATION_DATE, " +
           "NEWS.NE_MODIFICATION_DATE, COUNT(COMMENTS.COM_COMMENT_ID) COMMENTS_COUNT FROM NEWS LEFT JOIN COMMENTS ON NEWS.NE_NEWS_ID = COMMENTS.COM_NEWS_ID  GROUP BY COMMENTS.COM_NEWS_ID, NEWS.NE_NEWS_ID, " +
           "NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, " +
           "NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT, " +
           "NEWS.NE_CREATION_DATE, NEWS.NE_MODIFICATION_DATE " +
           "ORDER BY COMMENTS_COUNT DESC, NEWS.NE_MODIFICATION_DATE DESC)) WHERE NE_NEWS_ID = ?";
    private static final String PREV_NEWS = "PREV_NEWS";
    private static final String NEXT_NEWS = "NEXT_NEWS";
    private static final String SQL_SEARCH_NEWS_PAGED = "SELECT  NE_NEWS_ID, NE_TITLE, NE_SHORT_TEXT, NE_FULL_TEXT, NE_CREATION_DATE, NE_MODIFICATION_DATE FROM(" +
            "SELECT a.NE_NEWS_ID,a.NE_TITLE,a.NE_SHORT_TEXT,a.NE_FULL_TEXT,a.NE_CREATION_DATE,a.NE_MODIFICATION_DATE, rownum rw FROM(" +
            "SELECT DISTINCT NEWS.NE_NEWS_ID,  NEWS.NE_TITLE, NEWS.NE_SHORT_TEXT, NEWS.NE_FULL_TEXT,NEWS.NE_CREATION_DATE, NEWS.NE_MODIFICATION_DATE FROM NEWS JOIN NEWS_AUTHORS ON " +
            "(NEWS.NE_NEWS_ID = NEWS_AUTHORS.NA_NEWS_ID) JOIN NEWS_TAGS ON (NEWS.NE_NEWS_ID = NEWS_TAGS.NT_NEWS_ID) LEFT JOIN COMMENTS ON (NEWS.NE_NEWS_ID = COMMENTS.COM_NEWS_ID)";
    private static final String SQL_WHERE_CLAUSE_PAGED = ") a WHERE rownum < ((? * ?) + 1 )) WHERE rw >= (((?-1)* ?) + 1)";


    @Autowired
    private DataSource datasource;

    /**
     * Implementation of method {@link NewsDAO#find(Object)}
     * @see NewsDAO
     */
    public News find(Long id) throws DAOException{

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_BY_ID);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();

            News news = null;
            if(resultSet.next()){
                news = buildEntity(resultSet);
            }
            return news;
        }catch(SQLException e){
            throw new DAOException("Method find in NewsDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#getAll()}
     * @see NewsDAO
     */
    public List<News> getAll() throws DAOException{

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<News> newsList;

        try{
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_ALL);
            resultSet = statement.executeQuery();

            newsList = new ArrayList<News>();
            News news;
            while(resultSet.next()){
                news = buildEntity(resultSet);
                newsList.add(news);
            }
            return newsList;
        }catch(SQLException e){
            throw new DAOException("Method getAll in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#create(Object)}
     * @see NewsDAO
     */
    public News create(News news) throws DAOException{

        final String[] keys = {"NE_NEWS_ID"};

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_CREATE, keys);

            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setTimestamp(4, news.getCreationDate());
            statement.setDate(5, news.getModificationDate());

            statement.executeUpdate();
            Long id = -1L;
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            return find(id);
        }catch(SQLException e){
            throw new DAOException("Method create in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#update(Object)}
     * @see NewsDAO
     */
    public News update(News news) throws DAOException{

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setTimestamp(4, news.getCreationDate());
            statement.setDate(5, news.getModificationDate());
            statement.setLong(6, news.getId());

            statement.executeUpdate();

            return news;

        }catch(SQLException e){
            throw new DAOException("Method update in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#delete(Object)}
     * @see NewsDAO
     */
    public void delete(Long id) throws DAOException{

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_DELETE_BY_ID);
            statement.setLong(1, id);

            statement.executeUpdate();

        }catch(SQLException e){
            throw new DAOException("Method delete in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#attachAuthorToNews(Long, Long)} )}
     * @see NewsDAO
     */
    public void attachAuthorToNews(Long idNews, Long idAuthor) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_ATTACH_AUTHOR_TO_NEWS);
            statement.setLong(1, idNews);
            statement.setLong(2, idAuthor);

            statement.executeUpdate();

        }catch(SQLException e){
            throw new DAOException("Method attachAuthorToNews in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#attachTagToNews(Long, Long)}
     * @see NewsDAO
     */
    public void attachTagToNews(Long idNews, Long idTag) throws DAOException{

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_ATTACH_TAG_TO_NEWS);
            statement.setLong(1, idNews);
            statement.setLong(2, idTag);

            statement.executeUpdate();

        }catch(SQLException e){
            throw new DAOException("Method attachTagToNews in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#detachAuthorFromNews(Long, Long)}
     * @see NewsDAO
     */
    public void detachAuthorFromNews(Long idNews, Long idAuthor) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_DETACH_AUTHOR_FROM_NEWS);
            statement.setLong(1, idNews);
            statement.setLong(2, idAuthor);

            statement.executeUpdate();

        }catch(SQLException e){
            throw new DAOException("Method detachAuthorFromNews in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#detachTagFromNews(Long, Long)}
     * @see NewsDAO
     */
    public void detachTagFromNews(Long idNews, Long idTag) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_DETACH_TAG_FROM_NEWS);
            statement.setLong(1, idNews);
            statement.setLong(2, idTag);

            statement.executeUpdate();

        }catch(SQLException e){
            throw new DAOException("Method detachTagFromNews in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#find(SearchCriteria)}
     * @see NewsDAO
     */
    public List<News> find(SearchCriteria searchCriteria) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        String fullQuery = buildQuery(SEARCH_NEWS_SQL, searchCriteria);

        try{
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(fullQuery);
            resultSet = statement.executeQuery();

            List<News> newsList = new ArrayList<News>();
            News news;
            while(resultSet.next()){
                news = buildEntity(resultSet);
                newsList.add(news);
            }
            return newsList;
        }catch(SQLException e){
            throw new DAOException("Method find in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }

    }

    private String buildQuery(String query, SearchCriteria searchCriteria){

        StringBuilder fullQuery = new StringBuilder(query);

        if(!searchCriteria.isEmpty()){

            fullQuery.append(" WHERE ");
            if(!searchCriteria.getAuthorList().isEmpty()){
                Iterator<Long> authorIterator = searchCriteria.getAuthorList().iterator();
                fullQuery.append("NA_AUTHOR_ID IN (");
                while(authorIterator.hasNext()){
                    fullQuery.append(authorIterator.next());
                    if(authorIterator.hasNext()){
                        fullQuery.append(", ");
                    }else{
                        fullQuery.append(")");
                    }
                }
                if(!searchCriteria.getTagList().isEmpty()){
                    fullQuery.append(" AND ");
                }
            }
            if(!searchCriteria.getTagList().isEmpty()){
                Iterator<Long> tagIterator = searchCriteria.getTagList().iterator();
                fullQuery.append("NT_TAG_ID IN (");
                while(tagIterator.hasNext()){
                    fullQuery.append(tagIterator.next());
                    if(tagIterator.hasNext()){
                        fullQuery.append(", ");
                    }else{
                        fullQuery.append(")");
                    }
                }

            }

        }
        return fullQuery.toString();
    }

    /**
     * Implementation of method {@link NewsDAO#getNewsCount()}
     * @see NewsDAO
     */
    public int getNewsCount() throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try{
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_COUNT_NEWS);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                return resultSet.getInt(1);
            }else{
                return 0;
            }
        }catch(SQLException e){
            throw new DAOException("Method getNewsCount in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    private News buildEntity(ResultSet resultSet) throws SQLException{
        News news = new News();
        news.setId(resultSet.getLong(NE_NEWS_ID));
        news.setTitle(resultSet.getString(NE_TITLE));
        news.setShortText(resultSet.getString(NE_SHORT_TEXT));
        news.setFullText(resultSet.getString(NE_FULL_TEXT));
        news.setCreationDate(resultSet.getTimestamp(NE_CREATION_DATE));
        news.setModificationDate(resultSet.getDate(NE_MODIFICATION_DATE));
        return news;
    }

    /**
     * Implementation of method {@link NewsDAO#getNewsByAuthor(Long)} )}
     * @see NewsDAO
     */
    public List<News> getNewsByAuthor(Long idAuthor) throws DAOException {

        List<News> newsList = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_NEWS_BY_AUTHOR);
            statement.setLong(1, idAuthor);
            resultSet = statement.executeQuery();

            newsList = new ArrayList<News>();
            News news = null;
            while(resultSet.next()){
                news = buildEntity(resultSet);
                newsList.add(news);
            }
            return newsList;
        }catch(SQLException e){
            throw new DAOException("Method getNewsByAuthor in NewsDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }

    }


    /**
     * Implementation of method {@link NewsDAO#getAllPaged(Integer, Integer)}
     * @see NewsDAO
     */
    public List<News> getAllPaged(Integer offset, Integer count) throws DAOException{

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<News> newsList;

        try{
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_ALL_PAGED);
            statement.setInt(1, offset);
            statement.setInt(2, count);
            statement.setInt(3, offset);
            statement.setInt(4, count);
            resultSet = statement.executeQuery();

            newsList = new ArrayList<News>();
            News news;
            while(resultSet.next()){
                news = buildEntity(resultSet);
                newsList.add(news);
            }
            return newsList;
        }catch(SQLException e){
            throw new DAOException("Method getAllPaged in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#attachAuthorToNews(Long, List)}
     * @see NewsDAO
     */
    @Override
    public void attachTagToNews(Long idNews, List<Long> idTagList) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_ATTACH_TAG_TO_NEWS);

            for(Long idTag : idTagList) {
                statement.setLong(1, idNews);
                statement.setLong(2, idTag);
                statement.addBatch();
            }

            statement.executeBatch();

        }catch(SQLException e){
            throw new DAOException("Method attachTagToNews in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#attachAuthorToNews(Long, List)}
     * @see NewsDAO
     */
    @Override
    public void attachAuthorToNews(Long idNews, List<Long> idAuthorList) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_ATTACH_AUTHOR_TO_NEWS);

            for(Long idAuthor : idAuthorList) {
                statement.setLong(1, idNews);
                statement.setLong(2, idAuthor);
                statement.addBatch();
            }

            statement.executeBatch();

        }catch(SQLException e){
            throw new DAOException("Method attachAuthorToNews in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#detachAuthorFromNews(Long, List)}
     * @see NewsDAO
     */
    @Override
    public void detachAuthorFromNews(Long idNews, List<Long> idAuthorList) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_DETACH_AUTHOR_FROM_NEWS);
            for(Long idAuthor:idAuthorList) {
                statement.setLong(1, idNews);
                statement.setLong(2, idAuthor);
                statement.addBatch();
            }

            statement.executeBatch();

        }catch(SQLException e){
            throw new DAOException("Method detachAuthorFromNews in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#detachTagFromNews(Long, List)}
     * @see NewsDAO
     */
    @Override
    public void detachTagFromNews(Long idNews, List<Long> idTagList) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_DETACH_TAG_FROM_NEWS);
            for(Long idTag : idTagList) {
                statement.setLong(1, idNews);
                statement.setLong(2, idTag);
                statement.addBatch();
            }
            statement.executeBatch();

        }catch(SQLException e){
            throw new DAOException("Method detachTagFromNews in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#getPrevious(Long)}
     * @see NewsDAO
     */
    @Override
    public Long getPrevious(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_PREV);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();

            Long prevId = null;
            if(resultSet.next()){
                prevId = resultSet.getLong(PREV_NEWS);
            }
            return prevId;
        }catch(SQLException e){
            throw new DAOException("Method getPrevious in NewsDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#getNext(Long)}
     * @see NewsDAO
     */
    @Override
    public Long getNext(Long id) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(SQL_GET_NEXT);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();

            Long nextId = null;
            if(resultSet.next()){
                nextId = resultSet.getLong(NEXT_NEWS);
            }
            return nextId;
        }catch(SQLException e){
            throw new DAOException("Method getNext in NewsDao", e);
        }finally {
            DAOUtil.closeConnection(datasource, connection, statement);
        }
    }

    /**
     * Implementation of method {@link NewsDAO#find(SearchCriteria, int, int)}
     * @see NewsDAO
     */
    public List<News> find(SearchCriteria searchCriteria, int offset, int count) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        String fullQuery = buildQuery(SQL_SEARCH_NEWS_PAGED, searchCriteria) + SQL_WHERE_CLAUSE_PAGED;

        try{
            connection = DataSourceUtils.doGetConnection(datasource);
            statement = connection.prepareStatement(fullQuery);
            statement.setInt(1, offset);
            statement.setInt(2, count);
            statement.setInt(3, offset);
            statement.setInt(4, count);
            resultSet = statement.executeQuery();

            List<News> newsList = new ArrayList<News>();
            News news;
            while(resultSet.next()){
                news = buildEntity(resultSet);
                newsList.add(news);
            }
            return newsList;
        }catch(SQLException e){
            throw new DAOException("Method find in NewsDao", e);
        }finally{
            DAOUtil.closeConnection(datasource, connection, statement);
        }

    }
}
