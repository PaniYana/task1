package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

/**
 * Created by Yana_Valchok on 4/6/2016.
 */

/**
 * Extension of GenericDao, parametrized by entity <tt>Tag</tt> and key type <tt>Long</tt>
 *
 * Provides with more special methods for working with this entity in database.
 * Contains more methods for finding entities.
 * @see GenericDAO
 */
public interface TagDAO extends GenericDAO<Tag, Long>{

    /**
     * Finds and returns list of tag which were added to certain news message.
     * @param idNews unique key of news message
     * @return list of tags
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    List<Tag> getTagsByNews(Long idNews) throws DAOException;

    /**
     * Inserts tags into certain table.
     *
     * @param tags to insert
     * @return keys of inserted entities
     * @throws DAOException if there were errors or exceptions in the run-time
     */
    List<Long> create(List<Tag> tags) throws DAOException;
}
