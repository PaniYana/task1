package com.epam.newsmanagement.controller.command.impl;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagement;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana_Valchok on 6/14/2016.
 */
@Component
public class FilterCommandImpl implements Command {

    private static final String PAGE = "page";
    private static final int COUNT = 3;
    private static final String RETURN_PAGE = "/news/list";
    private static final String AUTHOR_LIST = "authorList";
    private static final String TAG_LIST = "tagList";
    private static final String NEWS_LIST = "newsList";
    private static final String OFFSET = "offset";
    private static final String AUTHOR_LIST_SELECTED = "authorListSelected";
    private static final String TAG_LIST_SELECTED = "tagListSelected";
    private static final String NEWS_COUNT = "newsCount";

    @Autowired
    private NewsManagement newsManagement;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @Autowired
    private NewsService newsService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String offset = request.getParameter(PAGE);
        List<NewsTO> newsList = null;
        List<Long> authorListSelected = getAuthors(request);
        List<Long> tagListSelected = getTags(request);
        List<Author> authorList = null;
        List<Tag> tagList = null;
        int newsCount = 0;
        if(offset == null) {
            offset = "1";
        }
        try {
            newsList = newsManagement.find(authorListSelected, tagListSelected, Integer.parseInt(offset),COUNT);
            authorList = authorService.getAll();
            tagList = tagService.getAll();
            newsCount = newsManagement.find(authorListSelected, tagListSelected).size();
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage());
        }
        request.setAttribute(NEWS_LIST, newsList);
        request.setAttribute(OFFSET, offset);
        request.setAttribute(AUTHOR_LIST, authorList);
        request.setAttribute(TAG_LIST, tagList);
        request.setAttribute(AUTHOR_LIST_SELECTED, authorListSelected);
        request.setAttribute(TAG_LIST_SELECTED, tagListSelected);
        request.setAttribute(NEWS_COUNT, newsCount);
        return RETURN_PAGE;
    }

    private List<Long> getAuthors(HttpServletRequest request){
        List authorList = new ArrayList<Long>();
        String[] authors = request.getParameterValues(AUTHOR_LIST);
        for (String author : authors) {
            authorList.add(Long.parseLong(author));
        }
        return authorList;
    }

    private List<Long> getTags(HttpServletRequest request){
        List tagList = new ArrayList<Long>();
        String[] tags = request.getParameterValues(TAG_LIST);
        for (String tag : tags) {
            tagList.add(Long.parseLong(tag));
        }
        return tagList;
    }
}
